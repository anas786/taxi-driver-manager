$(document).ready(function() {
    var dataTable = $('#example').DataTable( {
        //dom : 'l<"#add">frtip',
        "lengthMenu": [[25,50,100,200,500,-1], [25,50,100,200,500,"All"]],
        /*"columnDefs": [ {"targets": 6,"orderable": false} ],*/
        "aaSorting": [],
        "aoColumnDefs": [{bSortable: false, aTargets: [ 0 ]}],
        "processing": true,
        "serverSide": true,
        "ajax":{
            url : $('#example').data('url'), // json datasource
            type: "get",  // method  , by default get
            error: function(){  // error handling
                $(".example-error").html("");
                $("#example").append('<tbody class="employee-grid-error"><tr><th colspan="3">No Category found in the server</th></tr></tbody>');
                $("#example_processing").css("display","none");

            }
        }

    });
    
    // Select2
    $('.select2').select2();
    
    //creating multiple instances
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
    
    $('#checkboxAll').on('change', function() {
        if ($(this).is(':checked') ) {
            $('input[type=checkbox]').prop('checked', true);
            $('#bulk_delete').prop('disabled', false);
        } else {
            $('input[type=checkbox]').prop('checked', false);
            $('#bulk_delete').prop('disabled', true);
        }
    });
});

$(document).on('change', '.bulk-checkbox', function() {
    if( $('.bulk-checkbox:checked').length > 0 ) {
        $('#bulk_delete').prop('disabled', false);
    } else {
        $('#bulk_delete').prop('disabled', true);
    }
});

function fetchStates() {
    var country_id = $('#country_id').val();
    if( country_id != '' ) {
        $.ajax({
            url: API_URL + 'localization/states',
            method: 'GET',
            data: {
                country_id: country_id
            },
            dataType: 'JSON',
            beforeSend: function() {
                
            },
            success: function(res) {
                console.log(res);
                var opts = '';
                if( res.status == 1 ) {
                    for( var i = 0; i < res.data.length; i++ ) {
                        opts += '<option value="'+res.data[i].id+'">' + res.data[i].name + '</option>';
                    }
                    $('#state_id').html(opts).trigger('change');
                }
            },
            error: function() {
                
            }
        });
    }
}
function fetchCities() {
    var state_id = $('#state_id').val();
    if( state_id != '' ) {
        $.ajax({
            url: API_URL + 'localization/cities',
            method: 'GET',
            data: {
                state_id: state_id
            },
            dataType: 'JSON',
            beforeSend: function() {
                
            },
            success: function(res) {
                console.log(res);
                var opts = '';
                if( res.status == 1 ) {
                    for( var i = 0; i < res.data.length; i++ ) {
                        opts += '<option value="'+res.data[i].id+'">' + res.data[i].name + '</option>';
                    }
                    $('#city_id').html(opts).trigger('change');
                }
            },
            error: function() {
                
            }
        });
    }
}

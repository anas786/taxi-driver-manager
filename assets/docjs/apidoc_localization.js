/*********************************
Localization APIs
*******************************/


/**
* @api {get} /v1/localization/states Get States
* @apiName states
* @apiGroup Localization
* @apiVersion 1.0.0
*
* @apiParam {Number} country_id Country ID.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Data Array containing states.
* @apiSuccess {Number} data.id    State ID.
* @apiSuccess {String} data.name    State Name.
* @apiSuccess {String} data.country_id    Country ID.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "id": "2723",
*               "name": "Baluchistan",
*               "country_id": "166"
*           },
*           {
*               "id": "2724",
*               "name": "Federal Capital Area",
*               "country_id": "166"
*           },
*           ...
*       ],
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
* @api {get} /v1/localization/cities Get Cities
* @apiName cities
* @apiGroup Localization
* @apiVersion 1.0.0
*
* @apiParam {Number} state_id State ID.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Data Array containing cities.
* @apiSuccess {Number} data.id    City ID.
* @apiSuccess {String} data.name    City Name.
* @apiSuccess {String} data.state_id    State ID.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "id": "31540",
*               "name": "Adilpur",
*               "state_id": "2729"
*           },
*           {
*               "id": "31541",
*               "name": "Badah",
*               "state_id": "2729"
*           },
*           ...
*       ],
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
* @api {get} /v1/localization/cities_by_country Get Cities By Country
* @apiName cities_by_country
* @apiGroup Localization
* @apiVersion 1.0.0
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data Array containing cities.
* @apiSuccess {Number} data.country_id    Country ID
* @apiSuccess {String} data.country_name    Country Name
* @apiSuccess {String} data.phonecode    Country Phonecode
* @apiSuccess {String} data.flag    Country Flag
* @apiSuccess {Object[]} data.cities    Array of Cities
* @apiSuccess {Number} data.cities.id    City ID.
* @apiSuccess {String} data.cities.name    City Name.
* @apiSuccess {String} data.cities.state_id    State ID.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data":
*       "country_id": "166",
        "country_name": "Pakistan",
        "phonecode": "+92",
        "flag": "http://45.55.174.194/nvoii_manager/assets/img/flags/+92.png",
        "cities": [
*           {
*               "id": "31540",
*               "name": "Adilpur",
*               "state_id": "2729"
*           },
*           {
*               "id": "31541",
*               "name": "Badah",
*               "state_id": "2729"
*           },
*           ...
*       ],
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/

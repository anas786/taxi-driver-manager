/*********************************
Passenger APIs
*******************************/


/**
* @api {post} /v1/passenger/send_code Send Verification code to Passenger's phone
* @apiName send_code
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {String} mobile Passenger mobile number.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data=null    Data.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": null,
*       "msg": "Please enter the code received via SMS."
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 403 NotAllowed) {Boolean} status=false Status of request.
 * @apiError (Error 403 NotAllowed) {String} data=null Data.
 * @apiError (Error 403 NotAllowed) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/



/**
* @api {post} /v1/passenger/verify_code Verify Code
* @apiName verify_code
* @apiDescription This API verifies the code and returns Passenger's info. The API will return only <code>driver_id</code> if this is new registration otherwise passenger's complete information will be returned.
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {String} mobile Passenger mobile number.
* @apiParam {Number} code Verification code.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing Passenger's info.
* @apiSuccess {Boolean} data.is_registered    Flag to show whether passenger is registered or not
* @apiSuccess {Object} data.info    Object with complete passenger info (if is_registered flag is TRUE).
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "is_registered": true,
*           "info": {
*               "id": "1",
*               "firstname": "Muzammil",
*               "lastname": "Versiani",
*               "email": "versiani.muzammil@gmail.com",
*               "country_id": "166",
*               "mobile": "+923243367335",
*               "photo": "default.png",
*               "code": null,
*               "is_active": "1",
*               "is_delete": "0",
*               "date_add": "0000-00-00 00:00:00",
*               "date_update": "0000-00-00 00:00:00",
*               "date_registered": "2017-11-17 11:15:51",
*               "options": [
*                   {
*                       "option_key": "contact_sync",
*                       "option_value": "0"
*                   }
*               ],
*               "rating": 5,
*               "saved_locations": [
*                   {
*                       "id": 1,
*                       "passenger_id": 17,
*                       "icon": "http://localhost/ridenow_manager/assets/uploads/icons/home.png",
*                       "label": "Home",
*                       "latitude": 24.2308273,
*                       "longitude": 67.3453443
*                   }
*                   ...
*               ]
*           }
*       },
*       "msg": "Code verified."
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {post} /v1/driver/device_token Save/Update Device Token
 * @apiName device_token
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {String} token Device Token.
 * @apiParam {String} platform Device Platform (Android or iOS).
 * @apiParam {Number} user_id User ID.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data=null    Data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": true,
*       "msg": "Token saved"
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 403 NotAllowed) {Boolean} status=false Status of request.
 * @apiError (Error 403 NotAllowed) {String} data=null Data.
 * @apiError (Error 403 NotAllowed) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
* @api {get} /v1/passenger/options Passenger Options
* @apiName options
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {Number} passenger_id Passenger ID
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Array containing options.
* @apiSuccess {String} data.option_key    Option Key.
* @apiSuccess {String} data.option_value    Option Value.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "option_key": "contact_sync",
*               "option_value": "1"
*           }
*       ],
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {get} /v1/passenger/rating Passenger Rating
 * @apiName rating
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Number} data    Passenger Rating.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": 5,
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
* @api {post} /v1/passenger/update_option Update Passenger Option
* @apiName update_option
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {Number} passenger_id    Passenger ID.
* @apiParam {String} option_key    Option Key (Valid keys: contact_sync, default_payment_method (0=Cash)).
* @apiParam {String} option_value    Option Value.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Array containing options.
* @apiSuccess {String} data.option_key    Option Key.
* @apiSuccess {String} data.option_value    Option Value.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "option_key": "contact_sync",
*               "option_value": "1"
*           }
*       ],
*       "msg": "Option has been updated"
*     }
* 
* @apiError (Error 404 InvalidOption) {Boolean} status=false Status of request.
* @apiError (Error 404 InvalidOption) {String} data=null Data.
* @apiError (Error 404 InvalidOption) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {post} /v1/passenger/cancel_ride Cancel Ride
 * @apiName cancel_ride
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} ride_id    Ride ID.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Data Object.
 * @apiSuccess {Boolean} data.is_cancelled    Cancel Flag.
 * @apiSuccess {String} data.date_update    Date Update.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": {
*           "is_cancelled": "1",
*           "date_update": "2018-03-27 17:19:54"
*       }
*       ,
*       "msg": "Ride cancelled"
*     }
 *
 * @apiError (Error 404 InvalidOption) {Boolean} status=false Status of request.
 * @apiError (Error 404 InvalidOption) {String} data=null Data.
 * @apiError (Error 404 InvalidOption) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
* @api {post} /v1/passenger/update_photo Update Passenger Photo
* @apiName update_photo
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {Number} passenger_id    Passenger ID.
* @apiParam {Object} photo    Passenger Photo (Image Object).
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing driver new photo.
* @apiSuccess {Number} data.photo    Driver Photo URL.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "photo": "http://localhost/ridenow_manager/assets/uploads/passenger/default.png"
*       },
*       "msg": "Photo has been updated"
*     }
* 
* @apiError (Error 404 InvalidImage) {Boolean} status=false Status of request.
* @apiError (Error 404 InvalidImage) {String} data=null Data.
* @apiError (Error 404 InvalidImage) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/



/**
 * @api {get} /v1/passenger/cancellation_reasons Passenger Cancellation Reasons
 * @apiName cancellation_reasons
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing reasons.
 * @apiSuccess {Number} data.id    Reason ID.
 * @apiSuccess {Number} data.type    Type ID (1=Driver, 2=Passenger).
 * @apiSuccess {String} data.reason    Reason Text.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
*               "id": "5",
*               "type": "2",
*               "reason": "The fare is too expensive"
*           },
*           {
*               "id": "6",
*               "type": "2",
*               "reason": "The ride has been delayed"
*           },
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */



/**
 * @api {get} /v1/passenger/rating_messages Passenger Rating Messages
 * @apiName rating_messages
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing reasons.
 * @apiSuccess {Number} data.id    Reason ID.
 * @apiSuccess {String} data.message    Message Text.
 * @apiSuccess {Number} data.type=2    Type ID (1=Driver, 2=Passenger).
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
*               "id": "6",
*               "message": "Driving",
*               "type": "2"
*           },
*           {
*               "id": "7",
*               "message": "Route by NVOII",
*               "type": "2"
*           },
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {post} /v1/passenger/rate_driver Passenger's rating to Driver
 * @apiName rate_driver
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} ride_id Ride ID
 * @apiParam {rating} rating Rating Number from 1 to 5
 * @apiParam {Number} [rating_message_id] Rating Message ID
 * @apiParam {String} [notes] Notes
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    NULL.
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Rating has been saved."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */



/**
 * @api {post} /v1/passenger/save_location Save passenger's location in DB
 * @apiName save_location
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 * @apiParam {String} label Location Label
 * @apiParam {String} icon Location Icon
 * @apiParam {Number} latitude Location Latitude
 * @apiParam {Number} longitude Location Longitude
 * @apiParam {String} address Address in Text Form
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    NULL.
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Location saved successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {get} /v1/passenger/saved_locations Get passenger's saved locations
 * @apiName saved_locations
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing saved locations.
 * @apiSuccess {Number} data.id Location ID
 * @apiSuccess {Number} data.passenger_id Passenger ID
 * @apiSuccess {String} data.label Location Label
 * @apiSuccess {String} data.icon Location Icon
 * @apiSuccess {Number} data.latitude Location Latitude
 * @apiSuccess {Number} data.longitude Location Longitude
 * @apiSuccess {Number} data.address Address
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
*               "id": 1,
*               "passenger_id": 17,
*               "icon": "http://localhost/ridenow_manager/assets/uploads/icons/home.png",
*               "label": "Home",
*               "latitude": 24.2308273,
*               "longitude": 67.3453443,
*               "address": "Gulshan-e-Iqbal, Karachi, Pakistan"
*           }
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {post} /v1/passenger/save_location Save passenger's location in DB
 * @apiName save_location
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 * @apiParam {String} label Location Label
 * @apiParam {String} icon Location Icon
 * @apiParam {Number} latitude Location Latitude
 * @apiParam {Number} longitude Location Longitude
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    NULL.
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Location saved successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {post} /v1/passenger/save_card Save Passenger Credit Card
 * @apiName save_card
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 * @apiParam {Number} card_number Card Number
 * @apiParam {String} expiry Card Expiry Date (e.g 08/2019)
 * @apiParam {Number} cvv Card CVV
 * @apiParam {String} zip Valid Zip Code
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing saved cards.
 * @apiSuccess {Number} data.id Card ID
 * @apiSuccess {Number} data.passenger_id Passenger ID
 * @apiSuccess {String} data.card_number Last 4 digits of card number
 * @apiSuccess {String} data.expiry_month Card Expiry Month
 * @apiSuccess {Number} data.expiry_year Card Expiry Year
 * @apiSuccess {Number} data.card_type Card Type
 * @apiSuccess {String} data.cvv Hidden CVV
 * @apiSuccess {String} data.zip Zip Code
 * @apiSuccess {Number} data.image Card Logo Image
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
                "id": "8",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "zip": "123456",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
            {
                "id": "7",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "cvv": "12345",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
*           ...
*       ],
*       "msg": "Card saved successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
 * @api {post} /v1/passenger/update_card Update Passenger Credit Card
 * @apiName update_card
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} card_id Credit Card ID
 * @apiParam {Number} passenger_id Passenger ID
 * @apiParam {Number} card_number Card Number
 * @apiParam {String} expiry Card Expiry Date (e.g 08/2019)
 * @apiParam {Number} cvv Card CVV
 * @apiParam {String} zip Valid Zip Code
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing saved cards.
 * @apiSuccess {Number} data.id Card ID
 * @apiSuccess {Number} data.passenger_id Passenger ID
 * @apiSuccess {String} data.card_number Last 4 digits of card number
 * @apiSuccess {String} data.expiry_month Card Expiry Month
 * @apiSuccess {Number} data.expiry_year Card Expiry Year
 * @apiSuccess {Number} data.card_type Card Type
 * @apiSuccess {String} data.cvv Hidden CVV
 * @apiSuccess {String} data.zip Zip Code
 * @apiSuccess {Number} data.image Card Logo Image
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
                "id": "8",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "zip": "123456",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
            {
                "id": "7",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "cvv": "12345",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
*           ...
*       ],
*       "msg": "Card updated successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
 * @api {get} /v1/passenger/cards Get Passenger Credit Cards
 * @apiName cards
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing saved cards.
 * @apiSuccess {Number} data.id Card ID
 * @apiSuccess {Number} data.passenger_id Passenger ID
 * @apiSuccess {String} data.card_number Last 4 digits of card number
 * @apiSuccess {String} data.expiry_month Card Expiry Month
 * @apiSuccess {Number} data.expiry_year Card Expiry Year
 * @apiSuccess {Number} data.card_type Card Type
 * @apiSuccess {Number} data.image Card Logo Image
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
                "id": "8",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
            {
                "id": "7",
                "passenger_id": "1",
                "card_number": "*5394",
                "expiry_month": "10",
                "expiry_year": "2022",
                "card_type": "mastercard",
                "cvv": "***",
                "image": "http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png"
            },
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
 * @api {get} /v1/passenger/logout Passenger Logout
 * @apiName logout
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {String} passenger_id Passenger ID.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data=null    Data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Logout successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
* @api {post} /v1/passenger/update_profile Update Passenger Profile
* @apiName update_profile
* @apiGroup Passenger
* @apiVersion 1.0.0
*
* @apiParam {Number} passenger_id    Passenger ID.
* @apiParam {String} firstname    Passenger Firstname.
* @apiParam {String} lastname    Passenger Lastname.
* @apiParam {String} email    Passenger Email address.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Array containing passenger info.
* @apiSuccess {Number} data.id    Passenger ID.
* @apiSuccess {String} data.firstname    Passenger Firstname.
* @apiSuccess {String} data.lastname    Passenger Lastname.
* @apiSuccess {String} data.email    Passenger Email.
* @apiSuccess {Number} data.country_id    Passenger Country ID.
* @apiSuccess {String} data.mobile    Passenger Mobile Number.
* @apiSuccess {String} data.photo    Passenger Photo.
* @apiSuccess {Number} data.code    Passenger Code.
* @apiSuccess {Boolean} data.is_active    Passenger Status Flag (0 = In active, 1 = Active).
* @apiSuccess {Boolean} data.is_delete    Passenger Delete Flag (0 = In active, 1 = Active).
* @apiSuccess {Date} data.date_add    Add date.
* @apiSuccess {Date} data.date_update    Update date.
* @apiSuccess {Date} data.date_registered    Registration date.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           "id": "1",
*           "firstname": "Muzammil",
*           "lastname": "Versiani",
*           "email": "versiani.muzammil@gmail.com",
*           "country_id": "166",
*           "mobile": "+923243367335",
*           "photo": "default.png",
*           "code": null,
*           "is_active": "1",
*           "is_delete": "0",
*           "date_add": "0000-00-00 00:00:00",
*           "date_update": "0000-00-00 00:00:00",
*           "date_registered": "2017-11-17 11:15:51",
*           "options": [
*               {
*                   "option_key": "contact_sync",
*                   "option_value": "0"
*               }
*           ]
*       ],
*       "msg": "Profile has been updated."
*     }
* 
* @apiError (Error 404 EmailExists) {Boolean} status=false Status of request.
* @apiError (Error 404 EmailExists) {String} data=null Data.
* @apiError (Error 404 EmailExists) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/



/**
 * @api {get} /v1/driver/booking_history Passenger Booking History
 * @apiName booking_history
 * @apiGroup Passenger
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} passenger_id Passenger ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Data containing options.
 * @apiSuccess {String} data.driving_license Driving License
 * @apiSuccess {String} data.insurance_certificate Insurance Certificate
 * @apiSuccess {String} data.inspection_certificate Inspection Certificate
 * @apiSuccess {String} data.taxi_association_number Taxi Association Number
 * @apiSuccess {String} data.private_hire_vehicle_license Private Hire Vehicle License
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": {
*           "past": [
            {
                "id": "22",
                "driver_id": "17",
                "passenger_id": "1",
                "channel": "channel_1521129275355",
                "vehicle_type_id": "1",
                "passengers": "2",
                "pickup_coordinates": "24.9090218,67.0825462",
                "pickup_location": "Gulshan-e-Iqbal, Karachi, Pakistan",
                "dropoff_coordinates": "24.921883,67.065521",
                "dropoff_location": "Aisha Manzil, Karachi, Pakistan",
                "payment_method": "1",
                "ride_type": "1",
                "is_accepted": "1",
                "is_completed": "0",
                "is_cancelled": "0",
                "is_delete": "0",
                "date_add": "2018-03-15 15:54:00",
                "date_update": "2018-03-15 15:54:35",
                "driver": {
                    "id": "17",
                    "firstname": "Uzair",
                    "lastname": "Noman",
                    "email": "uzairation@gmail.com",
                    "country_id": "2",
                    "mobile": "+923473556114",
                    "state_id": "1",
                    "city_id": "1",
                    "password": "16d7a4fca7442dda3ad93c9a726597e4",
                    "photo": "http://45.55.174.194/nvoii_manager/assets/uploads/drivers/588b11ad7a92d13777fe0be3adf633bf_1520155848.jpg",
                    "invite_code": "",
                    "reset_code": null,
                    "is_busy": "0",
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "2018-02-08 16:43:49",
                    "date_update": "2018-02-08 16:43:49",
                    "date_registered": "2018-02-08 16:43:49"
                },
                "passenger": {
                    "id": "1",
                    "firstname": "Muzammil",
                    "lastname": "Versiani",
                    "email": "versiani.muzammil@gmail.com",
                    "country_id": "166",
                    "mobile": "03243367335",
                    "photo": "http://45.55.174.194/nvoii_manager/assets/uploads/passengers/default.png",
                    "code": null,
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "0000-00-00 00:00:00",
                    "date_update": "0000-00-00 00:00:00",
                    "date_registered": "2017-11-17 11:15:51"
                },
                "vehicle_type": {
                    "id": "1",
                    "name": "NVOII",
                    "tagline": "Travel with ease",
                    "description": "",
                    "image": "http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png",
                    "country_id": "166",
                    "max_capacity": "3",
                    "base_fare": "5.00",
                    "minimum_fare": "3.00",
                    "per_mile_fare": "5.00",
                    "per_minute_fare": "5.00",
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "2018-03-20 15:50:42",
                    "date_update": "2018-03-20 15:50:42"
                },
                "payment_method_info": {
                    "id": "1",
                    "payment_method": "Cash",
                    "is_active": "1"
                },
                "ride_type_text": "Now",
                "history": [
                    {
                        "id": "22",
                        "ride_id": "22",
                        "status": "1",
                        "datetime": "2018-03-15 15:54:00"
                    },
                    {
                        "id": "23",
                        "ride_id": "22",
                        "status": "2",
                        "datetime": "2018-03-15 15:54:35"
                    }
                ]
            },
            ....
            ],
            "upcoming": [],
            "cancelled": []
*       },
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */
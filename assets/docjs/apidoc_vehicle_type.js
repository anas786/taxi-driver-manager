/*********************************
Vehicle Type API
*******************************/

/**
 * @api {post} /v1/vehicle_type/get_all Get All Vehicle Types
 * @apiName get_all
 * @apiGroup Vehicle Type
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing passenger info.
 * @apiSuccess {Number} data.id    Vehicle Type ID.
 * @apiSuccess {String} data.name    Vehicle Type Name.
 * @apiSuccess {String} data.tagline    Tagline.
 * @apiSuccess {String} data.description    Description.
 * @apiSuccess {String} data.image    Vehicle Type Image.
 * @apiSuccess {Number} data.country_id    Country ID.
 * @apiSuccess {Number} data.max_capacity    Max Capacity.
 * @apiSuccess {Number} data.base_fare    Base Fare.
 * @apiSuccess {Number} data.minimum_fare    Minimum Fare.
 * @apiSuccess {Number} data.per_mile_fare    Per Mile Fare.
 * @apiSuccess {Number} data.per_minute_fare    Per Minute Fare.
 * @apiSuccess {Boolean} data.is_active    Type Status Flag (0 = In active, 1 = Active).
 * @apiSuccess {Boolean} data.is_delete    Type Delete Flag (0 = In active, 1 = Active).
 * @apiSuccess {Date} data.date_add    Add Date.
 * @apiSuccess {Date} data.date_update    Last Update Date.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
            {
                "id": "1",
                "name": "NVOII",
                "tagline": "Travel with ease",
                "description": "",
                "image": "http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png",
                "country_id": "166",
                "max_capacity": "3",
                "base_fare": "5.00",
                "minimum_fare": "3.00",
                "per_mile_fare": "5.00",
                "per_minute_fare": "5.00",
                "is_active": "1",
                "is_delete": "0",
                "date_add": "2018-03-20 15:50:42",
                "date_update": "2018-03-20 15:50:42"
            }
        ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 EmailExists) {Boolean} status=false Status of request.
 * @apiError (Error 404 EmailExists) {String} data=null Data.
 * @apiError (Error 404 EmailExists) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */
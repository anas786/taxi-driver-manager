/*********************************
Driver APIs
*******************************/


/**
* @api {post} /v1/driver/login Driver Login
* @apiName login
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {String} email Driver email address or mobile number.
* @apiParam {String} password Driver password.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing driver information.
* @apiSuccess {Number} data.id    Driver ID.
* @apiSuccess {String} data.firstname    First Name.
* @apiSuccess {String} data.lastname    Last Name.
* @apiSuccess {String} data.email Driver Email Address.
* @apiSuccess {Number} data.country_id Country ID.
* @apiSuccess {String} data.mobile Driver Mobile Number.
* @apiSuccess {Number} data.state_id State ID.
* @apiSuccess {Number} data.city_id City ID.
* @apiSuccess {String} data.password  Driver password(MD5 encrypted).
* @apiSuccess {String} data.photo        Driver Photo.
* @apiSuccess {string} data.invite_code Invite Code.
* @apiSuccess {Boolean} data.is_active Active Flag.
* @apiSuccess {Boolean} data.is_delete Delete Flag.
* @apiSuccess {String} data.date_add Driver Add Date.
* @apiSuccess {String} data.date_update Driver Update Date.
* @apiSuccess {String} data.date_registered Driver Registration Date.
* @apiSuccess {Object} data.documents Holds driver documents 
* @apiSuccess {String} data.documents.driving_license Driving License
* @apiSuccess {String} data.documents.insurance_certificate Insurance Certificate 
* @apiSuccess {String} data.documents.inspection_certificate Inspection Certificate
* @apiSuccess {String} data.documents.taxi_association_number Taxi Association Number
* @apiSuccess {String} data.documents.private_hire_vehicle_license Private Hire Vehicle License
* @apiSuccess {Object[]} data.options Array holds Driver's Options
* @apiSuccess {String} data.options.option_key Option Key
* @apiSuccess {String} data.options.option_value Option Value
* @apiSuccess {Number} data.rating Driver Rating
* @apiSuccess {Object} data.vehicle Object containing Driver Vehicle Info
* @apiSuccess {String} data.vehicle.plate_number Vehicle Plate Number
* @apiSuccess {String} data.vehicle.make Vehicle Make
* @apiSuccess {String} data.vehicle.model Vehicle Model
* @apiSuccess {String} data.vehicle.year Vehicle Year
* @apiSuccess {String} data.vehicle.color Vehicle Color
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "id": "3",
*           "firstname": "Driver",
*           "lastname": "Tofel",
*           "email": "tofotofo@gmail.com",
*           "country_id": "231",
*           "mobile": "123456789",
*           "state_id": "3919",
*           "city_id": "42594",
*           "password": "091faf34f3ebc004beef1a1253504f52",
*           "photo": "http://localhost/ridenow_manager/assets/uploads/drivers/default.png",
*           "invite_code": "CXN5QA",
*           "is_active": "1",
*           "is_delete": "0",
*           "date_add": "2017-11-20 11:50:41",
*           "date_update": "2017-11-20 12:05:20",
*           "date_registered": "2017-11-20 12:05:20",
*           "documents": {
*               "driving_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc",
*               "insurance_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
               "inspection_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc",
*                "taxi_association_number": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
*               "private_hire_vehicle_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/"
*           },
*           "options": [
*               {
*                   "option_key": "booking_alert",
*                  "option_value": "1"
*                },
*               {
*                   "option_key": "pickup_alert",
*                   "option_value": "1"
*               },
*               {
*                   "option_key": "contact_sync",
*                  "option_value": "1"
*               }
*           ],
*           "rating": 5,
*           "vehicle": {
*               "plate_number": "TXI1578",
*               "make": "Toyota",
*               "model": "Corolla",
*               "year": "2010",
*               "color": "Black"
*           }
*       },
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {post} /v1/driver/send_code Send Verification code to Driver's phone
 * @apiName send_code
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {String} mobile Driver mobile number.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data=null    Data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Please enter the code received via SMS."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 403 NotAllowed) {Boolean} status=false Status of request.
 * @apiError (Error 403 NotAllowed) {String} data=null Data.
 * @apiError (Error 403 NotAllowed) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
 * @api {post} /v1/driver/device_token Save/Update Device Token
 * @apiName device_token
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {String} token Device Token.
 * @apiParam {String} platform Device Platform (Android or iOS).
 * @apiParam {Number} user_id User ID.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data=null    Data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": true,
*       "msg": "Token saved"
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 403 NotAllowed) {Boolean} status=false Status of request.
 * @apiError (Error 403 NotAllowed) {String} data=null Data.
 * @apiError (Error 403 NotAllowed) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {get} /v1/driver/logout Driver Logout
 * @apiName logout
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {String} driver_id Driver ID.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data=null    Data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Logout successfully."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */



/**
 * @api {post} /v1/driver/verify_code Verify Code
 * @apiName verify_code
 * @apiDescription This API verifies the code and returns Driver's info.
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {String} mobile Driver mobile number.
 * @apiParam {Number} code Verification code.
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Data containing Passenger's info.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": {
*           "id": "3",
*           "firstname": "Driver",
*           "lastname": "Tofel",
*           "email": "tofotofo@gmail.com",
*           "country_id": "231",
*           "mobile": "123456789",
*           "state_id": "3919",
*           "city_id": "42594",
*           "password": "091faf34f3ebc004beef1a1253504f52",
*           "photo": "http://localhost/ridenow_manager/assets/uploads/drivers/default.png",
*           "taxi_association_number": "ABC3476",
*           "invite_code": "CXN5QA",
*           "is_active": "1",
*           "is_delete": "0",
*           "date_add": "2017-11-20 11:50:41",
*           "date_update": "2017-11-20 12:05:20",
*           "date_registered": "2017-11-20 12:05:20",
*           "documents": {
*               "driving_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc",
*               "insurance_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
               "inspection_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc",
*                "taxi_association_number": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
*               "private_hire_vehicle_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/"
*           },
*           "options": [
*               {
*                   "option_key": "booking_alert",
*                  "option_value": "1"
*                },
*               {
*                   "option_key": "pickup_alert",
*                   "option_value": "1"
*               },
*               {
*                   "option_key": "contact_sync",
*                  "option_value": "1"
*               }
*           ],
*           "rating": 5,
*           "vehicle": {
*               "plate_number": "TXI1578",
*               "make": "Toyota",
*               "model": "Corolla",
*               "year": "2010",
*               "color": "Black"
*           }
*       },
*       "msg": "Code verified."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
* @api {post} /v1/driver/register Driver Registration
* @apiName register
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {String} firstname    First Name.
* @apiParam {String} lastname    Last Name.
* @apiParam {String} email Email Address.
* @apiParam {Number} country_id Country ID.
* @apiParam {String} mobile Mobile Number.
* @apiParam {Number} city_id City ID.
* @apiParam {String} taxi_association  Taxi Association Number.
* @apiParam {String} password  Driver password.
* @apiParam {string} [invite_code] Invite Code.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing driver ID.
* @apiSuccess {Number} data.driver_id    Driver ID.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "driver_id": "3"
*       },
*       "msg": "Driver Registered"
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/




/**
* @api {post} /v1/driver/update_photo Update Driver Photo
* @apiName update_photo
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id    Driver ID.
* @apiParam {Object} photo    Driver Photo (Image Object).
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing driver new photo.
* @apiSuccess {Number} data.photo    Driver Photo URL.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "photo": "http://localhost/ridenow_manager/assets/uploads/drivers/default.png"
*       },
*       "msg": "Photo has been updated"
*     }
* 
* @apiError (Error 404 InvalidImage) {Boolean} status=false Status of request.
* @apiError (Error 404 InvalidImage) {String} data=null Data.
* @apiError (Error 404 InvalidImage) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/



/**
* @api {post} /v1/driver/update_document Update Driver Document
* @apiName update_document
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id    Driver ID.
* @apiParam {String} document_type    Document Type (Valid types: driving_license, insurance_certificate, inspection_certificate, taxi_association_number OR private_hire_vehicle_license).
* @apiParam {Object} document    Driver Document (Image Object).
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing driver document.
* @apiSuccess {Number} data.document    Driver Document URL.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "document": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.pdf"
*       },
*       "msg": "Document has been updated"
*     }
* 
* @apiError (Error 404 InvalidDocument) {Boolean} status=false Status of request.
* @apiError (Error 404 InvalidDocument) {String} data=null Data.
* @apiError (Error 404 InvalidDocument) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
* @api {post} /v1/driver/update_option Update Driver Option
* @apiName update_option
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id    Driver ID.
* @apiParam {String} option_key    Option Key (Valid keys: booking_alert, pickup_alert, contact_sync).
* @apiParam {String} option_value    Option Value.
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Array containing options.
* @apiSuccess {String} data.option_key    Option Key.
* @apiSuccess {String} data.option_value    Option Value.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "option_key": "booking_alert",
*               "option_value": "1"
*           },
*           {
*               "option_key": "pickup_alert",
*               "option_value": "1"
*           },
*           {
*               "option_key": "contact_sync",
*               "option_value": "1"
*           }
*       ],
*       "msg": "Option has been updated"
*     }
* 
* @apiError (Error 404 InvalidOption) {Boolean} status=false Status of request.
* @apiError (Error 404 InvalidOption) {String} data=null Data.
* @apiError (Error 404 InvalidOption) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {get} /v1/driver/rating Driver Rating
 * @apiName rating
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} driver_id Driver ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Number} data    Passenger Rating.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": 5,
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */

/**
 * @api {get} /v1/driver/reports Driver Report
 * @apiName reports
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} driver_id Driver ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Number} data    Object Containing report data.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
            "status": true,
            "data": {
                "today": {
                    "date": "Apr 26, 2018"
                    "trips": 1,
                    "canceled_trips": 0,
                    "earnings": "50.00",
                    "tips": "0.00",
                    "guarantee": 0,
                    "cash_collected": 0,
                    "adjustments": 0,
                    "wallet": 0,
                    "rating": 5,
                    "acceptance": 0,
                    "availability": 0
                },
                "current_cycle": {
                    "date": "Apr 19, 2018 - Apr 25, 2018"
                    "trips": 2,
                    "canceled_trips": 0,
                    "earnings": "1,365.00",
                    "tips": "0.00",
                    "guarantee": 0,
                    "cash_collected": 0,
                    "adjustments": 0,
                    "wallet": 0,
                    "rating": 5,
                    "acceptance": 0,
                    "availability": 0
                },
                "previous_cycle": {
                    "date": "Apr 12, 2018 - Apr 18, 2018"
                    "trips": 19,
                    "canceled_trips": 0,
                    "earnings": "115.00",
                    "tips": "0.00",
                    "guarantee": 0,
                    "cash_collected": 0,
                    "adjustments": 0,
                    "wallet": 0,
                    "rating": 5,
                    "acceptance": 0,
                    "availability": 0
                }
            },
            "msg": ""
        }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {get} /v1/driver/vehicle Driver Vehicle Info
 * @apiName vehicle
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} driver_id Driver ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Object containing Vehicle Info.
 * @apiSuccess {String} data.plate_number Vehicle Plate Number
 * @apiSuccess {String} data.make Vehicle Make
 * @apiSuccess {String} data.model Vehicle Model
 * @apiSuccess {String} data.year Vehicle Year
 * @apiSuccess {String} data.color Vehicle Color
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": {
*           "plate_number": "TXI1578",
*           "make": "Toyota",
*           "model": "Corolla",
*           "year": "2010",
*           "color": "Black"
*       },
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
* @api {post} /v1/driver/update_status Update Driver Status
* @apiName update_status
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id    Driver ID.
* @apiParam {Boolean} status    Driver Status (0 = Offline, 1 = Online).
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data=null    Data.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": null,
*       "msg": "Operation performed successfully."
*     }
* 
* @apiError (Error 404 ErrorOccurred) {Boolean} status=false Status of request.
* @apiError (Error 404 ErrorOccurred) {String} data=null Data.
* @apiError (Error 404 ErrorOccurred) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/

/**
* @api {get} /v1/driver/options Driver Options
* @apiName options
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id Driver ID
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object[]} data    Array containing options.
* @apiSuccess {String} data.option_key    Option Key.
* @apiSuccess {String} data.option_value    Option Value.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": [
*           {
*               "option_key": "booking_alert",
*               "option_value": "1"
*           },
*           {
*               "option_key": "pickup_alert",
*               "option_value": "1"
*           },
*           {
*               "option_key": "contact_sync",
*               "option_value": "1"
*           }
*       ],
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {get} /v1/driver/cancellation_reasons Driver Cancellation Reasons
 * @apiName cancellation_reasons
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing reasons.
 * @apiSuccess {Number} data.id    Reason ID.
 * @apiSuccess {Number} data.type    Type ID (1=Driver, 2=Passenger).
 * @apiSuccess {String} data.reason    Reason Text.
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
*               "id": "1",
*               "type": "1",
*               "reason": "Excessive Load"
*           },
*           {
*               "id": "2",
*               "type": "1",
*               "reason": "Wrong Address"
*           },
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {get} /v1/driver/rating_messages Driver Rating Messages
 * @apiName rating_messages
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object[]} data    Array containing reasons.
 * @apiSuccess {Number} data.id    Reason ID.
 * @apiSuccess {String} data.message    Message Text.
 * @apiSuccess {Number} data.type=1    Type ID (1=Driver, 2=Passenger).
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
*           {
*               "id": "1",
*               "message": "Poor Behaviour",
*               "type": "1"
*           },
*           {
*               "id": "2",
*               "message": "Accompanying Passenger",
*               "type": "1"
*           },
*           ...
*       ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {post} /v1/driver/rate_passenger Driver's rating to Passenger
 * @apiName rate_passenger
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} ride_id Ride ID
 * @apiParam {rating} rating Rating Number from 1 to 5
 * @apiParam {Number} [rating_message_id] Rating Message ID
 * @apiParam {String} [notes] Notes
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    NULL.
 * @apiSuccess {String} msg Response Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": null,
*       "msg": "Rating has been saved."
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
* @api {get} /v1/driver/documents Driver Documents
* @apiName documents
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} driver_id Driver ID
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data containing options.
* @apiSuccess {String} data.driving_license Driving License
* @apiSuccess {String} data.insurance_certificate Insurance Certificate 
* @apiSuccess {String} data.inspection_certificate Inspection Certificate
* @apiSuccess {String} data.taxi_association_number Taxi Association Number
* @apiSuccess {String} data.private_hire_vehicle_license Private Hire Vehicle License
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": {
*           "driving_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc",
*           "insurance_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
            "inspection_certificate": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc",
*           "taxi_association_number": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/",
*           "private_hire_vehicle_license": "http://localhost/ridenow_manager/assets/uploads/drivers/doc/"
*       },
*       "msg": ""
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
 * @api {get} /v1/driver/history Driver History
 * @apiName history
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} driver_id Driver ID
 * @apiParam {Date} date Date (YYYY-mm-dd)
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Data containing history.
 * @apiSuccess {String} data.total_earning Total earning of the day
 * @apiSuccess {Object[]} data.rides Array Containing Rides
 * @apiSuccess {Number} data.rides.id Ride ID
 * @apiSuccess {Number} data.rides.driver_id Driver ID
 * @apiSuccess {Number} data.rides.passenger_id Passenger ID
 * @apiSuccess {Number} data.rides.channel Channel Name
 * @apiSuccess {Number} data.rides.vehicle_type_id Vehicle Type ID
 * @apiSuccess {Number} data.rides.passengers Number of Passengers
 * @apiSuccess {String} data.rides.pickup_coordinates Pickup Coordinates
 * @apiSuccess {String} data.rides.pickup_location Pickup Location
 * @apiSuccess {String} data.rides.dropoff_coordinates Dropoff Coordinates
 * @apiSuccess {String} data.rides.dropoff_location Dropoff Location
 * @apiSuccess {Number} data.rides.payment_method Payment Method
 * @apiSuccess {Number} data.rides.ride_type Ride Type ID (1= Ride Now, 2= Ride Later)
 * @apiSuccess {Boolean} data.rides.is_accepted Ride Accepted Flag
 * @apiSuccess {Boolean} data.rides.is_completed Ride Completed Flag
 * @apiSuccess {Boolean} data.rides.is_cancelled Ride Cancel Flag
 * @apiSuccess {Boolean} data.rides.is_delete Is Delete Flag
 * @apiSuccess {Date} data.rides.date_add Ride Add DateTime
 * @apiSuccess {Date} data.rides.date_update Ride Update DateTime
 * @apiSuccess {Number} data.rides.rating Ride Rating
 * @apiSuccess {String} data.rides.passenger Passenger Name
 * @apiSuccess {String} data.rides.ride_time Ride Time
 * @apiSuccess {Number} data.rides.price Ride Price
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": {
*           "total_earning": 10,
            "rides": [
              {
                "id": "205",
                "driver_id": "71",
                "passenger_id": "7",
                "channel": "channel_1525885589531",
                "vehicle_type_id": "1",
                "passengers": "2",
                "pickup_coordinates": "24.922173963882887,67.06135581253761",
                "pickup_location": "Plot R 47, Federal B Area Memon Colony Block 3 Gulberg, Karachi, Karachi City, Sindh, Pakistan",
                "dropoff_coordinates": "24.8824552,67.11291699999992",
                "dropoff_location": "Shahrah-e-Faisal Rd, Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan",
                "payment_method": "1",
                "ride_type": "1",
                "is_accepted": "1",
                "is_completed": "1",
                "is_cancelled": "0",
                "is_delete": "0",
                "date_add": "2018-05-09 17:06:11",
                "date_update": "2018-05-09 17:06:29",
                "rating": "5",
                "passenger": "Muzammil Versiani",
                "ride_time": "10:06 pm",
                "price": "5.00"
              },
              {
                "id": "216",
                "driver_id": "71",
                "passenger_id": "7",
                "channel": "channel_1525886498782",
                "vehicle_type_id": "2",
                "passengers": "2",
                "pickup_coordinates": "24.9223738,67.06134610000004",
                "pickup_location": "Plot R 39, FB Area Block 3 Memon Colony Block 3 Gulberg, Karachi, Karachi City, Sindh, Pakistan",
                "dropoff_coordinates": "24.8824552,67.11291699999992",
                "dropoff_location": "Shahrah-e-Faisal Rd, Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan",
                "payment_method": "1",
                "ride_type": "1",
                "is_accepted": "1",
                "is_completed": "1",
                "is_cancelled": "0",
                "is_delete": "0",
                "date_add": "2018-05-09 17:21:03",
                "date_update": "2018-05-09 17:21:38",
                "rating": "5",
                "passenger": "Muzammil Versiani",
                "ride_time": "10:21 pm",
                "price": "5.00"
              }
            ]
*       },
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */


/**
 * @api {get} /v1/driver/reservations Driver Future Reservations
 * @apiName reservations
 * @apiGroup Driver
 * @apiVersion 1.0.0
 *
 * @apiParam {Number} driver_id Driver ID
 *
 * @apiSuccess {Boolean} status=true Status of request.
 * @apiSuccess {Object} data    Data containing options.
 * @apiSuccess {String} data.driving_license Driving License
 * @apiSuccess {String} data.insurance_certificate Insurance Certificate
 * @apiSuccess {String} data.inspection_certificate Inspection Certificate
 * @apiSuccess {String} data.taxi_association_number Taxi Association Number
 * @apiSuccess {String} data.private_hire_vehicle_license Private Hire Vehicle License
 * @apiSuccess {String} msg Message.
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
*       "status": true,
*       "data": [
            {
                "id": "68",
                "driver_id": "17",
                "passenger_id": "1",
                "channel": "channel_1522171215305",
                "vehicle_type_id": "1",
                "passengers": "2",
                "pickup_coordinates": "24.9090218,67.0825462",
                "pickup_location": "Gulshan-e-Iqbal, Karachi, Pakistan",
                "dropoff_coordinates": "24.921883,67.065521",
                "dropoff_location": "Aisha Manzil, Karachi, Pakistan",
                "payment_method": "1",
                "ride_type": "2",
                "is_accepted": "1",
                "is_completed": "0",
                "is_cancelled": "0",
                "is_delete": "0",
                "date_add": "2018-03-27 17:19:54",
                "date_update": "2018-03-27 17:20:15",
                "driver": {
                    "id": "17",
                    "firstname": "Uzair",
                    "lastname": "Noman",
                    "email": "uzairation@gmail.com",
                    "country_id": "2",
                    "mobile": "+923473556114",
                    "state_id": "1",
                    "city_id": "1",
                    "password": "16d7a4fca7442dda3ad93c9a726597e4",
                    "photo": "http://45.55.174.194/nvoii_manager/assets/uploads/drivers/588b11ad7a92d13777fe0be3adf633bf_1520155848.jpg",
                    "invite_code": "",
                    "reset_code": null,
                    "is_busy": "0",
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "2018-02-08 16:43:49",
                    "date_update": "2018-02-08 16:43:49",
                    "date_registered": "2018-02-08 16:43:49"
                },
                "passenger": {
                    "id": "1",
                    "firstname": "Muzammil",
                    "lastname": "Versiani",
                    "email": "versiani.muzammil@gmail.com",
                    "country_id": "166",
                    "mobile": "03243367335",
                    "photo": "http://45.55.174.194/nvoii_manager/assets/uploads/passengers/default.png",
                    "code": null,
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "0000-00-00 00:00:00",
                    "date_update": "0000-00-00 00:00:00",
                    "date_registered": "2017-11-17 11:15:51"
                },
                "vehicle_type": {
                    "id": "1",
                    "name": "NVOII",
                    "tagline": "Travel with ease",
                    "description": "",
                    "image": "http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png",
                    "country_id": "166",
                    "max_capacity": "3",
                    "base_fare": "5.00",
                    "minimum_fare": "3.00",
                    "per_mile_fare": "5.00",
                    "per_minute_fare": "5.00",
                    "is_active": "1",
                    "is_delete": "0",
                    "date_add": "2018-03-20 15:50:42",
                    "date_update": "2018-03-20 15:50:42"
                },
                "payment_method_info": {
                    "id": "1",
                    "payment_method": "Cash",
                    "is_active": "1"
                },
                "ride_type_text": "Later",
                "history": [
                    {
                        "id": "85",
                        "ride_id": "68",
                        "status": "1",
                        "datetime": "2018-03-27 17:19:54"
                    },
                    {
                        "id": "86",
                        "ride_id": "68",
                        "status": "2",
                        "datetime": "2018-03-27 17:20:15"
                    },
                    {
                        "id": "87",
                        "ride_id": "68",
                        "status": "3",
                        "datetime": "2018-03-27 17:21:06"
                    },
                    {
                        "id": "88",
                        "ride_id": "68",
                        "status": "4",
                        "datetime": "2018-03-27 17:21:36"
                    },
                    {
                        "id": "89",
                        "ride_id": "68",
                        "status": "5",
                        "datetime": "2018-03-27 17:21:43"
                    }
                ]
            }
        ],
*       "msg": ""
*     }
 *
 * @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
 * @apiError (Error 404 NotFound) {String} data=null Data.
 * @apiError (Error 404 NotFound) {String} msg Error message.
 *
 * @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
 * @apiError (Error 400 BadRequest) {String} data=null Data.
 * @apiError (Error 400 BadRequest) {String} msg Error message.
 */



/**
* @api {post} /v1/driver/forgot Forgot Password
* @apiName forgot
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {String} email Driver Email address or Phone Number
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data Object.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": null,
*       "msg": "Please use the code sent to your email address to set new password."
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/


/**
* @api {post} /v1/driver/reset Reset Password
* @apiName reset
* @apiGroup Driver
* @apiVersion 1.0.0
*
* @apiParam {Number} code Reset Code that Driver received from <a href="#api-Driver-forgot">Forgot Password API</a>
* @apiParam {String} password Driver New Password
*
* @apiSuccess {Boolean} status=true Status of request.
* @apiSuccess {Object} data    Data Object.
* @apiSuccess {String} msg Message.
* 
* @apiSuccessExample {json} Success-Response:
*     HTTP/1.1 200 OK
*     {
*       "status": true,
*       "data": null,
*       "msg": "Password has been reset successfully. Please login with your new password."
*     }
* 
* @apiError (Error 404 NotFound) {Boolean} status=false Status of request.
* @apiError (Error 404 NotFound) {String} data=null Data.
* @apiError (Error 404 NotFound) {String} msg Error message.
* 
* @apiError (Error 400 BadRequest) {Boolean} status=false Status of request.
* @apiError (Error 400 BadRequest) {String} data=null Data.
* @apiError (Error 400 BadRequest) {String} msg Error message.
*/
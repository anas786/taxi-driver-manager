define({ "api": [
  {
    "type": "get",
    "url": "/v1/driver/cancellation_reasons",
    "title": "Driver Cancellation Reasons",
    "name": "cancellation_reasons",
    "group": "Driver",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing reasons.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Reason ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.type",
            "description": "<p>Type ID (1=Driver, 2=Passenger).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.reason",
            "description": "<p>Reason Text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"1\",\n          \"type\": \"1\",\n          \"reason\": \"Excessive Load\"\n      },\n      {\n          \"id\": \"2\",\n          \"type\": \"1\",\n          \"reason\": \"Wrong Address\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/device_token",
    "title": "Save/Update Device Token",
    "name": "device_token",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Device Token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "platform",
            "description": "<p>Device Platform (Android or iOS).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": true,\n  \"msg\": \"Token saved\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 403 NotAllowed": [
          {
            "group": "Error 403 NotAllowed",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/documents",
    "title": "Driver Documents",
    "name": "documents",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.driving_license",
            "description": "<p>Driving License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.insurance_certificate",
            "description": "<p>Insurance Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.inspection_certificate",
            "description": "<p>Inspection Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.taxi_association_number",
            "description": "<p>Taxi Association Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.private_hire_vehicle_license",
            "description": "<p>Private Hire Vehicle License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"driving_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc\",\n      \"insurance_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n        \"inspection_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc\",\n      \"taxi_association_number\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n      \"private_hire_vehicle_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\"\n  },\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/forgot",
    "title": "Forgot Password",
    "name": "forgot",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Driver Email address or Phone Number</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Please use the code sent to your email address to set new password.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/history",
    "title": "Driver History",
    "name": "history",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "date",
            "description": "<p>Date (YYYY-mm-dd)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing history.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.total_earning",
            "description": "<p>Total earning of the day</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.rides",
            "description": "<p>Array Containing Rides</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.id",
            "description": "<p>Ride ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.driver_id",
            "description": "<p>Driver ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.channel",
            "description": "<p>Channel Name</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.vehicle_type_id",
            "description": "<p>Vehicle Type ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.passengers",
            "description": "<p>Number of Passengers</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.pickup_coordinates",
            "description": "<p>Pickup Coordinates</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.pickup_location",
            "description": "<p>Pickup Location</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.dropoff_coordinates",
            "description": "<p>Dropoff Coordinates</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.dropoff_location",
            "description": "<p>Dropoff Location</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.payment_method",
            "description": "<p>Payment Method</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.ride_type",
            "description": "<p>Ride Type ID (1= Ride Now, 2= Ride Later)</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.rides.is_accepted",
            "description": "<p>Ride Accepted Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.rides.is_completed",
            "description": "<p>Ride Completed Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.rides.is_cancelled",
            "description": "<p>Ride Cancel Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.rides.is_delete",
            "description": "<p>Is Delete Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.rides.date_add",
            "description": "<p>Ride Add DateTime</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.rides.date_update",
            "description": "<p>Ride Update DateTime</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.rating",
            "description": "<p>Ride Rating</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.passenger",
            "description": "<p>Passenger Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.rides.ride_time",
            "description": "<p>Ride Time</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rides.price",
            "description": "<p>Ride Price</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"total_earning\": 10,\n        \"rides\": [\n          {\n            \"id\": \"205\",\n            \"driver_id\": \"71\",\n            \"passenger_id\": \"7\",\n            \"channel\": \"channel_1525885589531\",\n            \"vehicle_type_id\": \"1\",\n            \"passengers\": \"2\",\n            \"pickup_coordinates\": \"24.922173963882887,67.06135581253761\",\n            \"pickup_location\": \"Plot R 47, Federal B Area Memon Colony Block 3 Gulberg, Karachi, Karachi City, Sindh, Pakistan\",\n            \"dropoff_coordinates\": \"24.8824552,67.11291699999992\",\n            \"dropoff_location\": \"Shahrah-e-Faisal Rd, Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan\",\n            \"payment_method\": \"1\",\n            \"ride_type\": \"1\",\n            \"is_accepted\": \"1\",\n            \"is_completed\": \"1\",\n            \"is_cancelled\": \"0\",\n            \"is_delete\": \"0\",\n            \"date_add\": \"2018-05-09 17:06:11\",\n            \"date_update\": \"2018-05-09 17:06:29\",\n            \"rating\": \"5\",\n            \"passenger\": \"Muzammil Versiani\",\n            \"ride_time\": \"10:06 pm\",\n            \"price\": \"5.00\"\n          },\n          {\n            \"id\": \"216\",\n            \"driver_id\": \"71\",\n            \"passenger_id\": \"7\",\n            \"channel\": \"channel_1525886498782\",\n            \"vehicle_type_id\": \"2\",\n            \"passengers\": \"2\",\n            \"pickup_coordinates\": \"24.9223738,67.06134610000004\",\n            \"pickup_location\": \"Plot R 39, FB Area Block 3 Memon Colony Block 3 Gulberg, Karachi, Karachi City, Sindh, Pakistan\",\n            \"dropoff_coordinates\": \"24.8824552,67.11291699999992\",\n            \"dropoff_location\": \"Shahrah-e-Faisal Rd, Faisal Cantonment, Karachi, Karachi City, Sindh, Pakistan\",\n            \"payment_method\": \"1\",\n            \"ride_type\": \"1\",\n            \"is_accepted\": \"1\",\n            \"is_completed\": \"1\",\n            \"is_cancelled\": \"0\",\n            \"is_delete\": \"0\",\n            \"date_add\": \"2018-05-09 17:21:03\",\n            \"date_update\": \"2018-05-09 17:21:38\",\n            \"rating\": \"5\",\n            \"passenger\": \"Muzammil Versiani\",\n            \"ride_time\": \"10:21 pm\",\n            \"price\": \"5.00\"\n          }\n        ]\n  },\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/login",
    "title": "Driver Login",
    "name": "login",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Driver email address or mobile number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Driver password.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing driver information.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.firstname",
            "description": "<p>First Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.lastname",
            "description": "<p>Last Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Driver Email Address.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.country_id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.mobile",
            "description": "<p>Driver Mobile Number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.state_id",
            "description": "<p>State ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.city_id",
            "description": "<p>City ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.password",
            "description": "<p>Driver password(MD5 encrypted).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.photo",
            "description": "<p>Driver Photo.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "data.invite_code",
            "description": "<p>Invite Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_active",
            "description": "<p>Active Flag.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_delete",
            "description": "<p>Delete Flag.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.date_add",
            "description": "<p>Driver Add Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.date_update",
            "description": "<p>Driver Update Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.date_registered",
            "description": "<p>Driver Registration Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.documents",
            "description": "<p>Holds driver documents</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.documents.driving_license",
            "description": "<p>Driving License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.documents.insurance_certificate",
            "description": "<p>Insurance Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.documents.inspection_certificate",
            "description": "<p>Inspection Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.documents.taxi_association_number",
            "description": "<p>Taxi Association Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.documents.private_hire_vehicle_license",
            "description": "<p>Private Hire Vehicle License</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.options",
            "description": "<p>Array holds Driver's Options</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.options.option_key",
            "description": "<p>Option Key</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.options.option_value",
            "description": "<p>Option Value</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.rating",
            "description": "<p>Driver Rating</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.vehicle",
            "description": "<p>Object containing Driver Vehicle Info</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.vehicle.plate_number",
            "description": "<p>Vehicle Plate Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.vehicle.make",
            "description": "<p>Vehicle Make</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.vehicle.model",
            "description": "<p>Vehicle Model</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.vehicle.year",
            "description": "<p>Vehicle Year</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.vehicle.color",
            "description": "<p>Vehicle Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"id\": \"3\",\n      \"firstname\": \"Driver\",\n      \"lastname\": \"Tofel\",\n      \"email\": \"tofotofo@gmail.com\",\n      \"country_id\": \"231\",\n      \"mobile\": \"123456789\",\n      \"state_id\": \"3919\",\n      \"city_id\": \"42594\",\n      \"password\": \"091faf34f3ebc004beef1a1253504f52\",\n      \"photo\": \"http://localhost/ridenow_manager/assets/uploads/drivers/default.png\",\n      \"invite_code\": \"CXN5QA\",\n      \"is_active\": \"1\",\n      \"is_delete\": \"0\",\n      \"date_add\": \"2017-11-20 11:50:41\",\n      \"date_update\": \"2017-11-20 12:05:20\",\n      \"date_registered\": \"2017-11-20 12:05:20\",\n      \"documents\": {\n          \"driving_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc\",\n          \"insurance_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n           \"inspection_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc\",\n           \"taxi_association_number\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n          \"private_hire_vehicle_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\"\n      },\n      \"options\": [\n          {\n              \"option_key\": \"booking_alert\",\n             \"option_value\": \"1\"\n           },\n          {\n              \"option_key\": \"pickup_alert\",\n              \"option_value\": \"1\"\n          },\n          {\n              \"option_key\": \"contact_sync\",\n             \"option_value\": \"1\"\n          }\n      ],\n      \"rating\": 5,\n      \"vehicle\": {\n          \"plate_number\": \"TXI1578\",\n          \"make\": \"Toyota\",\n          \"model\": \"Corolla\",\n          \"year\": \"2010\",\n          \"color\": \"Black\"\n      }\n  },\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/logout",
    "title": "Driver Logout",
    "name": "logout",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Logout successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/options",
    "title": "Driver Options",
    "name": "options",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_key",
            "description": "<p>Option Key.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_value",
            "description": "<p>Option Value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"option_key\": \"booking_alert\",\n          \"option_value\": \"1\"\n      },\n      {\n          \"option_key\": \"pickup_alert\",\n          \"option_value\": \"1\"\n      },\n      {\n          \"option_key\": \"contact_sync\",\n          \"option_value\": \"1\"\n      }\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/rate_passenger",
    "title": "Driver's rating to Passenger",
    "name": "rate_passenger",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ride_id",
            "description": "<p>Ride ID</p>"
          },
          {
            "group": "Parameter",
            "type": "rating",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating Number from 1 to 5</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rating_message_id",
            "description": "<p>Rating Message ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "notes",
            "description": "<p>Notes</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>NULL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Rating has been saved.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/rating",
    "title": "Driver Rating",
    "name": "rating",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data",
            "description": "<p>Passenger Rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": 5,\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/rating_messages",
    "title": "Driver Rating Messages",
    "name": "rating_messages",
    "group": "Driver",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing reasons.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Reason ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>Message Text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.type",
            "defaultValue": "1",
            "description": "<p>Type ID (1=Driver, 2=Passenger).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"1\",\n          \"message\": \"Poor Behaviour\",\n          \"type\": \"1\"\n      },\n      {\n          \"id\": \"2\",\n          \"message\": \"Accompanying Passenger\",\n          \"type\": \"1\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/register",
    "title": "Driver Registration",
    "name": "register",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>First Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Last Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email Address.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Mobile Number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "city_id",
            "description": "<p>City ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "taxi_association",
            "description": "<p>Taxi Association Number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Driver password.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "invite_code",
            "description": "<p>Invite Code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing driver ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.driver_id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"driver_id\": \"3\"\n  },\n  \"msg\": \"Driver Registered\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/reports",
    "title": "Driver Report",
    "name": "reports",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data",
            "description": "<p>Object Containing report data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n        \"status\": true,\n        \"data\": {\n            \"today\": {\n                \"date\": \"Apr 26, 2018\"\n                \"trips\": 1,\n                \"canceled_trips\": 0,\n                \"earnings\": \"50.00\",\n                \"tips\": \"0.00\",\n                \"guarantee\": 0,\n                \"cash_collected\": 0,\n                \"adjustments\": 0,\n                \"wallet\": 0,\n                \"rating\": 5,\n                \"acceptance\": 0,\n                \"availability\": 0\n            },\n            \"current_cycle\": {\n                \"date\": \"Apr 19, 2018 - Apr 25, 2018\"\n                \"trips\": 2,\n                \"canceled_trips\": 0,\n                \"earnings\": \"1,365.00\",\n                \"tips\": \"0.00\",\n                \"guarantee\": 0,\n                \"cash_collected\": 0,\n                \"adjustments\": 0,\n                \"wallet\": 0,\n                \"rating\": 5,\n                \"acceptance\": 0,\n                \"availability\": 0\n            },\n            \"previous_cycle\": {\n                \"date\": \"Apr 12, 2018 - Apr 18, 2018\"\n                \"trips\": 19,\n                \"canceled_trips\": 0,\n                \"earnings\": \"115.00\",\n                \"tips\": \"0.00\",\n                \"guarantee\": 0,\n                \"cash_collected\": 0,\n                \"adjustments\": 0,\n                \"wallet\": 0,\n                \"rating\": 5,\n                \"acceptance\": 0,\n                \"availability\": 0\n            }\n        },\n        \"msg\": \"\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/reservations",
    "title": "Driver Future Reservations",
    "name": "reservations",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.driving_license",
            "description": "<p>Driving License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.insurance_certificate",
            "description": "<p>Insurance Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.inspection_certificate",
            "description": "<p>Inspection Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.taxi_association_number",
            "description": "<p>Taxi Association Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.private_hire_vehicle_license",
            "description": "<p>Private Hire Vehicle License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n        {\n            \"id\": \"68\",\n            \"driver_id\": \"17\",\n            \"passenger_id\": \"1\",\n            \"channel\": \"channel_1522171215305\",\n            \"vehicle_type_id\": \"1\",\n            \"passengers\": \"2\",\n            \"pickup_coordinates\": \"24.9090218,67.0825462\",\n            \"pickup_location\": \"Gulshan-e-Iqbal, Karachi, Pakistan\",\n            \"dropoff_coordinates\": \"24.921883,67.065521\",\n            \"dropoff_location\": \"Aisha Manzil, Karachi, Pakistan\",\n            \"payment_method\": \"1\",\n            \"ride_type\": \"2\",\n            \"is_accepted\": \"1\",\n            \"is_completed\": \"0\",\n            \"is_cancelled\": \"0\",\n            \"is_delete\": \"0\",\n            \"date_add\": \"2018-03-27 17:19:54\",\n            \"date_update\": \"2018-03-27 17:20:15\",\n            \"driver\": {\n                \"id\": \"17\",\n                \"firstname\": \"Uzair\",\n                \"lastname\": \"Noman\",\n                \"email\": \"uzairation@gmail.com\",\n                \"country_id\": \"2\",\n                \"mobile\": \"+923473556114\",\n                \"state_id\": \"1\",\n                \"city_id\": \"1\",\n                \"password\": \"16d7a4fca7442dda3ad93c9a726597e4\",\n                \"photo\": \"http://45.55.174.194/nvoii_manager/assets/uploads/drivers/588b11ad7a92d13777fe0be3adf633bf_1520155848.jpg\",\n                \"invite_code\": \"\",\n                \"reset_code\": null,\n                \"is_busy\": \"0\",\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"2018-02-08 16:43:49\",\n                \"date_update\": \"2018-02-08 16:43:49\",\n                \"date_registered\": \"2018-02-08 16:43:49\"\n            },\n            \"passenger\": {\n                \"id\": \"1\",\n                \"firstname\": \"Muzammil\",\n                \"lastname\": \"Versiani\",\n                \"email\": \"versiani.muzammil@gmail.com\",\n                \"country_id\": \"166\",\n                \"mobile\": \"03243367335\",\n                \"photo\": \"http://45.55.174.194/nvoii_manager/assets/uploads/passengers/default.png\",\n                \"code\": null,\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"0000-00-00 00:00:00\",\n                \"date_update\": \"0000-00-00 00:00:00\",\n                \"date_registered\": \"2017-11-17 11:15:51\"\n            },\n            \"vehicle_type\": {\n                \"id\": \"1\",\n                \"name\": \"NVOII\",\n                \"tagline\": \"Travel with ease\",\n                \"description\": \"\",\n                \"image\": \"http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png\",\n                \"country_id\": \"166\",\n                \"max_capacity\": \"3\",\n                \"base_fare\": \"5.00\",\n                \"minimum_fare\": \"3.00\",\n                \"per_mile_fare\": \"5.00\",\n                \"per_minute_fare\": \"5.00\",\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"2018-03-20 15:50:42\",\n                \"date_update\": \"2018-03-20 15:50:42\"\n            },\n            \"payment_method_info\": {\n                \"id\": \"1\",\n                \"payment_method\": \"Cash\",\n                \"is_active\": \"1\"\n            },\n            \"ride_type_text\": \"Later\",\n            \"history\": [\n                {\n                    \"id\": \"85\",\n                    \"ride_id\": \"68\",\n                    \"status\": \"1\",\n                    \"datetime\": \"2018-03-27 17:19:54\"\n                },\n                {\n                    \"id\": \"86\",\n                    \"ride_id\": \"68\",\n                    \"status\": \"2\",\n                    \"datetime\": \"2018-03-27 17:20:15\"\n                },\n                {\n                    \"id\": \"87\",\n                    \"ride_id\": \"68\",\n                    \"status\": \"3\",\n                    \"datetime\": \"2018-03-27 17:21:06\"\n                },\n                {\n                    \"id\": \"88\",\n                    \"ride_id\": \"68\",\n                    \"status\": \"4\",\n                    \"datetime\": \"2018-03-27 17:21:36\"\n                },\n                {\n                    \"id\": \"89\",\n                    \"ride_id\": \"68\",\n                    \"status\": \"5\",\n                    \"datetime\": \"2018-03-27 17:21:43\"\n                }\n            ]\n        }\n    ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/reset",
    "title": "Reset Password",
    "name": "reset",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>Reset Code that Driver received from <a href=\"#api-Driver-forgot\">Forgot Password API</a></p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Driver New Password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data Object.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Password has been reset successfully. Please login with your new password.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/send_code",
    "title": "Send Verification code to Driver's phone",
    "name": "send_code",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Driver mobile number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Please enter the code received via SMS.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 403 NotAllowed": [
          {
            "group": "Error 403 NotAllowed",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/update_document",
    "title": "Update Driver Document",
    "name": "update_document",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "document_type",
            "description": "<p>Document Type (Valid types: driving_license, insurance_certificate, inspection_certificate, taxi_association_number OR private_hire_vehicle_license).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "document",
            "description": "<p>Driver Document (Image Object).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing driver document.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.document",
            "description": "<p>Driver Document URL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"document\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.pdf\"\n  },\n  \"msg\": \"Document has been updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidDocument": [
          {
            "group": "Error 404 InvalidDocument",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidDocument",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidDocument",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/update_option",
    "title": "Update Driver Option",
    "name": "update_option",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "option_key",
            "description": "<p>Option Key (Valid keys: booking_alert, pickup_alert, contact_sync).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "option_value",
            "description": "<p>Option Value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_key",
            "description": "<p>Option Key.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_value",
            "description": "<p>Option Value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"option_key\": \"booking_alert\",\n          \"option_value\": \"1\"\n      },\n      {\n          \"option_key\": \"pickup_alert\",\n          \"option_value\": \"1\"\n      },\n      {\n          \"option_key\": \"contact_sync\",\n          \"option_value\": \"1\"\n      }\n  ],\n  \"msg\": \"Option has been updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidOption": [
          {
            "group": "Error 404 InvalidOption",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/update_photo",
    "title": "Update Driver Photo",
    "name": "update_photo",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "photo",
            "description": "<p>Driver Photo (Image Object).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing driver new photo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.photo",
            "description": "<p>Driver Photo URL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"photo\": \"http://localhost/ridenow_manager/assets/uploads/drivers/default.png\"\n  },\n  \"msg\": \"Photo has been updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidImage": [
          {
            "group": "Error 404 InvalidImage",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidImage",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidImage",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/update_status",
    "title": "Update Driver Status",
    "name": "update_status",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "description": "<p>Driver Status (0 = Offline, 1 = Online).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Operation performed successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 ErrorOccurred": [
          {
            "group": "Error 404 ErrorOccurred",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 ErrorOccurred",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 ErrorOccurred",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/driver/vehicle",
    "title": "Driver Vehicle Info",
    "name": "vehicle",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "driver_id",
            "description": "<p>Driver ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Object containing Vehicle Info.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.plate_number",
            "description": "<p>Vehicle Plate Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.make",
            "description": "<p>Vehicle Make</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.model",
            "description": "<p>Vehicle Model</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.year",
            "description": "<p>Vehicle Year</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.color",
            "description": "<p>Vehicle Color</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"plate_number\": \"TXI1578\",\n      \"make\": \"Toyota\",\n      \"model\": \"Corolla\",\n      \"year\": \"2010\",\n      \"color\": \"Black\"\n  },\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "post",
    "url": "/v1/driver/verify_code",
    "title": "Verify Code",
    "name": "verify_code",
    "description": "<p>This API verifies the code and returns Driver's info.</p>",
    "group": "Driver",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Driver mobile number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>Verification code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing Passenger's info.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"id\": \"3\",\n      \"firstname\": \"Driver\",\n      \"lastname\": \"Tofel\",\n      \"email\": \"tofotofo@gmail.com\",\n      \"country_id\": \"231\",\n      \"mobile\": \"123456789\",\n      \"state_id\": \"3919\",\n      \"city_id\": \"42594\",\n      \"password\": \"091faf34f3ebc004beef1a1253504f52\",\n      \"photo\": \"http://localhost/ridenow_manager/assets/uploads/drivers/default.png\",\n      \"taxi_association_number\": \"ABC3476\",\n      \"invite_code\": \"CXN5QA\",\n      \"is_active\": \"1\",\n      \"is_delete\": \"0\",\n      \"date_add\": \"2017-11-20 11:50:41\",\n      \"date_update\": \"2017-11-20 12:05:20\",\n      \"date_registered\": \"2017-11-20 12:05:20\",\n      \"documents\": {\n          \"driving_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_7459e30acd4362e2ae62ac4cf9551ddd_1511179318.doc\",\n          \"insurance_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n           \"inspection_certificate\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/3_a72377bbe3a3f1988485d0e9631b6c82_1511179520.doc\",\n           \"taxi_association_number\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\",\n          \"private_hire_vehicle_license\": \"http://localhost/ridenow_manager/assets/uploads/drivers/doc/\"\n      },\n      \"options\": [\n          {\n              \"option_key\": \"booking_alert\",\n             \"option_value\": \"1\"\n           },\n          {\n              \"option_key\": \"pickup_alert\",\n              \"option_value\": \"1\"\n          },\n          {\n              \"option_key\": \"contact_sync\",\n             \"option_value\": \"1\"\n          }\n      ],\n      \"rating\": 5,\n      \"vehicle\": {\n          \"plate_number\": \"TXI1578\",\n          \"make\": \"Toyota\",\n          \"model\": \"Corolla\",\n          \"year\": \"2010\",\n          \"color\": \"Black\"\n      }\n  },\n  \"msg\": \"Code verified.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_driver.js",
    "groupTitle": "Driver"
  },
  {
    "type": "get",
    "url": "/v1/localization/cities",
    "title": "Get Cities",
    "name": "cities",
    "group": "Localization",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "state_id",
            "description": "<p>State ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Data Array containing cities.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>City ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>City Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.state_id",
            "description": "<p>State ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"31540\",\n          \"name\": \"Adilpur\",\n          \"state_id\": \"2729\"\n      },\n      {\n          \"id\": \"31541\",\n          \"name\": \"Badah\",\n          \"state_id\": \"2729\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_localization.js",
    "groupTitle": "Localization"
  },
  {
    "type": "get",
    "url": "/v1/localization/cities_by_country",
    "title": "Get Cities By Country",
    "name": "cities_by_country",
    "group": "Localization",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data Array containing cities.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.country_id",
            "description": "<p>Country ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.country_name",
            "description": "<p>Country Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.phonecode",
            "description": "<p>Country Phonecode</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.flag",
            "description": "<p>Country Flag</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data.cities",
            "description": "<p>Array of Cities</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.cities.id",
            "description": "<p>City ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.cities.name",
            "description": "<p>City Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.cities.state_id",
            "description": "<p>State ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\":\n  \"country_id\": \"166\",\n    \"country_name\": \"Pakistan\",\n    \"phonecode\": \"+92\",\n    \"flag\": \"http://45.55.174.194/nvoii_manager/assets/img/flags/+92.png\",\n    \"cities\": [\n      {\n          \"id\": \"31540\",\n          \"name\": \"Adilpur\",\n          \"state_id\": \"2729\"\n      },\n      {\n          \"id\": \"31541\",\n          \"name\": \"Badah\",\n          \"state_id\": \"2729\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_localization.js",
    "groupTitle": "Localization"
  },
  {
    "type": "get",
    "url": "/v1/localization/states",
    "title": "Get States",
    "name": "states",
    "group": "Localization",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>Country ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Data Array containing states.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>State ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>State Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.country_id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"2723\",\n          \"name\": \"Baluchistan\",\n          \"country_id\": \"166\"\n      },\n      {\n          \"id\": \"2724\",\n          \"name\": \"Federal Capital Area\",\n          \"country_id\": \"166\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_localization.js",
    "groupTitle": "Localization"
  },
  {
    "type": "get",
    "url": "/v1/driver/booking_history",
    "title": "Passenger Booking History",
    "name": "booking_history",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.driving_license",
            "description": "<p>Driving License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.insurance_certificate",
            "description": "<p>Insurance Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.inspection_certificate",
            "description": "<p>Inspection Certificate</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.taxi_association_number",
            "description": "<p>Taxi Association Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.private_hire_vehicle_license",
            "description": "<p>Private Hire Vehicle License</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"past\": [\n        {\n            \"id\": \"22\",\n            \"driver_id\": \"17\",\n            \"passenger_id\": \"1\",\n            \"channel\": \"channel_1521129275355\",\n            \"vehicle_type_id\": \"1\",\n            \"passengers\": \"2\",\n            \"pickup_coordinates\": \"24.9090218,67.0825462\",\n            \"pickup_location\": \"Gulshan-e-Iqbal, Karachi, Pakistan\",\n            \"dropoff_coordinates\": \"24.921883,67.065521\",\n            \"dropoff_location\": \"Aisha Manzil, Karachi, Pakistan\",\n            \"payment_method\": \"1\",\n            \"ride_type\": \"1\",\n            \"is_accepted\": \"1\",\n            \"is_completed\": \"0\",\n            \"is_cancelled\": \"0\",\n            \"is_delete\": \"0\",\n            \"date_add\": \"2018-03-15 15:54:00\",\n            \"date_update\": \"2018-03-15 15:54:35\",\n            \"driver\": {\n                \"id\": \"17\",\n                \"firstname\": \"Uzair\",\n                \"lastname\": \"Noman\",\n                \"email\": \"uzairation@gmail.com\",\n                \"country_id\": \"2\",\n                \"mobile\": \"+923473556114\",\n                \"state_id\": \"1\",\n                \"city_id\": \"1\",\n                \"password\": \"16d7a4fca7442dda3ad93c9a726597e4\",\n                \"photo\": \"http://45.55.174.194/nvoii_manager/assets/uploads/drivers/588b11ad7a92d13777fe0be3adf633bf_1520155848.jpg\",\n                \"invite_code\": \"\",\n                \"reset_code\": null,\n                \"is_busy\": \"0\",\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"2018-02-08 16:43:49\",\n                \"date_update\": \"2018-02-08 16:43:49\",\n                \"date_registered\": \"2018-02-08 16:43:49\"\n            },\n            \"passenger\": {\n                \"id\": \"1\",\n                \"firstname\": \"Muzammil\",\n                \"lastname\": \"Versiani\",\n                \"email\": \"versiani.muzammil@gmail.com\",\n                \"country_id\": \"166\",\n                \"mobile\": \"03243367335\",\n                \"photo\": \"http://45.55.174.194/nvoii_manager/assets/uploads/passengers/default.png\",\n                \"code\": null,\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"0000-00-00 00:00:00\",\n                \"date_update\": \"0000-00-00 00:00:00\",\n                \"date_registered\": \"2017-11-17 11:15:51\"\n            },\n            \"vehicle_type\": {\n                \"id\": \"1\",\n                \"name\": \"NVOII\",\n                \"tagline\": \"Travel with ease\",\n                \"description\": \"\",\n                \"image\": \"http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png\",\n                \"country_id\": \"166\",\n                \"max_capacity\": \"3\",\n                \"base_fare\": \"5.00\",\n                \"minimum_fare\": \"3.00\",\n                \"per_mile_fare\": \"5.00\",\n                \"per_minute_fare\": \"5.00\",\n                \"is_active\": \"1\",\n                \"is_delete\": \"0\",\n                \"date_add\": \"2018-03-20 15:50:42\",\n                \"date_update\": \"2018-03-20 15:50:42\"\n            },\n            \"payment_method_info\": {\n                \"id\": \"1\",\n                \"payment_method\": \"Cash\",\n                \"is_active\": \"1\"\n            },\n            \"ride_type_text\": \"Now\",\n            \"history\": [\n                {\n                    \"id\": \"22\",\n                    \"ride_id\": \"22\",\n                    \"status\": \"1\",\n                    \"datetime\": \"2018-03-15 15:54:00\"\n                },\n                {\n                    \"id\": \"23\",\n                    \"ride_id\": \"22\",\n                    \"status\": \"2\",\n                    \"datetime\": \"2018-03-15 15:54:35\"\n                }\n            ]\n        },\n        ....\n        ],\n        \"upcoming\": [],\n        \"cancelled\": []\n  },\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/cancel_ride",
    "title": "Cancel Ride",
    "name": "cancel_ride",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ride_id",
            "description": "<p>Ride ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_cancelled",
            "description": "<p>Cancel Flag.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.date_update",
            "description": "<p>Date Update.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"is_cancelled\": \"1\",\n      \"date_update\": \"2018-03-27 17:19:54\"\n  }\n  ,\n  \"msg\": \"Ride cancelled\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidOption": [
          {
            "group": "Error 404 InvalidOption",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/cancellation_reasons",
    "title": "Passenger Cancellation Reasons",
    "name": "cancellation_reasons",
    "group": "Passenger",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing reasons.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Reason ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.type",
            "description": "<p>Type ID (1=Driver, 2=Passenger).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.reason",
            "description": "<p>Reason Text.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"5\",\n          \"type\": \"2\",\n          \"reason\": \"The fare is too expensive\"\n      },\n      {\n          \"id\": \"6\",\n          \"type\": \"2\",\n          \"reason\": \"The ride has been delayed\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/cards",
    "title": "Get Passenger Credit Cards",
    "name": "cards",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing saved cards.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Card ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.card_number",
            "description": "<p>Last 4 digits of card number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.expiry_month",
            "description": "<p>Card Expiry Month</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.expiry_year",
            "description": "<p>Card Expiry Year</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.card_type",
            "description": "<p>Card Type</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.image",
            "description": "<p>Card Logo Image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n            \"id\": \"8\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n        {\n            \"id\": \"7\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/driver/device_token",
    "title": "Save/Update Device Token",
    "name": "device_token",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Device Token.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "platform",
            "description": "<p>Device Platform (Android or iOS).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>User ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": true,\n  \"msg\": \"Token saved\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 403 NotAllowed": [
          {
            "group": "Error 403 NotAllowed",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/logout",
    "title": "Passenger Logout",
    "name": "logout",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Logout successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/options",
    "title": "Passenger Options",
    "name": "options",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_key",
            "description": "<p>Option Key.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_value",
            "description": "<p>Option Value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"option_key\": \"contact_sync\",\n          \"option_value\": \"1\"\n      }\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/rate_driver",
    "title": "Passenger's rating to Driver",
    "name": "rate_driver",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ride_id",
            "description": "<p>Ride ID</p>"
          },
          {
            "group": "Parameter",
            "type": "rating",
            "optional": false,
            "field": "rating",
            "description": "<p>Rating Number from 1 to 5</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "rating_message_id",
            "description": "<p>Rating Message ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "notes",
            "description": "<p>Notes</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>NULL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Rating has been saved.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/rating",
    "title": "Passenger Rating",
    "name": "rating",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data",
            "description": "<p>Passenger Rating.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": 5,\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/rating_messages",
    "title": "Passenger Rating Messages",
    "name": "rating_messages",
    "group": "Passenger",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing reasons.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Reason ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.message",
            "description": "<p>Message Text.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.type",
            "defaultValue": "2",
            "description": "<p>Type ID (1=Driver, 2=Passenger).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": \"6\",\n          \"message\": \"Driving\",\n          \"type\": \"2\"\n      },\n      {\n          \"id\": \"7\",\n          \"message\": \"Route by NVOII\",\n          \"type\": \"2\"\n      },\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/save_card",
    "title": "Save Passenger Credit Card",
    "name": "save_card",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_number",
            "description": "<p>Card Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "expiry",
            "description": "<p>Card Expiry Date (e.g 08/2019)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cvv",
            "description": "<p>Card CVV</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>Valid Zip Code</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing saved cards.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Card ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.card_number",
            "description": "<p>Last 4 digits of card number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.expiry_month",
            "description": "<p>Card Expiry Month</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.expiry_year",
            "description": "<p>Card Expiry Year</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.card_type",
            "description": "<p>Card Type</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.cvv",
            "description": "<p>Hidden CVV</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.zip",
            "description": "<p>Zip Code</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.image",
            "description": "<p>Card Logo Image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n            \"id\": \"8\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"zip\": \"123456\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n        {\n            \"id\": \"7\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"cvv\": \"12345\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n      ...\n  ],\n  \"msg\": \"Card saved successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/save_location",
    "title": "Save passenger's location in DB",
    "name": "save_location",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>Location Label</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Location Icon</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Location Latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude",
            "description": "<p>Location Longitude</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>NULL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Location saved successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/save_location",
    "title": "Save passenger's location in DB",
    "name": "save_location",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "label",
            "description": "<p>Location Label</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "icon",
            "description": "<p>Location Icon</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "latitude",
            "description": "<p>Location Latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "longitude",
            "description": "<p>Location Longitude</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address in Text Form</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>NULL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Location saved successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "get",
    "url": "/v1/passenger/saved_locations",
    "title": "Get passenger's saved locations",
    "name": "saved_locations",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing saved locations.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Location ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.label",
            "description": "<p>Location Label</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.icon",
            "description": "<p>Location Icon</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.latitude",
            "description": "<p>Location Latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.longitude",
            "description": "<p>Location Longitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"id\": 1,\n          \"passenger_id\": 17,\n          \"icon\": \"http://localhost/ridenow_manager/assets/uploads/icons/home.png\",\n          \"label\": \"Home\",\n          \"latitude\": 24.2308273,\n          \"longitude\": 67.3453443,\n          \"address\": \"Gulshan-e-Iqbal, Karachi, Pakistan\"\n      }\n      ...\n  ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/send_code",
    "title": "Send Verification code to Passenger's phone",
    "name": "send_code",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Passenger mobile number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": null,\n  \"msg\": \"Please enter the code received via SMS.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 403 NotAllowed": [
          {
            "group": "Error 403 NotAllowed",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 403 NotAllowed",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/update_card",
    "title": "Update Passenger Credit Card",
    "name": "update_card",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>Credit Card ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_number",
            "description": "<p>Card Number</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "expiry",
            "description": "<p>Card Expiry Date (e.g 08/2019)</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cvv",
            "description": "<p>Card CVV</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zip",
            "description": "<p>Valid Zip Code</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing saved cards.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Card ID</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.passenger_id",
            "description": "<p>Passenger ID</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.card_number",
            "description": "<p>Last 4 digits of card number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.expiry_month",
            "description": "<p>Card Expiry Month</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.expiry_year",
            "description": "<p>Card Expiry Year</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.card_type",
            "description": "<p>Card Type</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.cvv",
            "description": "<p>Hidden CVV</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.zip",
            "description": "<p>Zip Code</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.image",
            "description": "<p>Card Logo Image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Response Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n            \"id\": \"8\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"zip\": \"123456\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n        {\n            \"id\": \"7\",\n            \"passenger_id\": \"1\",\n            \"card_number\": \"*5394\",\n            \"expiry_month\": \"10\",\n            \"expiry_year\": \"2022\",\n            \"card_type\": \"mastercard\",\n            \"cvv\": \"***\",\n            \"cvv\": \"12345\",\n            \"image\": \"http://localhost/ridenow_manager/assets/uploads/cards/mastercard.png\"\n        },\n      ...\n  ],\n  \"msg\": \"Card updated successfully.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/update_option",
    "title": "Update Passenger Option",
    "name": "update_option",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "option_key",
            "description": "<p>Option Key (Valid keys: contact_sync, default_payment_method (0=Cash)).</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "option_value",
            "description": "<p>Option Value.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing options.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_key",
            "description": "<p>Option Key.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.option_value",
            "description": "<p>Option Value.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      {\n          \"option_key\": \"contact_sync\",\n          \"option_value\": \"1\"\n      }\n  ],\n  \"msg\": \"Option has been updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidOption": [
          {
            "group": "Error 404 InvalidOption",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidOption",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/update_photo",
    "title": "Update Passenger Photo",
    "name": "update_photo",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "photo",
            "description": "<p>Passenger Photo (Image Object).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing driver new photo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.photo",
            "description": "<p>Driver Photo URL.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"photo\": \"http://localhost/ridenow_manager/assets/uploads/passenger/default.png\"\n  },\n  \"msg\": \"Photo has been updated\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 InvalidImage": [
          {
            "group": "Error 404 InvalidImage",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 InvalidImage",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 InvalidImage",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/update_profile",
    "title": "Update Passenger Profile",
    "name": "update_profile",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "passenger_id",
            "description": "<p>Passenger ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Passenger Firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Passenger Lastname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Passenger Email address.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing passenger info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Passenger ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.firstname",
            "description": "<p>Passenger Firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.lastname",
            "description": "<p>Passenger Lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.email",
            "description": "<p>Passenger Email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.country_id",
            "description": "<p>Passenger Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.mobile",
            "description": "<p>Passenger Mobile Number.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.photo",
            "description": "<p>Passenger Photo.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.code",
            "description": "<p>Passenger Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_active",
            "description": "<p>Passenger Status Flag (0 = In active, 1 = Active).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_delete",
            "description": "<p>Passenger Delete Flag (0 = In active, 1 = Active).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.date_add",
            "description": "<p>Add date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.date_update",
            "description": "<p>Update date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.date_registered",
            "description": "<p>Registration date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n      \"id\": \"1\",\n      \"firstname\": \"Muzammil\",\n      \"lastname\": \"Versiani\",\n      \"email\": \"versiani.muzammil@gmail.com\",\n      \"country_id\": \"166\",\n      \"mobile\": \"+923243367335\",\n      \"photo\": \"default.png\",\n      \"code\": null,\n      \"is_active\": \"1\",\n      \"is_delete\": \"0\",\n      \"date_add\": \"0000-00-00 00:00:00\",\n      \"date_update\": \"0000-00-00 00:00:00\",\n      \"date_registered\": \"2017-11-17 11:15:51\",\n      \"options\": [\n          {\n              \"option_key\": \"contact_sync\",\n              \"option_value\": \"0\"\n          }\n      ]\n  ],\n  \"msg\": \"Profile has been updated.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 EmailExists": [
          {
            "group": "Error 404 EmailExists",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 EmailExists",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 EmailExists",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/passenger/verify_code",
    "title": "Verify Code",
    "name": "verify_code",
    "description": "<p>This API verifies the code and returns Passenger's info. The API will return only <code>driver_id</code> if this is new registration otherwise passenger's complete information will be returned.</p>",
    "group": "Passenger",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>Passenger mobile number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>Verification code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": "<p>Data containing Passenger's info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_registered",
            "description": "<p>Flag to show whether passenger is registered or not</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.info",
            "description": "<p>Object with complete passenger info (if is_registered flag is TRUE).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": {\n      \"is_registered\": true,\n      \"info\": {\n          \"id\": \"1\",\n          \"firstname\": \"Muzammil\",\n          \"lastname\": \"Versiani\",\n          \"email\": \"versiani.muzammil@gmail.com\",\n          \"country_id\": \"166\",\n          \"mobile\": \"+923243367335\",\n          \"photo\": \"default.png\",\n          \"code\": null,\n          \"is_active\": \"1\",\n          \"is_delete\": \"0\",\n          \"date_add\": \"0000-00-00 00:00:00\",\n          \"date_update\": \"0000-00-00 00:00:00\",\n          \"date_registered\": \"2017-11-17 11:15:51\",\n          \"options\": [\n              {\n                  \"option_key\": \"contact_sync\",\n                  \"option_value\": \"0\"\n              }\n          ],\n          \"rating\": 5,\n          \"saved_locations\": [\n              {\n                  \"id\": 1,\n                  \"passenger_id\": 17,\n                  \"icon\": \"http://localhost/ridenow_manager/assets/uploads/icons/home.png\",\n                  \"label\": \"Home\",\n                  \"latitude\": 24.2308273,\n                  \"longitude\": 67.3453443\n              }\n              ...\n          ]\n      }\n  },\n  \"msg\": \"Code verified.\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 NotFound",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_passenger.js",
    "groupTitle": "Passenger"
  },
  {
    "type": "post",
    "url": "/v1/vehicle_type/get_all",
    "title": "Get All Vehicle Types",
    "name": "get_all",
    "group": "Vehicle_Type",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "true",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "data",
            "description": "<p>Array containing passenger info.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.id",
            "description": "<p>Vehicle Type ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>Vehicle Type Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.tagline",
            "description": "<p>Tagline.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.description",
            "description": "<p>Description.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.image",
            "description": "<p>Vehicle Type Image.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.country_id",
            "description": "<p>Country ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.max_capacity",
            "description": "<p>Max Capacity.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.base_fare",
            "description": "<p>Base Fare.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.minimum_fare",
            "description": "<p>Minimum Fare.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.per_mile_fare",
            "description": "<p>Per Mile Fare.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.per_minute_fare",
            "description": "<p>Per Minute Fare.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_active",
            "description": "<p>Type Status Flag (0 = In active, 1 = Active).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "data.is_delete",
            "description": "<p>Type Delete Flag (0 = In active, 1 = Active).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.date_add",
            "description": "<p>Add Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.date_update",
            "description": "<p>Last Update Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true,\n  \"data\": [\n        {\n            \"id\": \"1\",\n            \"name\": \"NVOII\",\n            \"tagline\": \"Travel with ease\",\n            \"description\": \"\",\n            \"image\": \"http://45.55.174.194/nvoii_manager/assets/uploads/vehicle_types/fe71c8a404ffd0c62b8af8ecaddc8619_1521561042.png\",\n            \"country_id\": \"166\",\n            \"max_capacity\": \"3\",\n            \"base_fare\": \"5.00\",\n            \"minimum_fare\": \"3.00\",\n            \"per_mile_fare\": \"5.00\",\n            \"per_minute_fare\": \"5.00\",\n            \"is_active\": \"1\",\n            \"is_delete\": \"0\",\n            \"date_add\": \"2018-03-20 15:50:42\",\n            \"date_update\": \"2018-03-20 15:50:42\"\n        }\n    ],\n  \"msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 EmailExists": [
          {
            "group": "Error 404 EmailExists",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 404 EmailExists",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 404 EmailExists",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ],
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "type": "Boolean",
            "optional": false,
            "field": "status",
            "defaultValue": "false",
            "description": "<p>Status of request.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "data",
            "defaultValue": "null",
            "description": "<p>Data.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>Error message.</p>"
          }
        ]
      }
    },
    "filename": "assets/docjs/apidoc_vehicle_type.js",
    "groupTitle": "Vehicle_Type"
  }
] });

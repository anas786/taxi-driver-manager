<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_m extends CI_Model {
    
    private $tbl = 'role';
    
	public function getAllRoles($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like('id', $search);
			$this->db->or_like('role', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		
		if(strlen($search))
        {
			$this->db->like('id', $search);
			$this->db->or_like('role', $search);
		}
		
		$cols = array(
            0 => '',
			1 => 'id',
			2 => 'role',
			7 => ''
		);
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by('id', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
		
		$this->db->select('*');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		
		//print_r($result);die;
		$i = $start;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('role/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a href="'.base_url('role/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
			
			$data[] = array($checkbox,
                            $v['id'],
							$v['role'],
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}
    
    public function getAll()
    {
        $result = $this->db->get($this->tbl);
        return $result->result_array();
    }
    
    
}

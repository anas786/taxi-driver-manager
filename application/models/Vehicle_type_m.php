<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_type_m extends CI_Model {
    
    private $tbl = 'vehicle_type';
    
	public function getAllVehicleTypes($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.name', $search);
			$this->db->or_like($this->tbl . '.tagline', $search);
			$this->db->or_like($this->tbl . '.description', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.name', $search);
            $this->db->or_like($this->tbl . '.tagline', $search);
            $this->db->or_like($this->tbl . '.description', $search);
		}
		
		$cols = array(
            0 => '',
            1 => 'id',
			2 => 'image',
			3 => 'name',
			4 => 'country',
            5 => 'is_active',
			6 => 'date_added',
            7 => ''
		);
        
        //$this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.name');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
        
        $this->db->where($this->tbl . '.is_delete', 0);
		
		$this->db->select('*');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('vehicle_type/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Vehicle Type?\');" href="'.base_url('vehicle_type/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            if( $v['is_active'] )
            {
                $active = '<span class="label label-primary">Active</span>';
            }
            else
            {
                $active = '<span class="label label-danger">Inactive</span>';
            }
            
            $photo = '<img src="'.base_url(VEHICLE_TYPE_PHOTO_PATH . $v['image']).'" alt="" width="50" class="img-responsive">';
            
            $country = $this->db->get_where('country', array('id' => $v['country_id']));
            $country = $country->row_array();
			
			$data[] = array($checkbox,
							$v['id'],
                            $photo,
							$v['name'],
							$country['name'],
							$active,
							easyDate($v['date_add']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}

    public function getAll()
    {
        $result = $this->db->get_where($this->tbl, array('is_delete' => 0));
        if( $result->num_rows() > 0 )
        {
            $vehicle_types = array();
            foreach( $result->result_array() as $vehicle_type )
            {
                $vehicle_type['image'] = base_url(VEHICLE_TYPE_PHOTO_PATH . $vehicle_type['image']);
                $vehicle_types[] = $vehicle_type;
            }
            return $vehicle_types;
        }
        return false;
    }
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
//        $this->db->join($this->tbl_doc, $this->tbl . '.id=' . $this->tbl_doc . '.driver_id');
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function getCompleteInfo($id)
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        if( $result->num_rows() > 0 ) {
            $vehicle_type = $result->row_array();
            $vehicle_type['image'] = base_url(VEHICLE_TYPE_PHOTO_PATH . $vehicle_type['image']);

            return $vehicle_type;
        }
        return false;
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('name' => $data['name']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function addDoc($data) 
    {
        if( $this->db->insert($this->tbl_doc, $data) )
        {
            return true;
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    public function editDoc($data, $where) 
    {
        if( $this->db->update($this->tbl_doc, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    
    public function editOptions($data, $where) 
    {
        $options = $this->getOptions($where['driver_id']);
        $i = 0;
        foreach( $options as $option )
        {
            if( array_key_exists($option['option_key'], $data) )
            {
                $dat = array(
                    'option_value' => $data[$option['option_key']]
                );
                $where = array(
                    'driver_id' => $where['driver_id'],
                    'option_key' => $option['option_key']
                );
                $this->db->update($this->tbl_option, $dat, $where);
            }
            $i++;
        }
        return $this->getOptions($where['driver_id']);
    }
    
    public function editStatus($status, $where) 
    {
        if( $status == 1 )
        {
            // Go Online
            $data = $where;
            if( $this->db->insert($this->tbl_online, $data) ) {
                return true;
            }
        }
        else
        {
            // Go Offline
            if( $this->db->delete($this->tbl_online, $where) ) {
                return true;
            }
        }
        return false;
    }
    
    
    private function generateResetCode()
    {
        $reset_code = '';
        for( $i = 1; $i <= 5; $i++ )
        {
            $reset_code .= mt_rand(0, 9);
        }
        return $reset_code;
    }
    
}

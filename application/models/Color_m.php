<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Color_m extends CI_Model {
    
    private $tbl = 'color';

    public function getColors()
    {
        $this->db->order_by('name', 'ASC');
        $result = $this->db->get($this->tbl);
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }
    
	public function getAllColors($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.year', $search);
			$this->db->or_like($this->tbl . '.make', $search);
			$this->db->or_like($this->tbl . '.model', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.year', $search);
            $this->db->or_like($this->tbl . '.make', $search);
            $this->db->or_like($this->tbl . '.model', $search);
		}
		
		$cols = array(
            0 => '',
            1 => '',
			2 => 'make',
			3 => 'model',
			4 => 'year',
            5 => 'is_active',
            6 => ''
		);
        
        //$this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.make', 'ASC');
			$this->db->order_by($this->tbl . '.model', 'ASC');
			$this->db->order_by($this->tbl . '.year', 'ASC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
		
		$this->db->select('*');

		$this->db->where($this->tbl . '.is_delete', 0);
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = 1;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('manager/car/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Car?\');" href="'.base_url('manager/car/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';

            $is_active = '<span class="label label-danger">Inactive</span>';
            if( $v['is_active'] == 1 ) {
                $is_active = '<span class="label label-success">Active</span>';
            }
			
			$data[] = array($checkbox,
							$i,
							$v['make'],
							$v['model'],
                            $v['year'],
							$is_active,
							$buttons
						);
			$i++;
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('make' => $data['make'], 'model' => $data['model'], 'year' => $data['year']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }

}

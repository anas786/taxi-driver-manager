<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_m extends CI_Model {
    
    private $tbl = 'driver_vehicle';
    private $tbl_make_model = 'vehicle_make_model';
    private $tbl_driver = 'driver';
    private $tbl_vehicle_type = 'vehicle_type';

	public function getAllVehicles($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
            $this->db->group_start();
			$this->db->like($this->tbl . '.id', $search);
            $this->db->like($this->tbl . '.plate_number', $search);
			$this->db->or_like($this->tbl_make_model . '.make', $search);
			$this->db->or_like($this->tbl_make_model . '.model', $search);
			$this->db->or_like($this->tbl_make_model . '.year', $search);
			$this->db->or_like($this->tbl_driver . '.firstname', $search);
			$this->db->or_like($this->tbl_driver . '.lastname', $search);
			$this->db->or_like($this->tbl_vehicle_type . '.name', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_delete', 0);

            $this->db->join($this->tbl_make_model, $this->tbl_make_model. '.id=' . $this->tbl . '.vehicle_make_model_id');
            $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
            $this->db->join($this->tbl_vehicle_type, $this->tbl_vehicle_type. '.id=' . $this->tbl . '.vehicle_type_id');

		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
            $this->db->group_start();
            $this->db->like($this->tbl . '.id', $search);
            $this->db->like($this->tbl . '.plate_number', $search);
            $this->db->or_like($this->tbl_make_model . '.make', $search);
            $this->db->or_like($this->tbl_make_model . '.model', $search);
            $this->db->or_like($this->tbl_make_model . '.year', $search);
            $this->db->or_like($this->tbl_driver . '.firstname', $search);
            $this->db->or_like($this->tbl_driver . '.lastname', $search);
            $this->db->or_like($this->tbl_vehicle_type . '.name', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_delete', 0);
		}
		
		$cols = array(
            0 => '',
            1 => 'id',
			2 => 'driver_name',
			3 => 'vehicle_name',
			4 => 'plate_number',
            5 => 'is_active',
			6 => 'date_added',
            7 => ''
		);

        $this->db->join($this->tbl_make_model, $this->tbl_make_model. '.id=' . $this->tbl . '.vehicle_make_model_id');
        $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
        $this->db->join($this->tbl_vehicle_type, $this->tbl_vehicle_type. '.id=' . $this->tbl . '.vehicle_type_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.date_add', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
        
        $this->db->where($this->tbl . '.is_delete', 0);
		
		$this->db->select($this->tbl . '.*, ' . $this->tbl_make_model . '.make,' . $this->tbl_make_model . '.model,' . $this->tbl_make_model . '.year,' . $this->tbl_vehicle_type . '.name, ' . $this->tbl_driver . '.firstname,' . $this->tbl_driver . '.lastname,' . $this->tbl_driver . '.id AS `driver_id`');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('vehicle/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Vehicle?\');" href="'.base_url('vehicle/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            if( $v['is_active'] )
            {
                $active = '<span class="label label-primary">Active</span>';
            }
            else
            {
                $active = '<span class="label label-danger">Inactive</span>';
            }
			
			$data[] = array($checkbox,
							$v['id'],
                            $v['lastname'] . ' ' . $v['firstname'],
							$v['make'] . ' ' . $v['model'] . ' ' . $v['year'],
							$v['plate_number'],
							$active,
							easyDate($v['date_add']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function getCompleteInfo($id)
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        if( $result->num_rows() > 0 ) {
            $vehicle_type = $result->row_array();
            $vehicle_type['image'] = base_url(VEHICLE_TYPE_PHOTO_PATH . $vehicle_type['image']);

            return $vehicle_type;
        }
        return false;
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('driver_id' => $data['driver_id']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }

    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_m extends CI_Model {
    
    private $tbl = 'user';
    
	public function getAllUsers($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.name', $search);
			$this->db->or_like($this->tbl . '.email', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.name', $search);
			$this->db->or_like($this->tbl . '.email', $search);
		}
		
		$cols = array(
            0 => '',
			1 => 'id',
			2 => 'name',
			3 => 'email',
			4 => 'role',
			5 => 'is_active',
			6 => 'date_add',
			7 => ''
		);
        
        $this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.id', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
        
        $this->db->where($this->tbl . '.is_delete', 0);
		
		$this->db->select($this->tbl . '.*, role.role');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = $start;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('user/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this User?\');" href="'.base_url('user/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            if( $v['is_active'] )
            {
                $active = '<span class="label label-primary">Active</span>';
            }
            else
            {
                $active = '<span class="label label-danger">Inactive</span>';
            }
			
			$data[] = array($checkbox,
                            $v['id'],
							$v['name'],
							$v['email'],
							$v['role'],
							$active,
							easyDateTime($v['date_add']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}
    
    public function getInfo($id) 
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        return $result->row_array();
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('email' => $data['email']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ride_m extends CI_Model {
    
    private $tbl = 'ride';
    private $tbl_route = 'ride_route';
    private $tbl_status = 'ride_status';
    private $tbl_passenger = 'passenger';
    private $tbl_driver = 'driver';
    private $tbl_vehicle_type = 'vehicle_type';
    private $tbl_payment_method = 'payment_method';
    private $tbl_ride_rating = 'ride_rating';
    private $tbl_rating_message = 'rating_message';

	public function getAllCompletedRides($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
            $this->db->group_start();
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.passengers', $search);
			$this->db->or_like($this->tbl . '.pickup_location', $search);
			$this->db->or_like($this->tbl . '.dropoff_location', $search);
			$this->db->or_like($this->tbl_passenger . '.firstname', $search);
            $this->db->or_like($this->tbl_passenger . '.lastname', $search);
            $this->db->or_like($this->tbl_driver . '.firstname', $search);
            $this->db->or_like($this->tbl_driver . '.lastname', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_completed', 1);
            $this->db->where($this->tbl . '.is_delete', 0);

            $this->db->join($this->tbl_passenger, $this->tbl_passenger . '.id=' . $this->tbl . '.passenger_id');
            $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
            $this->db->join($this->tbl_status, $this->tbl_status. '.ride_id=' . $this->tbl . '.id');
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
            $this->db->group_start();
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.passengers', $search);
            $this->db->or_like($this->tbl . '.pickup_location', $search);
            $this->db->or_like($this->tbl . '.dropoff_location', $search);
            $this->db->or_like($this->tbl_passenger . '.firstname', $search);
            $this->db->or_like($this->tbl_passenger . '.lastname', $search);
            $this->db->or_like($this->tbl_driver . '.firstname', $search);
            $this->db->or_like($this->tbl_driver . '.lastname', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_completed', 1);
		}
		
		$cols = array(
            0 => '',
            1 => 'id',
			2 => 'driver_name',
			3 => 'passenger_name',
			4 => 'pickup_location',
			5 => 'dropoff_location',
            6 => 'passengers',
			7 => 'complete_date',
            8 => ''
		);
        
        $this->db->join($this->tbl_passenger, $this->tbl_passenger . '.id=' . $this->tbl . '.passenger_id');
        $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
        $this->db->join($this->tbl_status, $this->tbl_status. '.ride_id=' . $this->tbl . '.id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl_status . '.datetime', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }

        $this->db->group_by($this->tbl_status . '.ride_id');
        
        $this->db->where($this->tbl . '.is_delete', 0);
        $this->db->where($this->tbl . '.is_completed', 1);

		$this->db->select($this->tbl . '.*,' . $this->tbl_status . '.status,' . $this->tbl_status . '.datetime AS `complete_date`,' . $this->tbl_driver . '.id AS `driver_id`,' . $this->tbl_driver . '.firstname AS `driver_first_name`,' . $this->tbl_driver . '.lastname AS `driver_last_name`,' . $this->tbl_passenger . '.id AS `passenger_id`,' . $this->tbl_passenger . '.firstname AS `passenger_first_name`,' . $this->tbl_passenger . '.lastname AS `passenger_last_name`');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = $start;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
            $buttons = '<a href="'.base_url('ride/detail/' . $v['id']).'" class="btn btn-info"><i class="fa fa-search"></i></a>&nbsp;';
			$buttons .= '<a href="'.base_url('ride/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Ride?\');" href="'.base_url('ride/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            $driver_name = '<a target="_blank" href="'.base_url('driver/edit/' . $v['driver_id']).'">' . $v['driver_last_name'] . ' ' . $v['driver_first_name'] . '</a>';
            $passenger_name = '<a target="_blank" href="'.base_url('passenger/edit/' . $v['passenger_id']).'">' . $v['passenger_last_name'] . ' ' . $v['passenger_first_name'] . '</a>';

			$data[] = array($checkbox,
                            $v['id'],
							$driver_name,
                            $passenger_name,
							$v['pickup_location'],
							$v['dropoff_location'],
							$v['passengers'],
							easyDateTime($v['complete_date']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}

    public function getAllOngoingRides($draw, $start, $length, $search, $order)
    {
        if(strlen($search))
        {
            $this->db->group_start();
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.passengers', $search);
            $this->db->or_like($this->tbl . '.pickup_location', $search);
            $this->db->or_like($this->tbl . '.dropoff_location', $search);
            $this->db->or_like($this->tbl_passenger . '.firstname', $search);
            $this->db->or_like($this->tbl_passenger . '.lastname', $search);
            $this->db->or_like($this->tbl_driver . '.firstname', $search);
            $this->db->or_like($this->tbl_driver . '.lastname', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_accepted', 1);
            $this->db->where($this->tbl . '.is_completed', 0);
            $this->db->where($this->tbl . '.is_cancelled', 0);
            $this->db->where($this->tbl . '.is_delete', 0);

            $this->db->join($this->tbl_passenger, $this->tbl_passenger . '.id=' . $this->tbl . '.passenger_id');
            $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
            $this->db->join($this->tbl_status, $this->tbl_status. '.ride_id=' . $this->tbl . '.id');
        }

        $recordsTotal = $this->db->count_all_results($this->tbl);

        if(strlen($search))
        {
            $this->db->group_start();
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.passengers', $search);
            $this->db->or_like($this->tbl . '.pickup_location', $search);
            $this->db->or_like($this->tbl . '.dropoff_location', $search);
            $this->db->or_like($this->tbl_passenger . '.firstname', $search);
            $this->db->or_like($this->tbl_passenger . '.lastname', $search);
            $this->db->or_like($this->tbl_driver . '.firstname', $search);
            $this->db->or_like($this->tbl_driver . '.lastname', $search);
            $this->db->group_end();
            $this->db->where($this->tbl . '.is_accepted', 1);
            $this->db->where($this->tbl . '.is_completed', 0);
            $this->db->where($this->tbl . '.is_cancelled', 0);
        }

        $cols = array(
            0 => '',
            1 => 'id',
            2 => 'driver_name',
            3 => 'passenger_name',
            4 => 'pickup_location',
            5 => 'dropoff_location',
            6 => 'passengers',
            7 => 'request_date',
            8 => ''
        );

        $this->db->join($this->tbl_passenger, $this->tbl_passenger . '.id=' . $this->tbl . '.passenger_id');
        $this->db->join($this->tbl_driver, $this->tbl_driver . '.id=' . $this->tbl . '.driver_id');
        $this->db->join($this->tbl_status, $this->tbl_status. '.ride_id=' . $this->tbl . '.id');

        if(!empty($order))
        {
            foreach($order as $row)
            {
                $this->db->order_by($cols[$row['column']], $row['dir']);
            }
        }
        else
        {
            $this->db->order_by($this->tbl_status . '.datetime', 'DESC');
        }

        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }

        $this->db->group_by($this->tbl_status . '.ride_id');

        $this->db->where($this->tbl . '.is_delete', 0);
        $this->db->where($this->tbl . '.is_accepted', 1);
        $this->db->where($this->tbl . '.is_completed', 0);
        $this->db->where($this->tbl . '.is_cancelled', 0);

        $this->db->select($this->tbl . '.*,' . $this->tbl_status . '.status,' . $this->tbl_driver . '.id AS `driver_id`,' . $this->tbl_driver . '.firstname AS `driver_first_name`,' . $this->tbl_driver . '.lastname AS `driver_last_name`,' . $this->tbl_passenger . '.id AS `passenger_id`,' . $this->tbl_passenger . '.firstname AS `passenger_first_name`,' . $this->tbl_passenger . '.lastname AS `passenger_last_name`');

        $result = $this->db->get($this->tbl);
        $result = $result->result_array();

        $data = array();
        $i = $start;
        foreach ($result as $key => $v)
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
            $buttons = '<a href="'.base_url('ride/detail/' . $v['id']).'" class="btn btn-info"><i class="fa fa-search"></i></a>&nbsp;';
            $buttons .= '<a href="'.base_url('ride/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Ride?\');" href="'.base_url('ride/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';

            $driver_name = '<a target="_blank" href="'.base_url('driver/edit/' . $v['driver_id']).'">' . $v['driver_last_name'] . ' ' . $v['driver_first_name'] . '</a>';
            $passenger_name = '<a target="_blank" href="'.base_url('passenger/edit/' . $v['passenger_id']).'">' . $v['passenger_last_name'] . ' ' . $v['passenger_first_name'] . '</a>';

            $data[] = array($checkbox,
                $v['id'],
                $driver_name,
                $passenger_name,
                $v['pickup_location'],
                $v['dropoff_location'],
                $v['passengers'],
                easyDateTime($v['date_add']),
                $buttons
            );
        }

        $data = array(
            'draw'	=>	$draw,
            'recordsFiltered'	=>	$recordsTotal,
            'recordsTotal'		=>	$recordsTotal,
            'data'				=>	$data
        );

        return $data;
    }
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function getRidesByPassengerID($passenger_id)
    {
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get_where($this->tbl, array( 'passenger_id' => $passenger_id, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $rides = array();
            $rides['past'] = array();
            $rides['upcoming'] = array();
            $rides['cancelled'] = array();
            foreach( $result->result_array() as $ride )
            {
                if( $ride['ride_type'] == 1 && $ride['is_accepted'] == 1 && $ride['is_cancelled'] == 0 )
                {
                    $rides['past'][] = $this->getCompleteInfo($ride['id']);
                }
                else if( $ride['ride_type'] == 2 && $ride['is_accepted'] == 1 && $ride['is_cancelled'] == 0 )
                {
                    $rides['upcoming'][] = $this->getCompleteInfo($ride['id']);
                }
                else if( $ride['is_accepted'] == 1 && $ride['is_cancelled'] == 1 )
                {
                    $rides['cancelled'][] = $this->getCompleteInfo($ride['id']);
                }
            }
            return $rides;
        }
        return false;
    }

    public function getDriverReservations($driver_id)
    {
        $result = $this->db->get_where($this->tbl, array( 'driver_id' => $driver_id, 'ride_type' => 2, 'is_cancelled' => 0, 'is_completed' => 0, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $reservations = array();
            foreach( $result->result_array() as $reservation )
            {
                $reservations[] = $this->getCompleteInfo($reservation['id']);
            }
            return $reservations;
        }
        return false;
    }

    public function getCompleteInfo($id)
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        if( $result->num_rows() > 0 ) {
            $ride = $result->row_array();

            // Driver Info
            $result = $this->db->get_where($this->tbl_driver, array('id' => $ride['driver_id']));
            $driver = $result->row_array();
            $driver['photo'] = base_url(DRIVER_PHOTO_PATH . $driver['photo']);
            $ride['driver'] = $driver;

            // Passenger Info
            $result = $this->db->get_where($this->tbl_passenger, array('id' => $ride['passenger_id']));
            $passenger = $result->row_array();
            $passenger['photo'] = base_url(PASSENGER_PHOTO_PATH . $passenger['photo']);
            $ride['passenger'] = $passenger;

            // Vehicle Type Info
            $result = $this->db->get_where($this->tbl_vehicle_type, array('id' => $ride['vehicle_type_id']));
            $vehicle_type = $result->row_array();
            $vehicle_type['image'] = base_url(VEHICLE_TYPE_PHOTO_PATH . $vehicle_type['image']);
            $ride['vehicle_type'] = $vehicle_type;

            // Payment Method Info
            $result = $this->db->get_where($this->tbl_payment_method, array('id' => $ride['payment_method']));
            $payment_method = $result->row_array();
            $ride['payment_method_info'] = $payment_method;

            // Check Ride Type
            if( $ride['ride_type'] == 1 ) {
                $ride['ride_type_text'] = 'Now';
            } else if( $ride['ride_type'] == 2 ) {
                $ride['ride_type_text'] = 'Later';
            }

            // Ride Statuses
            $result = $this->db->get_where($this->tbl_status, array('ride_id' => $ride['id']));
            $ride['history'] = $result->result_array();

            // Ride Ratings
            $this->db->select($this->tbl_ride_rating . '.*, ' . $this->tbl_rating_message . '.message');
            $this->db->from($this->tbl_ride_rating);
            $this->db->join($this->tbl_rating_message, $this->tbl_ride_rating . '.rating_message_id=' . $this->tbl_rating_message . '.id', 'left');
            $this->db->where($this->tbl_ride_rating . '.ride_id', $ride['id']);
            $result = $this->db->get();
            foreach( $result->result_array() as $ride_rating )
            {
                if( $ride_rating['type'] == 1 ) {
                    // Ratings given by driver
                    $ride['driver_rating'] = $ride_rating;
                } else {
                    // Ratings given by passenger
                    $ride['passenger_rating'] = $ride_rating;
                }
            }

            return $ride;
        }
        return false;
    }

    public function rateRide($data)
    {
        if( $this->db->insert($this->tbl_ride_rating, $data) )
        {
            return true;
        }
        return false;
    }


    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('email' => $data['email']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function addDoc($data) 
    {
        if( $this->db->insert($this->tbl_doc, $data) )
        {
            return true;
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    public function editDoc($data, $where) 
    {
        if( $this->db->update($this->tbl_doc, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    
    public function editOptions($data, $where) 
    {
        $options = $this->getOptions($where['driver_id']);
        $i = 0;
        foreach( $options as $option )
        {
            if( array_key_exists($option['option_key'], $data) )
            {
                $dat = array(
                    'option_value' => $data[$option['option_key']]
                );
                $where = array(
                    'driver_id' => $where['driver_id'],
                    'option_key' => $option['option_key']
                );
                $this->db->update($this->tbl_option, $dat, $where);
            }
            $i++;
        }
        return $this->getOptions($where['driver_id']);
    }
    
    public function editStatus($status, $where) 
    {
        if( $status == 1 )
        {
            // Go Online
            $data = $where;
            if( $this->db->insert($this->tbl_online, $data) ) {
                return true;
            }
        }
        else
        {
            // Go Offline
            if( $this->db->delete($this->tbl_online, $where) ) {
                return true;
            }
        }
        return false;
    }
    
    
    private function generateResetCode()
    {
        $reset_code = '';
        for( $i = 1; $i <= 5; $i++ )
        {
            $reset_code .= mt_rand(0, 9);
        }
        return $reset_code;
    }
    
}

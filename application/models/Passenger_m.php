<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passenger_m extends CI_Model {
    
    private $tbl = 'passenger';
    private $tbl_device_token = 'device_token';
    private $tbl_option = 'passenger_option';
    private $tbl_passenger_saved_location = 'passenger_saved_location';
    private $tbl_cancellation_reason = 'cancellation_reason';
    private $tbl_rating_message = 'rating_message';
    private $tbl_passenger_card = 'passenger_card';
    private $tbl_online_user = 'online_user';

    public function login($email, $password)
	{
        $result = $this->db->get_where($this->tbl, array('email' => $email, 'password' => md5($password), 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $passenger = $result->row_array();
            $passenger['photo'] = base_url(PASSENGER_PHOTO_PATH . $passenger['photo']);
            // Ratings
            $passenger['rating'] = $this->getRatings($passenger['id']);
            return $passenger;
        }
        return false;
	}
    
    public function verifyMobileAndGenerateCode($mobile)
    {
        $result = $this->db->get_where($this->tbl, array('mobile' => $mobile, 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $row = $result->row_array();

            // Check whether user is already logged in
            $result = $this->db->get_where($this->tbl_online_user, array('user_id' => $row['id'], 'type' => 2));
            if( $result->num_rows() == 0 )
            {
                $code = $this->generateCode();
                $data = array(
                    'code' => $code
                );
                if( $this->db->update($this->tbl, $data, array('id' => $row['id'])) )
                {
                    return $code;
                }
            }
            else
            {
                return 1;
            }

        } else {
            // Create account
            $code = $this->generateCode();
            $data = array(
                'mobile' => $mobile,
                'code' => $code,
                'date_add' => datenow(),
                'date_update' => datenow(),
                'date_registered' => datenow()
            );
            if( $this->db->insert($this->tbl, $data) ) 
            {
                $passenger_id = $this->db->insert_id();
                // Insert record in Options table
                $options = array('contact_sync', 'default_payment_method');
                foreach( $options as $option )
                {
                    $data = array(
                        'passenger_id' => $passenger_id,
                        'option_key' => $option,
                        'option_value' => 0
                    );
                    $this->db->insert($this->tbl_option, $data);
                }
                return $code;    
            }
        }
        return 0;
    }

    public function saveToken($token, $platform, $user_id)
    {
        $result = $this->db->get_where($this->tbl_device_token, array('user_id' => $user_id, 'platform' => $platform));
        if( $result->num_rows() > 0 )
        {
            // UPDATE
            $data = array(
                'token' => $token,
                'date_update' => datenow()
            );
            $where = array(
                'user_id' => $user_id,
                'type' => 2,
                'platform' => $platform
            );
            if( $this->db->update($this->tbl_device_token, $data, $where) ) {
                return true;
            }
        }
        else
        {
            //INSERT
            $data = array(
                'user_id' => $user_id,
                'type' => 2,
                'token' => $token,
                'platform' => $platform,
                'date_add' => datenow(),
                'date_update' => datenow()
            );
            if( $this->db->insert($this->tbl_device_token, $data) ) {
                return true;
            }
            return true;
        }
        return false;
    }
    
    
    public function verifyCode($mobile, $code)
    {
        $result = $this->db->get_where($this->tbl, array('mobile' => $mobile, 'code' => $code, 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $row = $result->row_array();
            $data = array(
                'code' => NULL
            );
            if( $this->db->update($this->tbl, $data, array('id' => $row['id'])) ) 
            {
                // Add user as online
                $data = array(
                    'user_id' => $row['id'],
                    'type' => 2,
                    'datetime' => datenow()
                );
                $this->db->insert($this->tbl_online_user, $data);

                if( $row['email'] == NULL )
                {
                    $resp = array(
                        'is_registered' => false,
                        'passenger_id' => $row['id']
                    );
                }  
                else
                {
                    $resp = array(
                        'is_registered' => true,
                        'info' => $this->getCompleteInfo($row['id'])
                    );
                }
                return $resp;
            }
        }
        return false;
    }

    public function logout($passenger_id)
    {
        if( $this->db->delete($this->tbl_online_user, array('user_id' => $passenger_id, 'type' => 2 )) ) {
            return true;
        }
        return false;
    }
    
	public function getAllPassengers($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.firstname', $search);
			$this->db->or_like($this->tbl . '.lastname', $search);
			$this->db->or_like($this->tbl . '.email', $search);
			$this->db->or_like($this->tbl . '.mobile', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.firstname', $search);
			$this->db->or_like($this->tbl . '.lastname', $search);
			$this->db->or_like($this->tbl . '.email', $search);
			$this->db->or_like($this->tbl . '.mobile', $search);
		}
		
		$cols = array(
            0 => '',
            1 => 'photo',
			2 => 'firstname',
			3 => 'lastname',
			4 => 'email',
			5 => 'mobile',
            6 => 'is_active',
			7 => 'date_registered',
            8 => ''
		);
        
        //$this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.id', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
        
        $this->db->where($this->tbl . '.is_delete', 0);
		
		$this->db->select('*');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = $start;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('passenger/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Passenger?\');" href="'.base_url('passenger/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            if( $v['is_active'] )
            {
                $active = '<span class="label label-primary">Active</span>';
            }
            else
            {
                $active = '<span class="label label-danger">Inactive</span>';
            }
            
            $photo = '<img src="'.base_url(PASSENGER_PHOTO_PATH . $v['photo']).'" alt="" width="50" class="img-responsive">';
			
			$data[] = array($checkbox,
                            $photo,
							$v['firstname'],
							$v['lastname'],
							$v['email'],
							$v['mobile'],
							$active,
							easyDate($v['date_registered']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}
    
    public function getOptions($passenger_id) 
    {
        $this->db->select('option_key, option_value');
        $result = $this->db->get_where($this->tbl_option, array('passenger_id' => $passenger_id));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getCancellationReasons()
    {
        $result = $this->db->get_where($this->tbl_cancellation_reason, array('type' => 2));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getRatingMessages()
    {
        $result = $this->db->get_where($this->tbl_rating_message, array('type' => 2));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getRatings($passenger_id)
    {
        $rating = 0;
        $sql = "SELECT SUM(tbl_ride_rating.rating) AS `total_rating`, COUNT(tbl_ride_rating.id) AS `total_records` FROM `tbl_ride_rating` JOIN tbl_ride ON tbl_ride.id = tbl_ride_rating.ride_id WHERE tbl_ride_rating.type = 2 AND tbl_ride.passenger_id = " . $passenger_id;
        $result = $this->db->query($sql);
        if( $result->num_rows() > 0 ) {
            $res = $result->row_array();
            if( $res['total_records'] > 0 ) {
                $rating = $res['total_rating'] / $res['total_records'];
            }
        }
        return number_format($rating, 2);
    }
    
    public function getInfo($id) 
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        return $result->row_array();
    }
    
    public function getCompleteInfo($id)
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        $info = $result->row_array();

        $date_registered = new DateTime($info['date_registered']);
        $info['date_registered'] = $date_registered->format('d F Y');

        $info['photo'] = base_url(PASSENGER_PHOTO_PATH . $info['photo']);
        
        // Options
        $info['options'] = $this->getOptions($id);

        // Rating
        $info['rating'] = $this->getRatings($id);

        // Save Locations
        $info['saved_locations'] = $this->getSavedLocations($id);

        // Save Card
        $info['saved_cards'] = $this->getCards($id);
        
        return $info;
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('email' => $data['email']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }

    public function saveLocation($data)
    {
        $result = $this->db->get_where($this->tbl_passenger_saved_location, array('passenger_id' => $data['passenger_id'], 'icon' => $data['icon']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl_passenger_saved_location, $data) )
            {
                return $this->getSavedLocations($data['passenger_id']);
            }
        }
        else
        {
            $where = array(
                'passenger_id' => $data['passenger_id'],
                'icon' => $data['icon']
            );
            if( $this->db->update($this->tbl_passenger_saved_location, $data, $where) )
            {
                return $this->getSavedLocations($data['passenger_id']);
            }
        }
        return false;
    }

    public function saveCard($data)
    {
        if( $this->db->insert($this->tbl_passenger_card, $data) )
        {
            return $this->getCards($data['passenger_id']);
        }
        return false;
    }

    public function updateCard($data, $where)
    {
        if( $this->db->update($this->tbl_passenger_card, $data, $where) )
        {
            return $this->getCards($data['passenger_id']);
        }
        return false;
    }

    public function getCards($passenger_id)
    {
        $result = $this->db->get_where($this->tbl_passenger_card, array('passenger_id' => $passenger_id));
        if( $result->num_rows() > 0 )
        {
            $cards = array();
            foreach( $result->result_array() as $card)
            {
                $card['image'] = base_url('assets/uploads/cards/' . $card['card_type'] . '.png');
                $card['card_number'] = '*' . substr($card['card_number'], -4);
                $card['cvv'] = '***';
                $cards[] = $card;
            }
            return $cards;
        }
        return false;
    }

    public function getSavedLocations($passenger_id)
    {
        // Home Location
        $result = $this->db->get_where($this->tbl_passenger_saved_location, array('passenger_id' => $passenger_id, 'icon' => 'ios-home-outline'));
        $home = null;
        if( $result->num_rows() > 0 )
        {
            $home = $result->row_array();
        }
        // Work Location
        $result = $this->db->get_where($this->tbl_passenger_saved_location, array('passenger_id' => $passenger_id, 'icon' => 'ios-briefcase-outline'));
        $work = null;
        if( $result->num_rows() > 0 )
        {
            $work = $result->row_array();
        }
        $return = array();
        $return[] = $home;
        $return[] = $work;
        return $return;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    public function editOptions($data, $where) 
    {
        $options = $this->getOptions($where['passenger_id']);
        $i = 0;
        foreach( $options as $option )
        {
            if( array_key_exists($option['option_key'], $data) )
            {
                $dat = array(
                    'option_value' => $data[$option['option_key']]
                );
                $where = array(
                    'passenger_id' => $where['passenger_id'],
                    'option_key' => $option['option_key']
                );
                $this->db->update($this->tbl_option, $dat, $where);
            }
            $i++;
        }
        return $this->getOptions($where['passenger_id']);
    }
    
    private function generateCode()
    {
        $reset_code = '';
        for( $i = 1; $i <= 4; $i++ )
        {
            $reset_code .= mt_rand(0, 9);
        }
        return $reset_code;
    }
}

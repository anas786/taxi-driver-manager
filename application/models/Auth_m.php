<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_m extends CI_Model {
    
    private $tbl = 'user';
    
	public function login($email, $password)
	{
        $result = $this->db->get_where($this->tbl, array('email' => $email, 'password' => md5($password), 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            return $result->row_array();    
        }
        return false;
	}
}

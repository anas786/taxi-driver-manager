<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver_m extends CI_Model {
    
    private $tbl = 'driver';
    private $tbl_doc = 'driver_document';
    private $tbl_device_token = 'device_token';
    private $tbl_option = 'driver_option';
    private $tbl_online = 'online_driver';
    private $tbl_online_user = 'online_user';
    private $tbl_cancellation_reason = 'cancellation_reason';
    private $tbl_rating_message = 'rating_message';
    private $tbl_driver_vehicle = 'driver_vehicle';
    private $tbl_make_model = 'vehicle_make_model';
    private $tbl_color = 'color';
    private $tbl_ride = 'ride';
    private $tbl_ride_price = 'ride_price';
    private $tbl_ride_rating = 'ride_rating';
    private $tbl_passenger = 'passenger';

    public function login($email, $password)
	{
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->group_start();
        $this->db->where($this->tbl . '.email', $email);
        $this->db->or_where($this->tbl . '.mobile', $email);
        $this->db->group_end();
        $this->db->where($this->tbl . '.password', md5($password));
//        $this->db->where($this->tbl . '.is_active', 1);
        $this->db->where($this->tbl . '.is_delete', 0);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            $driver = $result->row_array();
            $driver['photo'] = base_url(DRIVER_PHOTO_PATH . $driver['photo']); 
            
            // Documents
            $driver['documents'] = $this->getDocuments($driver['id']);
            
            // Options
            $driver['options'] = $this->getOptions($driver['id']);

            // Ratings
            $driver['rating'] = $this->getRatings($driver['id']);

            // Vehicle
            $driver['vehicle'] = $this->getVehicle($driver['id']);
            
            return $driver;   
        }
        return false;
	}

	public function saveToken($token, $platform, $user_id)
    {
        $result = $this->db->get_where($this->tbl_device_token, array('user_id' => $user_id, 'platform' => $platform));
        if( $result->num_rows() > 0 )
        {
            // UPDATE
            $data = array(
                'token' => $token,
                'date_update' => datenow()
            );
            $where = array(
                'user_id' => $user_id,
                'type' => 1,
                'platform' => $platform
            );
            if( $this->db->update($this->tbl_device_token, $data, $where) ) {
                return true;
            }
        }
        else
        {
            //INSERT
            $data = array(
                'user_id' => $user_id,
                'type' => 1,
                'token' => $token,
                'platform' => $platform,
                'date_add' => datenow(),
                'date_update' => datenow()
            );
            if( $this->db->insert($this->tbl_device_token, $data) ) {
                return true;
            }
            return true;
        }
        return false;
    }

    public function verifyMobileAndGenerateCode($mobile)
    {
        $result = $this->db->get_where($this->tbl, array('mobile' => $mobile, 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $row = $result->row_array();

            // Check whether user is already logged in
            $result = $this->db->get_where($this->tbl_online_user, array('user_id' => $row['id'], 'type' => 1));
            if( $result->num_rows() == 0 )
            {
                $code = $this->generateResetCode();
                $data = array(
                    'reset_code' => $code
                );
                if( $this->db->update($this->tbl, $data, array('id' => $row['id'])) )
                {
                    return $code;
                }
            }
            else
            {
                return 1;
            }

        }
        return 0;
    }

    public function logout($driver_id)
    {
        if( $this->db->delete($this->tbl_online_user, array('user_id' => $driver_id, 'type' => 1)) ) {
            return true;
        }
        return false;
    }


    public function verifyCode($mobile, $code)
    {
        $result = $this->db->get_where($this->tbl, array('mobile' => $mobile, 'reset_code' => $code, 'is_active' => 1, 'is_delete' => 0));
        if( $result->num_rows() > 0 ) {
            $row = $result->row_array();
            $data = array(
                'reset_code' => NULL
            );
            if( $this->db->update($this->tbl, $data, array('id' => $row['id'])) )
            {
                // Add user as online
                $data = array(
                    'user_id' => $row['id'],
                    'type' => 1,
                    'datetime' => datenow()
                );
                $this->db->insert($this->tbl_online_user, $data);

                return $this->getCompleteInfo($row['id']);
            }
        }
        return false;
    }
    
    public function register($email, $firstname, $lastname, $mobile, $taxi_association, $country_id, $city_id, $invite_code)
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->group_start();
        $this->db->where($this->tbl . '.email', $email);
        $this->db->or_where($this->tbl . '.mobile', $mobile);
        $this->db->group_end();
        
        $this->db->where($this->tbl . '.is_delete', 0);
        $result = $this->db->get();
        if( $result->num_rows() == 0 )
        {
            $state = $this->db->get_where('city', array('id' => $city_id));
            $state = $state->row_array();
            $data = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email' => $email,
                'mobile' => $mobile,
                'country_id' => $country_id,
                'state_id' => $state['state_id'],
                'city_id' => $city_id,
                'photo' => 'default.png',
                'taxi_association_number' => $taxi_association,
                'invite_code' => $invite_code,
                'date_add' => datenow(),
                'date_update' => datenow(),
                'date_registered' => datenow(),
            );
            if( $this->db->insert($this->tbl, $data) )
            {
                $driver_id = $this->db->insert_id();
                // Insert record in Document table
                $data = array(
                    'driver_id' => $driver_id
                );
                $this->db->insert($this->tbl_doc, $data);
                
                // Insert record in Options table
                $options = array('booking_alert', 'pickup_alert', 'contact_sync');
                foreach( $options as $option )
                {
                    $data = array(
                        'driver_id' => $driver_id,
                        'option_key' => $option,
                        'option_value' => 0
                    );
                    $this->db->insert($this->tbl_option, $data);
                }
                
                return $this->getCompleteInfo($driver_id);
            }
        }
        return false;
    }
    
    public function forgotPassword($email)
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->group_start();
        $this->db->where($this->tbl . '.email', $email);
        $this->db->or_where($this->tbl . '.mobile', $email);
        $this->db->group_end();
        $this->db->where($this->tbl . '.is_active', 1);
        $this->db->where($this->tbl . '.is_delete', 0);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            $driver = $result->row_array();
            // Set reset code
            $reset_code = $this->generateResetCode();
            $data = array(
                'reset_code' => $reset_code
            );
            if( $this->db->update($this->tbl, $data, array('id' => $driver['id'])) ) {
                return $reset_code;
            } 
        }
        return false;
    }
    
    public function resetPassword($code, $password)
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->where($this->tbl . '.reset_code', $code);
        $this->db->where($this->tbl . '.is_delete', 0);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            // Set new password
            $data = array(
                'password' => md5($password),
                'reset_code' => NULL
            );
            if( $this->db->update($this->tbl, $data, array('reset_code' => $code)) ) {
                return true;
            } 
        }
        return false;
    }
    
    public function getOptions($driver_id) 
    {
        $this->db->select('option_key, option_value');
        $result = $this->db->get_where($this->tbl_option, array('driver_id' => $driver_id));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getReports($driver_id)
    {
        $today = $current_cycle = $previous_cycle = array(
            'date' => date('M d, Y'),
            'trips' => 0,
            'canceled_trips' => 0,
            'earnings' => 0.00,
            'tips' => 0.00,
            'guarantee' => 0.00,
            'cash_collected' => 0.00,
            'adjustments' => 0.00,
            'wallet' => 0.00,
            'rating' => 0,
            'acceptance' => 0,
            'availability' => 0.00
        );

        // Get Today's Report
        $today_date = date('Y-m-d');
        $this->db->select($this->tbl_ride . '.*, ' . $this->tbl_ride_price . '.actual_price, ' . $this->tbl_ride_rating . '.rating');
        $this->db->from($this->tbl_ride);
        $this->db->join($this->tbl_ride_price, $this->tbl_ride_price . '.ride_id=' . $this->tbl_ride . '.id');
        $this->db->join($this->tbl_ride_rating, $this->tbl_ride_rating . '.ride_id=' . $this->tbl_ride . '.id', 'right');
        $this->db->where($this->tbl_ride . '.driver_id', $driver_id);
        $this->db->where('DATE(tbl_ride.date_add)', $today_date);
        $this->db->where($this->tbl_ride . '.is_delete', 0);
        $this->db->where($this->tbl_ride_rating . '.type', 1);
        $result = $this->db->get();
        if( $result->num_rows() > 0 )
        {
            $today['trips'] = $result->num_rows();
            $cancelled  = 0;
            $tips = 0.00;
            $number_of_ratings = 0;
            $total_ratings = 0;
            $acceptance = 0;
            $earnings = 0.00;
            foreach( $result->result_array() as $trip )
            {
                if( $trip['is_cancelled'] == 1 ) {
                    $cancelled++;
                }
                if( $trip['rating'] != NULL ) {
                    $number_of_ratings++;
                    $total_ratings += $trip['rating'];
                }
                $earnings += $trip['actual_price'];
            }

            $today['canceled_trips'] = $cancelled;
            if( $number_of_ratings > 0 ) {
                $today['rating'] = $total_ratings / $number_of_ratings;
            }

            $today['rating'] = number_format($today['rating'], 2);
            $today['earnings'] = number_format($earnings, 2);
            $today['tips'] = number_format($tips, 2);
        }


        // Get Current Cycle's Report
        $end_date = new DateTime();
        $end_date->sub(new DateInterval('P1D'));
        $formatted_date = $end_date->format('M d, Y');
        $end_date = $end_date->format('Y-m-d');

        $start_date = new DateTime($end_date);
        $start_date->sub(new DateInterval('P6D'));
        $formatted_date = $start_date->format('M d, Y') . ' - ' . $formatted_date;
        $start_date = $start_date->format('Y-m-d');

        $current_cycle['date'] = $formatted_date;

        $this->db->select($this->tbl_ride . '.*, ' . $this->tbl_ride_price . '.actual_price, ' . $this->tbl_ride_rating . '.rating');
        $this->db->from($this->tbl_ride);
        $this->db->join($this->tbl_ride_price, $this->tbl_ride_price . '.ride_id=' . $this->tbl_ride . '.id');
        $this->db->join($this->tbl_ride_rating, $this->tbl_ride_rating . '.ride_id=' . $this->tbl_ride . '.id', 'right');
        $this->db->where($this->tbl_ride . '.driver_id', $driver_id);
        $this->db->where('DATE(tbl_ride.date_add)>=', $start_date);
        $this->db->where('DATE(tbl_ride.date_add)<=', $end_date);
        $this->db->where($this->tbl_ride . '.is_delete', 0);
        $this->db->where($this->tbl_ride_rating . '.type', 1);
        $result = $this->db->get();
        if( $result->num_rows() > 0 )
        {
            $current_cycle['trips'] = $result->num_rows();
            $cancelled  = 0;
            $tips = 0.00;
            $number_of_ratings = 0;
            $total_ratings = 0;
            $acceptance = 0;
            $earnings = 0.00;
            foreach( $result->result_array() as $trip )
            {
                if( $trip['is_cancelled'] == 1 ) {
                    $cancelled++;
                }
                if( $trip['rating'] != NULL ) {
                    $number_of_ratings++;
                    $total_ratings += $trip['rating'];
                }
                $earnings += $trip['actual_price'];
            }

            $current_cycle['canceled_trips'] = $cancelled;
            if( $number_of_ratings > 0 ) {
                $current_cycle['rating'] = $total_ratings / $number_of_ratings;
            }
            $current_cycle['rating'] = number_format($current_cycle['rating'], 2);
            $current_cycle['earnings'] = number_format($earnings, 2);
            $current_cycle['tips'] = number_format($tips, 2);
        }


        // Get Previous Cycle's Report
        $end_date = new DateTime($start_date);
        $end_date->sub(new DateInterval('P1D'));
        $formatted_date = $end_date->format('M d, Y');
        $end_date = $end_date->format('Y-m-d');

        $start_date = new DateTime($end_date);
        $start_date->sub(new DateInterval('P6D'));
        $formatted_date = $start_date->format('M d, Y') . ' - ' . $formatted_date;
        $start_date = $start_date->format('Y-m-d');

        $previous_cycle['date'] = $formatted_date;

        $this->db->select($this->tbl_ride . '.*, ' . $this->tbl_ride_price . '.actual_price, ' . $this->tbl_ride_rating . '.rating');
        $this->db->from($this->tbl_ride);
        $this->db->join($this->tbl_ride_price, $this->tbl_ride_price . '.ride_id=' . $this->tbl_ride . '.id');
        $this->db->join($this->tbl_ride_rating, $this->tbl_ride_rating . '.ride_id=' . $this->tbl_ride . '.id', 'right');
        $this->db->where($this->tbl_ride . '.driver_id', $driver_id);
        $this->db->where('DATE(tbl_ride.date_add)>=', $start_date);
        $this->db->where('DATE(tbl_ride.date_add)<=', $end_date);
        $this->db->where($this->tbl_ride . '.is_delete', 0);
        $this->db->where($this->tbl_ride_rating . '.type', 1);
        $result = $this->db->get();
        if( $result->num_rows() > 0 )
        {
            $previous_cycle['trips'] = $result->num_rows();
            $cancelled  = 0;
            $tips = 0.00;
            $number_of_ratings = 0;
            $total_ratings = 0;
            $acceptance = 0;
            $earnings = 0.00;
            foreach( $result->result_array() as $trip )
            {
                if( $trip['is_cancelled'] == 1 ) {
                    $cancelled++;
                }
                if( $trip['rating'] != NULL ) {
                    $number_of_ratings++;
                    $total_ratings += $trip['rating'];
                }
                $earnings += $trip['actual_price'];
            }

            $previous_cycle['canceled_trips'] = $cancelled;
            if( $number_of_ratings > 0 ) {
                $previous_cycle['rating'] = $total_ratings / $number_of_ratings;
            }
            $previous_cycle['rating'] = number_format($previous_cycle['rating'], 2);
            $previous_cycle['earnings'] = number_format($earnings, 2);
            $previous_cycle['tips'] = number_format($tips, 2);
        }

        $response = array(
            'today' => $today,
            'current_cycle' => $current_cycle,
            'previous_cycle' => $previous_cycle
        );
        return $response;
    }

    public function getRatings($driver_id)
    {
        $rating = 0;
        $sql = "SELECT SUM(tbl_ride_rating.rating) AS `total_rating`, COUNT(tbl_ride_rating.id) AS `total_records` FROM `tbl_ride_rating` JOIN tbl_ride ON tbl_ride.id = tbl_ride_rating.ride_id WHERE tbl_ride_rating.type = 1 AND tbl_ride.driver_id = " . $driver_id;
        $result = $this->db->query($sql);
        if( $result->num_rows() > 0 ) {
            $res = $result->row_array();
            if( $res['total_records'] > 0 ) {
                $rating = $res['total_rating'] / $res['total_records'];
            }
        }
        return number_format($rating, 2);
    }

    public function getCancellationReasons()
    {
        $result = $this->db->get_where($this->tbl_cancellation_reason, array('type' => 1));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getRatingMessages()
    {
        $result = $this->db->get_where($this->tbl_rating_message, array('type' => 1));
        if( $result->num_rows() > 0 ) {
            return $result->result_array();
        }
        return false;
    }

    public function getVehicle($driver_id)
    {
        $this->db->select($this->tbl_driver_vehicle . '.plate_number, ' . $this->tbl_make_model . '.make,' . $this->tbl_make_model . '.model,' . $this->tbl_make_model . '.year, ' . $this->tbl_color . '.name AS `color`');
        $this->db->join($this->tbl_make_model, $this->tbl_make_model. '.id=' . $this->tbl_driver_vehicle . '.vehicle_make_model_id');
        $this->db->join($this->tbl_color, $this->tbl_color. '.id=' . $this->tbl_driver_vehicle . '.color_id');
        $this->db->from($this->tbl_driver_vehicle);
        $this->db->where($this->tbl_driver_vehicle . '.driver_id', $driver_id);
        $this->db->where($this->tbl_driver_vehicle . '.is_delete', 0);
        $this->db->where($this->tbl_driver_vehicle . '.is_active', 1);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            return $result->row_array();
        }
        return false;
    }

    public function getDocuments($driver_id) 
    {
        $this->db->select('CONCAT("'.base_url(DRIVER_DOC_PATH).'", driving_license) AS `driving_license`, CONCAT("'.base_url(DRIVER_DOC_PATH).'", insurance_certificate) AS `insurance_certificate`, CONCAT("'.base_url(DRIVER_DOC_PATH).'", inspection_certificate) AS `inspection_certificate`, CONCAT("'.base_url(DRIVER_DOC_PATH).'", taxi_association_number) AS `taxi_association_number`, CONCAT("'.base_url(DRIVER_DOC_PATH).'", private_hire_vehicle_license) AS `private_hire_vehicle_license`');
        $result = $this->db->get_where($this->tbl_doc, array('driver_id' => $driver_id));
        if( $result->num_rows() > 0 ) {
            return $result->row_array();
        }
        return false;
    }
    
	public function getAllDrivers($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.firstname', $search);
			$this->db->or_like($this->tbl . '.lastname', $search);
			$this->db->or_like($this->tbl . '.email', $search);
			$this->db->or_like($this->tbl . '.mobile', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.firstname', $search);
			$this->db->or_like($this->tbl . '.lastname', $search);
			$this->db->or_like($this->tbl . '.email', $search);
			$this->db->or_like($this->tbl . '.mobile', $search);
		}
		
		$cols = array(
            0 => '',
            1 => 'photo',
			2 => 'fullname',
			3 => 'email',
			4 => 'mobile',
			5 => 'country',
            6 => 'is_active',
			7 => 'date_registered',
            8 => ''
		);
        
        //$this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.id', 'DESC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
        
        $this->db->where($this->tbl . '.is_delete', 0);
		
		$this->db->select('*');
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = $start;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('driver/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Driver?\');" href="'.base_url('driver/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
            
            if( $v['is_active'] )
            {
                $active = '<span class="label label-primary">Active</span>';
            }
            else
            {
                $active = '<span class="label label-danger">Inactive</span>';
            }
            
            $fullname = $v['lastname'] . ' ' . $v['firstname'];
            
            $photo = '<img src="'.base_url(DRIVER_PHOTO_PATH . $v['photo']).'" alt="" width="50" class="img-responsive">';
            
            $country = $this->db->get_where('country', array('id' => $v['country_id']));
            $country = $country->row_array();
			
			$data[] = array($checkbox,
                            $photo,
							$fullname,
							$v['email'],
							$v['mobile'],
							$country['name'],
							$active,
							easyDate($v['date_registered']),
							$buttons
						);
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}

    public function getAll()
    {
        $result = $this->db->get_where($this->tbl, array('is_delete' => 0, 'is_active' => 1));
        if( $result->num_rows() > 0 )
        {
            $drivers = array();
            foreach( $result->result_array() as $driver )
            {
                $driver['photo'] = base_url(DRIVER_PHOTO_PATH . $driver['photo']);
                $drivers[] = $driver;
            }
            return $drivers;
        }
        return false;
    }
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*, ' . $this->tbl_doc . '.driving_license, ' . $this->tbl_doc . '.insurance_certificate, ' . $this->tbl_doc . '.inspection_certificate, ' . $this->tbl_doc . '.taxi_association_number, ' . $this->tbl_doc . '.private_hire_vehicle_license');
        $this->db->from($this->tbl);
        $this->db->join($this->tbl_doc, $this->tbl . '.id=' . $this->tbl_doc . '.driver_id');
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function getHistory($driver_id, $date)
    {
        $history = array(
            'total_earning' => 0.00,
            'rides' => null,
            'events' => null
        );
        $result = $this->db->get_where($this->tbl_ride, array('driver_id' => $driver_id, 'DATE(date_add)' => $date, 'is_delete' => 0));
        if( $result->num_rows() > 0 )
        {
            $i = 0;
            foreach( $result->result_array() as $ride )
            {
                $history['rides'][$i] = $ride;

                // Rating
                $history['rides'][$i]['rating'] = null;
                $rating = $this->db->get_where($this->tbl_ride_rating, array('type' => 2, 'ride_id' => $ride['id']));
                if( $rating->num_rows() > 0 ) {
                    $r = $rating->row_array();
                    $history['rides'][$i]['rating'] = $r['rating'];
                }

                // Passenger
                $rating = $this->db->get_where($this->tbl_passenger, array('id' => $ride['passenger_id']));
                $rating = $rating->row_array();
                $history['rides'][$i]['passenger'] = $rating['firstname'] . ' ' . $rating['lastname'];

                // Ride Time
                $history['rides'][$i]['ride_time'] = easyTime($history['rides'][$i]['date_add'], 'h:i a');

                // Ride Price
                $history['rides'][$i]['price'] = null;
                $rating = $this->db->get_where($this->tbl_ride_price, array('ride_id' => $ride['id']));
                if( $rating->num_rows() > 0 ) {
                    $r = $rating->row_array();
                    $history['rides'][$i]['price'] = $r['actual_price'];
                }

                $history['total_earning'] += $history['rides'][$i]['price'];

                $i++;
            }
        }

        $dd = explode("-", $date);
        $this->db->group_by('DATE(date_add)');
        $result = $this->db->get_where($this->tbl_ride, array('driver_id' => $driver_id, 'YEAR(date_add)' => $dd[0], 'MONTH(date_add)' => $dd[1], 'is_delete' => 0));
        if( $result->num_rows() > 0 )
        {
            foreach( $result->result_array() as $event )
            {
                $history['events'][] = $event['date_add'];
            }
        }

        return $history;
    }

    public function getCompleteInfo($id)
    {
        $result = $this->db->get_where($this->tbl, array('id' => $id));
        if( $result->num_rows() > 0 ) {
            $driver = $result->row_array();
            $driver['photo'] = base_url(DRIVER_PHOTO_PATH . $driver['photo']);

            // Documents
            $driver['documents'] = $this->getDocuments($driver['id']);

            // Options
            $driver['options'] = $this->getOptions($driver['id']);

            // Rating
            $driver['rating'] = $this->getRatings($driver['id']);

            // Vehicle
            $driver['vehicle'] = $this->getVehicle($driver['id']);

            return $driver;
        }
        return false;
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('email' => $data['email']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function addDoc($data) 
    {
        if( $this->db->insert($this->tbl_doc, $data) )
        {
            return true;
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    public function editDoc($data, $where) 
    {
        if( $this->db->update($this->tbl_doc, $data, $where) )
        {
            return true;
        }
        return false;
    }
    
    
    public function editOptions($data, $where) 
    {
        $options = $this->getOptions($where['driver_id']);
        $i = 0;
        foreach( $options as $option )
        {
            if( array_key_exists($option['option_key'], $data) )
            {
                $dat = array(
                    'option_value' => $data[$option['option_key']]
                );
                $where = array(
                    'driver_id' => $where['driver_id'],
                    'option_key' => $option['option_key']
                );
                $this->db->update($this->tbl_option, $dat, $where);
            }
            $i++;
        }
        return $this->getOptions($where['driver_id']);
    }
    
    public function editStatus($status, $where) 
    {
        if( $status == 1 )
        {
            // First check if driver account is active/approved
            $driver = $this->getInfo($where['driver_id']);
            if( $driver['is_active'] == 1 ) {
                // Go Online
                $data = $where;
                if( $this->db->insert($this->tbl_online, $data) ) {
                    return true;
                }
            }
        }
        else
        {
            // Go Offline
            if( $this->db->delete($this->tbl_online, $where) ) {
                return true;
            }
        }
        return false;
    }
    
    
    private function generateResetCode()
    {
        $reset_code = '';
        for( $i = 1; $i <= 4; $i++ )
        {
            $reset_code .= mt_rand(0, 9);
        }
        return $reset_code;
    }
    
}

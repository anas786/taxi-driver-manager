<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localization_m extends CI_Model {
    
    private $tbl_country = 'country';
    private $tbl_state = 'state';
    private $tbl_city = 'city';
    
	public function getCountries()
	{
        $result = $this->db->get($this->tbl_country);
        if( $result->num_rows() > 0 ) {
            return $result->result_array(); 
        }
        return false;    
	}
    
    public function getStates($country_id)
	{
        $result = $this->db->get_where($this->tbl_state, array('country_id' => $country_id));
        if( $result->num_rows() > 0 ) {
            return $result->result_array(); 
        }
        return false;   
	}
    
    public function getCities($state_id)
	{
        $result = $this->db->get_where($this->tbl_city, array('state_id' => $state_id));
        if( $result->num_rows() > 0 ) {
            return $result->result_array(); 
        }
        return false;    
	}
    
    public function getCitiesByCountryId($country_id)
	{
        $this->db->select($this->tbl_city . '.*');
        $this->db->from($this->tbl_city);
        $this->db->join($this->tbl_state, $this->tbl_city.'.state_id=' . $this->tbl_state.'.id');
        $this->db->join($this->tbl_country, $this->tbl_state.'.country_id=' . $this->tbl_country.'.id');
        $this->db->where($this->tbl_country . '.id', $country_id);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            return $result->result_array(); 
        }
        return false;    
	}

    public function getCitiesByCountryCode($code)
    {
        $this->db->select($this->tbl_city . '.*, ' . $this->tbl_country . '.id AS `country_id`, ' . $this->tbl_country . '.name AS `country_name`, ' . $this->tbl_country . '.phonecode');
        $this->db->from($this->tbl_city);
        $this->db->join($this->tbl_state, $this->tbl_city.'.state_id=' . $this->tbl_state.'.id');
        $this->db->join($this->tbl_country, $this->tbl_state.'.country_id=' . $this->tbl_country.'.id');
        $this->db->where($this->tbl_country . '.sortname', $code);
        $result = $this->db->get();
        if( $result->num_rows() > 0 ) {
            $cities = array();
            foreach( $result->result_array() as $city )
            {
                $cities[] = array(
                    'id' => $city['id'],
                    'name' => $city['name'],
                    'state_id' => $city['state_id']
                );
                $resp = array(
                    'country_id' => $city['country_id'],
                    'country_name' => $city['country_name'],
                    'phonecode' => '+' . $city['phonecode'],
                    'flag' => base_url('assets/img/flags/+' . $city['phonecode'] . '.png')
                );
            }
            $resp['cities'] = $cities;
            return $resp;
        }
        return false;
    }
}

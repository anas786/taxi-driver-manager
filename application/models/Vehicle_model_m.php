<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model_m extends CI_Model {
    
    private $tbl = 'vehicle_make_model';

    public function getModels()
    {
        $this->db->select('model');
        $this->db->from($this->tbl);
        $this->db->group_by('model');
        $this->db->order_by('model');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getMakes()
    {
        $this->db->select('make');
        $this->db->from($this->tbl);
        $this->db->group_by('make');
        $this->db->order_by('make');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function getYears()
    {
        $this->db->select('year');
        $this->db->from($this->tbl);
        $this->db->group_by('year');
        $this->db->order_by('year');
        $result = $this->db->get();
        return $result->result_array();
    }
    
	public function getAllVehicleModels($draw, $start, $length, $search, $order)
	{
        if(strlen($search))
        {
			$this->db->like($this->tbl . '.id', $search);
			$this->db->or_like($this->tbl . '.year', $search);
			$this->db->or_like($this->tbl . '.make', $search);
			$this->db->or_like($this->tbl . '.model', $search);
		}
		
		$recordsTotal = $this->db->count_all_results($this->tbl);
		
		if(strlen($search))
        {
            $this->db->like($this->tbl . '.id', $search);
            $this->db->or_like($this->tbl . '.year', $search);
            $this->db->or_like($this->tbl . '.make', $search);
            $this->db->or_like($this->tbl . '.model', $search);
		}
		
		$cols = array(
            0 => '',
            1 => '',
			2 => 'make',
			3 => 'model',
			4 => 'year',
            5 => ''
		);
        
        //$this->db->join('role', 'role.id=' . $this->tbl . '.role_id');
		
		if(!empty($order))
        {
			foreach($order as $row)
            {
				$this->db->order_by($cols[$row['column']], $row['dir']);
			}
		}
        else
        {
			$this->db->order_by($this->tbl . '.make', 'ASC');
			$this->db->order_by($this->tbl . '.model', 'ASC');
			$this->db->order_by($this->tbl . '.year', 'ASC');
		}
		
        if( $length > 0 ) {
            $this->db->limit($length, $start);
        }
		
		$this->db->select('*');

		$this->db->where($this->tbl . '.is_delete', 0);
		
		$result = $this->db->get($this->tbl);
		$result = $result->result_array();
		
		$data = array();
		$i = 1;
		foreach ($result as $key => $v) 
        {
            $checkbox = '<div class="checkbox check-default"><input type="checkbox" value="'.$v['id'].'" id="checkbox'.$v['id'].'" name="ids[]" class="bulk-checkbox"><label for="checkbox'.$v['id'].'"></label></div>';
			$buttons = '<a href="'.base_url('vehicle_model/edit/' . $v['id']).'" class="btn btn-success"><i class="fa fa-pencil"></i></a>&nbsp;';
            $buttons .= '<a onclick="return confirm(\'Are you sure that you want to remove this Vehicle Model?\');" href="'.base_url('vehicle_model/delete/' . $v['id']).'" class="btn btn-danger"><i class="fa fa-remove"></i></a>';
			
			$data[] = array($checkbox,
							$i,
							$v['make'],
							$v['model'],
                            $v['year'],
							$buttons
						);
			$i++;
		}
		
		$data = array(
			'draw'	=>	$draw,
			'recordsFiltered'	=>	$recordsTotal,
			'recordsTotal'		=>	$recordsTotal,
			'data'				=>	$data
		);
		
		return $data;
	}

    public function getAll()
    {
        $this->db->order_by('make');
        $this->db->order_by('model');
        $this->db->order_by('year');
        $result = $this->db->get_where($this->tbl, array('is_delete' => 0));
        if( $result->num_rows() > 0 )
        {
            return $result->result_array();
        }
        return false;
    }
    
    public function getInfo($id) 
    {
        $this->db->select($this->tbl . '.*');
        $this->db->from($this->tbl);
        $this->db->where($this->tbl . '.id', $id);
        $result = $this->db->get();
        return $result->row_array();
    }
    
    public function add($data) 
    {
        $result = $this->db->get_where($this->tbl, array('make' => $data['make'], 'model' => $data['model'], 'year' => $data['year']));
        if( $result->num_rows() == 0 )
        {
            if( $this->db->insert($this->tbl, $data) )
            {
                return $this->db->insert_id();
            }
        }
        return false;
    }
    
    public function edit($data, $where) 
    {
        if( $this->db->update($this->tbl, $data, $where) )
        {
            return true;
        }
        return false;
    }

}

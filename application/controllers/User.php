<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('User_m');
    }
    
	public function index()
	{
        if( hasAccess($this->session->user_role, 'USER_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'User List'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->User_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/user/list', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllUsers()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->User_m->getAllUsers($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'USER_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New User'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('name', 'Full Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'trim'));
                $this->form_validation->set_rules('password', 'Password', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $dat = array(
                        'name' => $this->input->post('name'),
                        'email' => $this->input->post('email'),
                        'password' => md5($this->input->post('password')),
                        'role_id' => $this->input->post('role_id'),
                        'is_active' => isset($_POST['is_active']) ? 1 : 0,
                        'date_add' => datenow(),
                        'date_update' => datenow()
                    );
                    $id = $this->User_m->add($dat);
                    if( !$id )
                    {
                        $this->session->set_flashdata('msg_error', 'User with this email address already exists.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_success', 'User has been added successfully.');
                        redirect( 'user/edit/' . $id );
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Role_m');
            $data['roles'] = $this->Role_m->getAll();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/user/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'USER_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit User'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('name', 'Full Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    if( $_POST['password'] != '' )
                    {
                        $dat = array(
                            'name' => $this->input->post('name'),
                            'email' => $this->input->post('email'),
                            'password' => md5($this->input->post('password')),
                            'role_id' => $this->input->post('role_id'),
                            'is_active' => isset($_POST['is_active']) ? 1 : 0,
                            'date_update' => datenow()
                        );
                    }
                    else
                    {
                        $dat = array(
                            'name' => $this->input->post('name'),
                            'email' => $this->input->post('email'),
                            'role_id' => $this->input->post('role_id'),
                            'is_active' => isset($_POST['is_active']) ? 1 : 0,
                            'date_update' => datenow()
                        );
                    }
                    $where = array(
                        'id' => $this->input->post('id')
                    );
                    if( $this->User_m->edit($dat, $where) )
                    {
                        $this->session->set_flashdata('msg_success', 'User has been updated successfully.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_error', 'User with this email address already exists.');
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Role_m');
            $data['roles'] = $this->Role_m->getAll();
            
            $data['user'] = $this->User_m->getInfo($id);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/user/edit', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'USER_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1,
                    'date_update' => datenow()
                );
                $where = array(
                    'id' => $id
                );
                if( $this->User_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'User has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'User cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid User ID.');
            }
            redirect( 'user' );
        }
        else
        {
            notallowed();
        }
    }
    
    
}

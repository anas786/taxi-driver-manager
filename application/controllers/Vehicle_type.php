<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_type extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('Vehicle_type_m');
    }
    
	public function index()
	{
        if( hasAccess($this->session->user_role, 'VEHICLE_TYPES_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Vehicle Type List'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Vehicle_type_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_type/list', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllVehicleTypes()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->Vehicle_type_m->getAllVehicleTypes($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_TYPES_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New Vehicle Type'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('name', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('tagline', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('country_id', 'Country', array('required', 'trim'));
                $this->form_validation->set_rules('max_capacity', 'Max Capacity', array('required', 'trim'));
                $this->form_validation->set_rules('base_fare', 'Base Fare', array('required', 'trim'));
                $this->form_validation->set_rules('minimum_fare', 'Minimum Fare', array('required', 'trim'));
                $this->form_validation->set_rules('per_mile_fare', 'Per Mile Fare', array('required', 'trim'));
                $this->form_validation->set_rules('per_minute_fare', 'Per Minute Fare', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $image = 'default.png';
                    $error = false;
                    if( $_FILES['image']['name'] != '' )
                    {
                        $config['upload_path']          = './' . VEHICLE_TYPE_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['image']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('image'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $image = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        $dat = array(
                            'name' => $this->input->post('name'),
                            'tagline' => $this->input->post('tagline'),
                            'description' => $this->input->post('description'),
                            'image' => $image,
                            'country_id' => $this->input->post('country_id'),
                            'max_capacity' => $this->input->post('max_capacity'),
                            'base_fare' => $this->input->post('base_fare'),
                            'minimum_fare' => $this->input->post('minimum_fare'),
                            'per_mile_fare' => $this->input->post('per_mile_fare'),
                            'per_minute_fare' => $this->input->post('per_minute_fare'),
                            'is_active' => isset($_POST['is_active']) ? 1 : 0,
                            'date_add' => datenow(),
                            'date_update' => datenow()
                        );
                        $id = $this->Vehicle_type_m->add($dat);
                        if( !$id )
                        {
                            $this->session->set_flashdata('msg_error', 'Vehicle Type with this name already exists.');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg_success', 'Vehicle Type has been added successfully.');
                            redirect( 'vehicle_type/edit/' . $id );
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_type/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_TYPES_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit Vehicle Type'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('name', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('tagline', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('country_id', 'Country', array('required', 'trim'));
                $this->form_validation->set_rules('max_capacity', 'Max Capacity', array('required', 'trim'));
                $this->form_validation->set_rules('base_fare', 'Base Fare', array('required', 'trim'));
                $this->form_validation->set_rules('minimum_fare', 'Minimum Fare', array('required', 'trim'));
                $this->form_validation->set_rules('per_mile_fare', 'Per Mile Fare', array('required', 'trim'));
                $this->form_validation->set_rules('per_minute_fare', 'Per Minute Fare', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $error = false;
                    $image = '';
                    if( $_FILES['image']['name'] != '' )
                    {
                        $config['upload_path']          = './' . VEHICLE_TYPE_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['image']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('image'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $image = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        if( $image != '' )
                        {
                            $dat = array(
                                'name' => $this->input->post('name'),
                                'tagline' => $this->input->post('tagline'),
                                'description' => $this->input->post('description'),
                                'image' => $image,
                                'country_id' => $this->input->post('country_id'),
                                'max_capacity' => $this->input->post('max_capacity'),
                                'base_fare' => $this->input->post('base_fare'),
                                'minimum_fare' => $this->input->post('minimum_fare'),
                                'per_mile_fare' => $this->input->post('per_mile_fare'),
                                'per_minute_fare' => $this->input->post('per_minute_fare'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow()
                            );
                        }
                        else
                        {
                            $dat = array(
                                'name' => $this->input->post('name'),
                                'tagline' => $this->input->post('tagline'),
                                'description' => $this->input->post('description'),
                                'country_id' => $this->input->post('country_id'),
                                'max_capacity' => $this->input->post('max_capacity'),
                                'base_fare' => $this->input->post('base_fare'),
                                'minimum_fare' => $this->input->post('minimum_fare'),
                                'per_mile_fare' => $this->input->post('per_mile_fare'),
                                'per_minute_fare' => $this->input->post('per_minute_fare'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow()
                            );
                        }
                        $where = array(
                            'id' => $this->input->post('id')
                        );
                        if( $this->Vehicle_type_m->edit($dat, $where) )
                        {
                            $id = $this->input->post('id');
                            $this->session->set_flashdata('msg_success', 'Vehicle Type has been updated successfully.');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg_error', 'Vehicle Type with this name already exists.');
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['vehicle_type'] = $this->Vehicle_type_m->getInfo($id);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_type/edit', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_TYPES_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1,
                    'date_update' => datenow()
                );
                $where = array(
                    'id' => $id
                );
                if( $this->Vehicle_type_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'Vehicle Type has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Vehicle Type cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid Vehicle Type ID.');
            }
            redirect( 'vehicle_type' );
        }
        else
        {
            notallowed();
        }
    }
    
    
    private function generateInviteCode()
    {
        $random = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $invite_code = '';
        $length = 6;
        for( $i = 1; $i <= $length; $i++ )
        {
            $index = mt_rand(0, count($random)-1);
            $invite_code .= $random[$index];
        }
        return strtoupper($invite_code);
    }
    
}

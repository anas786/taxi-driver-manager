<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('Vehicle_m');
    }
    
	public function index()
	{
        if( hasAccess($this->session->user_role, 'VEHICLE_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Driver Vehicle List'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Vehicle_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle/list', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllVehicles()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->Vehicle_m->getAllVehicles($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New Vehicle'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('driver_id', 'Driver', array('required', 'trim'));
                $this->form_validation->set_rules('vehicle_make_model_id', 'Vehicle', array('required', 'trim'));
                $this->form_validation->set_rules('color_id', 'Color', array('required', 'trim'));
                $this->form_validation->set_rules('vehicle_type_id', 'Vehicle Type', array('required', 'trim'));
                $this->form_validation->set_rules('plate_number', 'Plate Number', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $dat = array(
                        'driver_id' => $this->input->post('driver_id'),
                        'vehicle_make_model_id' => $this->input->post('vehicle_make_model_id'),
                        'color_id' => $this->input->post('color_id'),
                        'plate_number' => $this->input->post('plate_number'),
                        'vehicle_type_id' => $this->input->post('vehicle_type_id'),
                        'is_active' => isset($_POST['is_active']) ? 1 : 0,
                        'date_add' => datenow(),
                        'date_update' => datenow()
                    );
                    $id = $this->Vehicle_m->add($dat);
                    if( !$id )
                    {
                        $this->session->set_flashdata('msg_error', 'This driver is already using a vehicle.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_success', 'Vehicle has been added successfully.');
                        redirect( 'vehicle/edit/' . $id );
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Color_m');
            $data['colors'] = $this->Color_m->getColors();

            $this->load->model('Vehicle_type_m');
            $data['vehicle_types'] = $this->Vehicle_type_m->getAll();

            $this->load->model('Vehicle_model_m');
            $data['vehicle_make_models'] = $this->Vehicle_model_m->getAll();

            $this->load->model('Driver_m');
            $data['drivers'] = $this->Driver_m->getAll();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit Vehicle'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('driver_id', 'Driver', array('required', 'trim'));
                $this->form_validation->set_rules('vehicle_make_model_id', 'Vehicle', array('required', 'trim'));
                $this->form_validation->set_rules('color_id', 'Color', array('required', 'trim'));
                $this->form_validation->set_rules('vehicle_type_id', 'Vehicle Type', array('required', 'trim'));
                $this->form_validation->set_rules('plate_number', 'Plate Number', array('required', 'trim'));

                if( $this->form_validation->run() == TRUE )
                {
                    $dat = array(
                        'driver_id' => $this->input->post('driver_id'),
                        'vehicle_make_model_id' => $this->input->post('vehicle_make_model_id'),
                        'color_id' => $this->input->post('color_id'),
                        'plate_number' => $this->input->post('plate_number'),
                        'vehicle_type_id' => $this->input->post('vehicle_type_id'),
                        'is_active' => isset($_POST['is_active']) ? 1 : 0,
                        'date_update' => datenow()
                    );
                    $where = array(
                        'id' => $this->input->post('id')
                    );
                    if( !$this->Vehicle_m->edit($dat, $where) )
                    {
                        $this->session->set_flashdata('msg_error', 'This driver is already using a vehicle.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_success', 'Vehicle has been updated successfully.');
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }

            $this->load->model('Color_m');
            $data['colors'] = $this->Color_m->getColors();

            $this->load->model('Vehicle_type_m');
            $data['vehicle_types'] = $this->Vehicle_type_m->getAll();

            $this->load->model('Vehicle_model_m');
            $data['vehicle_make_models'] = $this->Vehicle_model_m->getAll();

            $this->load->model('Driver_m');
            $data['drivers'] = $this->Driver_m->getAll();
            
            $data['vehicle'] = $this->Vehicle_m->getInfo($id);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle/edit', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1,
                    'date_update' => datenow()
                );
                $where = array(
                    'id' => $id
                );
                if( $this->Vehicle_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'Vehicle has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Vehicle cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid Vehicle ID.');
            }
            redirect( 'vehicle' );
        }
        else
        {
            notallowed();
        }
    }

    
}

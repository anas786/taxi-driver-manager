<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_m');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        if( isLoggedIn() ) {
            redirect( 'dashboard' );
        }
        $data['pageMeta'] = array(
            'title' => 'Sign in'
        );
		$this->load->view(THEME . '/login', $data);
	}
    
    public function login()
	{
        $data['pageMeta'] = array(
            'title' => 'Sign in'
        );
        
        if( isset($_POST['txtusername']) ) 
        {
            $user = $this->Auth_m->login($this->input->post('txtusername'), $this->input->post('txtpassword'));
            if( $user )
            {
                $sess_data = array(
                    'user_id' => $user['id'],
                    'user_role' => $user['role_id'],
                    'user_name' => $user['name'],
                    'user_email' => $user['email']
                );
                $this->session->set_userdata($sess_data);
                redirect( 'dashboard' );
            }
            else
            {
                $this->session->set_flashdata('msg_error', 'Invalid Login Details!');
            }
        }
        
		$this->load->view(THEME . '/login', $data);
	}
    
    public function logout() 
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('msg_success', 'You\'ve been logged out successfully.');
        redirect( 'auth' );
    }
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ride extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('Ride_m');
    }

    public function index()
    {
        redirect( 'ride/completed' );
    }

    public function completed()
	{
        if( hasAccess($this->session->user_role, 'RIDE_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Completed Rides'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Ride_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/ride/list_completed', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllCompletedRides()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->Ride_m->getAllCompletedRides($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }

    public function ongoing()
    {
        if( hasAccess($this->session->user_role, 'RIDE_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Ongoing Rides'
            );

            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Ride_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }

            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
            $this->load->view(THEME . '/ride/list_ongoing', $data);
        }
        else
        {
            notallowed();
        }

    }

    public function getAllOngoingRides()
    {
        if($this->input->is_ajax_request())
        {
            $get = $this->input->get();

            $draw = 1;
            $start = 0;
            $length = 10;
            $search = '';
            $order = array();

            if(is_array($get)){
                $draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
                $start	= array_key_exists('start', $get)		? $get['start']		: 0;
                $length = array_key_exists('length', $get)		? $get['length']	: 10;
                $search = array_key_exists('search', $get)		? $get['search']	: '';
                $search = array_key_exists('value', $search)	? $search['value']	: '';
                $order	= array_key_exists('order', $get)		? $get['order']	: '';
                $order	= !is_array($order) ? array() : $order;
            }

            $offset = $start * $length;
            $data = $this->Ride_m->getAllOngoingRides($draw, $start, $length, $search, $order);


            echo json_encode($data);
            die;
        }
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'DRIVER_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New Driver'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('firstname', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('lastname', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'valid_email', 'trim'));
                $this->form_validation->set_rules('mobile', 'Mobile Number', array('required', 'trim'));
                $this->form_validation->set_rules('password', 'Password', array('required', 'trim'));
                $this->form_validation->set_rules('country_id', 'Country', array('required', 'trim'));
                $this->form_validation->set_rules('state_id', 'State', array('required', 'trim'));
                $this->form_validation->set_rules('city_id', 'City', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $photo = 'default.png';
                    $error = false;
                    if( $_FILES['photo']['name'] != '' ) 
                    {
                        $config['upload_path']          = './' . DRIVER_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('photo'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $photo = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        $dat = array(
                            'firstname' => $this->input->post('firstname'),
                            'lastname' => $this->input->post('lastname'),
                            'email' => $this->input->post('email'),
                            'mobile' => $this->input->post('mobile'),
                            'password' => md5($this->input->post('password')),
                            'photo' => $photo,
                            'country_id' => $this->input->post('country_id'),
                            'state_id' => $this->input->post('state_id'),
                            'city_id' => $this->input->post('city_id'),
                            'invite_code' => $this->generateInviteCode(),
                            'is_active' => isset($_POST['is_active']) ? 1 : 0,
                            'date_add' => datenow(),
                            'date_update' => datenow(),
                            'date_registered' => datenow()
                        );
                        $id = $this->Driver_m->add($dat);
                        if( !$id )
                        {
                            $this->session->set_flashdata('msg_error', 'Driver with this email address already exists.');
                        }
                        else
                        {
                            $dat = array(
                                'driver_id' => $id,
                                'driving_license' => '',
                                'insurance_certificate' => '',
                                'inspection_certificate' => '',
                                'taxi_association_number' => '',
                                'private_hire_vehicle_license' => '' 
                            );
                            $this->Driver_m->addDoc($dat);
                            // Upload Documents
                            // Driving License
                            if( $_FILES['driving_license']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['driving_license']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('driving_license'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Driver License Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'driving_license' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Insurance Certificate
                            if( $_FILES['insurance_certificate']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['insurance_certificate']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('insurance_certificate'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Insurance Certificate Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'insurance_certificate' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Inspection Certificate
                            if( $_FILES['inspection_certificate']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['inspection_certificate']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('inspection_certificate'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Inspection Certificate Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'inspection_certificate' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Taxi Association Number
                            if( $_FILES['taxi_association_number']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['taxi_association_number']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('taxi_association_number'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Taxi Association Number Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'taxi_association_number' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Private Hire Vehicle License
                            if( $_FILES['private_hire_vehicle_license']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['private_hire_vehicle_license']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('private_hire_vehicle_license'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Private Hire Vehicle License Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'private_hire_vehicle_license' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            
                            $this->session->set_flashdata('msg_success', 'Driver has been added successfully.');
                            redirect( 'driver/edit/' . $id );
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/driver/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'DRIVER_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit User'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('firstname', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('lastname', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'valid_email', 'trim'));
                $this->form_validation->set_rules('mobile', 'Mobile Number', array('required', 'trim'));
                $this->form_validation->set_rules('country_id', 'Country', array('required', 'trim'));
                $this->form_validation->set_rules('state_id', 'State', array('required', 'trim'));
                $this->form_validation->set_rules('city_id', 'City', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $error = false;
                    $photo = '';
                    if( $_FILES['photo']['name'] != '' ) 
                    {
                        $config['upload_path']          = './' . DRIVER_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('photo'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $photo = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        if( $_POST['password'] != '' && $photo != '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'password' => md5($this->input->post('password')),
                                'photo' => $photo,
                                'country_id' => $this->input->post('country_id'),
                                'state_id' => $this->input->post('state_id'),
                                'city_id' => $this->input->post('city_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        else if( $_POST['password'] != '' && $photo == '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'password' => md5($this->input->post('password')),
                                'country_id' => $this->input->post('country_id'),
                                'state_id' => $this->input->post('state_id'),
                                'city_id' => $this->input->post('city_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        else if( $_POST['password'] == '' && $photo != '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'country_id' => $this->input->post('country_id'),
                                'state_id' => $this->input->post('state_id'),
                                'city_id' => $this->input->post('city_id'),
                                'photo' => $photo,
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        else
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'country_id' => $this->input->post('country_id'),
                                'state_id' => $this->input->post('state_id'),
                                'city_id' => $this->input->post('city_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        $where = array(
                            'id' => $this->input->post('id')
                        );
                        if( $this->Driver_m->edit($dat, $where) )
                        {
                            $id = $this->input->post('id');
                            // Upload Documents
                            // Driving License
                            if( $_FILES['driving_license']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['driving_license']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('driving_license'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Driver License Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'driving_license' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Insurance Certificate
                            if( $_FILES['insurance_certificate']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['insurance_certificate']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('insurance_certificate'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Insurance Certificate Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'insurance_certificate' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Inspection Certificate
                            if( $_FILES['inspection_certificate']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['inspection_certificate']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('inspection_certificate'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Inspection Certificate Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'inspection_certificate' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Taxi Association Number
                            if( $_FILES['taxi_association_number']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['taxi_association_number']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('taxi_association_number'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Taxi Association Number Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'taxi_association_number' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            // Private Hire Vehicle License
                            if( $_FILES['private_hire_vehicle_license']['name'] != '' )
                            {
                                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                                $config['file_name']            = $id . '_' . md5($_FILES['private_hire_vehicle_license']['name']) . '_' . time();
                
                                $this->load->library('upload', $config);
                                
                                if ( !$this->upload->do_upload('private_hire_vehicle_license'))
                                {
                                    $this->session->set_flashdata('msg_error', 'Private Hire Vehicle License Could not be uploaded: ' . $this->upload->display_errors());
                                }
                                else
                                {
                                    $p_data = $this->upload->data();
                                    $doc = $p_data['file_name'];
                                    $dat = array(
                                        'private_hire_vehicle_license' => $doc
                                    );
                                    $where = array(
                                        'driver_id' => $id
                                    );
                                    $this->Driver_m->editDoc($dat, $where);
                                }
                            }
                            $this->session->set_flashdata('msg_success', 'Driver has been updated successfully.');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg_error', 'Driver with this email address already exists.');
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['driver'] = $this->Driver_m->getInfo($id);
            
            $data['states'] = $this->Localization_m->getStates($data['driver']['country_id']);
            $data['cities'] = $this->Localization_m->getCities($data['driver']['state_id']);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/driver/edit', $data);
        }
        else
        {
            notallowed();
        }
    }

    public function detail($id=0)
    {
        if( hasAccess($this->session->user_role, 'RIDE_DETAIL') )
        {
            $data['pageMeta'] = array(
                'title' => 'Ride Detail'
            );

            $data['ride'] = $this->Ride_m->getCompleteInfo($id);

            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );
            $data['ride_routes'] = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/directions/json?origin='.$data['ride']['pickup_coordinates'].'&destination='.$data['ride']['dropoff_coordinates'].'&mode=driving&key=' . GOOGLE_MAP_API_KEY, false, stream_context_create($arrContextOptions)));

            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
            $this->load->view(THEME . '/ride/detail', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'RIDE_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1
                );
                $where = array(
                    'id' => $id
                );
                if( $this->Driver_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'Driver has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Driver cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid Driver ID.');
            }
            redirect( 'driver' );
        }
        else
        {
            notallowed();
        }
    }
    
    
    private function generateInviteCode()
    {
        $random = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $invite_code = '';
        $length = 6;
        for( $i = 1; $i <= $length; $i++ )
        {
            $index = mt_rand(0, count($random)-1);
            $invite_code .= $random[$index];
        }
        return strtoupper($invite_code);
    }
    
}

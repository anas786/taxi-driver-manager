<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-API-KEY, Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once APPPATH . 'libraries/REST_Controller.php';

class Localization extends REST_Controller {
    
    private $response_code = 200;
    
    public function __construct() 
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();  
        $this->load->model('Localization_m');
	}
    
    public function states_get()
	{
        if( $this->get('country_id') )
        {
            $story = $this->Localization_m->getStates($this->get('country_id'));
            if( !$story )
            {
                $output = array('status' => 0, 'data' => null, 'msg' => 'No states found');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => 1, 'data' => $story, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => 0, 'data' => null, 'msg' => 'Country ID is required');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    
    public function cities_get()
	{
        if( $this->get('state_id') )
        {
            $story = $this->Localization_m->getCities($this->get('state_id'));
            if( !$story )
            {
                $output = array('status' => 0, 'data' => null, 'msg' => 'No cities found');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => 1, 'data' => $story, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => 0, 'data' => null, 'msg' => 'State ID is required');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    public function cities_by_country_get()
	{
        $ip = $this->input->ip_address();

        $get_location = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$ip));

        $cities = $this->Localization_m->getCitiesByCountryCode($get_location['geoplugin_countryCode']);
        if( !$cities )
        {
            $output = array('status' => 0, 'data' => null, 'msg' => 'Sorry! We are not providing services in your area.');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => 1, 'data' => $cities, 'msg' => '');
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    
}

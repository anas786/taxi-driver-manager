<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-API-KEY, Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller {
    
    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();  
        $this->load->model('Passenger_m');
    }
    
    public function login_post()
	{
        if( $this->post('email') && $this->post('password') )
        {
            $passenger = $this->Passenger_m->login($this->post('email'), $this->post('password'));
            if( !$passenger )
            {
                $output = array('status' => 0, 'data' => null, 'msg' => 'Invalid login details');
            }
            else 
            {
                $output = array('status' => 1, 'data' => $passenger, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => 0, 'data' => null, 'msg' => 'Missing required parameters');
        }
	   
        $this->response( $output );
	}
    
}

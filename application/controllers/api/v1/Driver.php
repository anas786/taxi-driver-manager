<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-API-KEY, Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once APPPATH . 'libraries/REST_Controller.php';

class Driver extends REST_Controller {
    
    private $response_code = 200;
    
    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();  
        $this->load->model('Driver_m');
    }

    public function send_code_post()
    {
        if( $this->post('mobile') )
        {
            $mobile =  '+' . preg_replace( '/[^0-9]/', '', $this->post('mobile') );
            $code = $this->Driver_m->verifyMobileAndGenerateCode($mobile);
            if( $code == 0 )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No account is associated with this mobile number.');
                $this->response_code = 404;
            }
            else if( $code == 1 )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'This account is already in use in other device.');
                $this->response_code = 403;
            }
            else
            {
                $this->load->library('Sms');
                $message = 'Your NVOII code is: ' . $code;
                $this->sms->send_message($mobile, $message);
                $output = array('status' => true, 'data' => null, 'msg' => 'Please enter the code received via SMS.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }


    public function verify_code_post()
    {
        if( $this->post('mobile') && $this->post('code') )
        {
            $mobile =  '+' . preg_replace( '/[^0-9]/', '', $this->post('mobile') );
            $data = $this->Driver_m->verifyCode($mobile, $this->post('code'));
            if( $data )
            {
                $output = array('status' => true, 'data' => $data, 'msg' => 'Code verified.');
            }
            else
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid code.');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function login_post()
	{
        if( $this->post('email') && $this->post('password') )
        {
            $driver = $this->Driver_m->login($this->post('email'), $this->post('password'));
            if( !$driver )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid login details');
                $this->response_code = 404;
            }
            else
            {
                //$this->load->library('Sms');
                //$this->sms->send_message('+923243367335', 'Hello ' . $driver['firstname']);
                $output = array('status' => true, 'data' => $driver, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
	}


    public function device_token_post()
    {
        if( $this->post('token') && $this->post('platform') && $this->post('user_id') )
        {
            $status = $this->Driver_m->saveToken($this->post('token'), $this->post('platform'), $this->post('user_id'));
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not save token');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $status, 'msg' => 'Token saved');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function register_post()
	{
        $posty = json_decode(file_get_contents('php://input'), true);
        if( $this->post('email') && $this->post('firstname') && $this->post('lastname') && $this->post('mobile') && $this->post('taxi_association') && $this->post('city_id') )
        {
            $invite_code = '';
            if( isset($posty['invite_code']) ) {
                $invite_code = $posty['invite_code'];
            }
            $mobile =  '+' . preg_replace( '/[^0-9]/', '', $this->post('mobile') );
            $driver = $this->Driver_m->register($this->post('email'), $this->post('firstname'), $this->post('lastname'), $mobile, $this->post('taxi_association'), $this->post('country_id'), $this->post('city_id'), $invite_code);
            if( !$driver )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Driver already exists with this email address/phone number');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $driver, 'msg' => 'Driver Registered');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    public function update_photo_post()
    {
        if( $this->post('driver_id') )
        {
            if( $_FILES['photo']['name'] != '' )
            {
                $config['upload_path']          = './' . DRIVER_PHOTO_PATH;
                $config['allowed_types']        = 'jpeg|jpg|png';
                $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
                $this->load->library('upload', $config);
                        
                if ( !$this->upload->do_upload('photo'))
                {
                    $output = array('status' => false, 'data' => null, 'msg' => 'Invalid image type, please upload valid jpeg/png/jpg image');
                    $this->response_code = 404;
                }
                else
                {
                    $p_data = $this->upload->data();
                    $photo = $p_data['file_name'];
                    $data = array(
                        'photo' => $photo
                    );
                    $this->Driver_m->edit($data, array('id' => $this->post('driver_id')));
                    $driver = $this->Driver_m->getCompleteInfo($this->post('driver_id'));
                    $output = array('status' => true, 'data' => $driver, 'msg' => 'Photo has been updated');
                }
                
            }
            else 
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No image found');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
    }
    
    public function update_document_post()
    {
        if( $this->post('driver_id') && $this->post('document_type') )
        {
            $document_type = $this->post('document_type');
            $driver_id = $this->post('driver_id');
            if( $_FILES['document']['name'] != '' )
            {
                $config['upload_path']          = './' . DRIVER_DOC_PATH;
                $config['allowed_types']        = 'jpeg|jpg|png|doc|pdf|docx';
                $config['file_name']            = $driver_id . '_' . md5($_FILES['document']['name']) . '_' . time();
                
                $this->load->library('upload', $config);
                        
                if ( !$this->upload->do_upload('document'))
                {
                    $output = array('status' => false, 'data' => null, 'msg' => 'Invalid file, please upload valid jpeg/png/jpg/doc/pdf/docx file');
                    $this->response_code = 404;
                }
                else
                {
                    $p_data = $this->upload->data();
                    $doc = $p_data['file_name'];
                    $data = array(
                        $document_type => $doc
                    );
                    $where = array(
                        'driver_id' => $driver_id
                    );
                    $this->Driver_m->editDoc($data, $where);
                    $driver = $this->Driver_m->getCompleteInfo($driver_id);
                    $output = array('status' => true, 'data' => $driver, 'msg' => 'Document has been updated');
                }
                
            }
            else 
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No document found');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
    }
    
    public function update_option_post()
	{
        $posty = $_POST;
        if( $this->post('driver_id') && $this->post('option_key') && array_key_exists('option_value', $posty) )
        {
            $options = array(
                $this->post('option_key') => $posty['option_value']
            );
            $where = array(
                'driver_id' => $this->post('driver_id')
            );
            $options = $this->Driver_m->editOptions($options, $where);
            if( !$options )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Option could not updated');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $options, 'msg' => 'Option has been updated');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    
    public function update_status_post()
	{
        $posty = $_POST;
        if( $this->post('driver_id') && array_key_exists('status', $posty) )
        {
            $where = array(
                'driver_id' => $this->post('driver_id')
            );
            $status = $this->Driver_m->editStatus($posty['status'], $where);
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Your account is now under review and an email of confirmation will follow shortly.');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => null, 'msg' => 'Operation performed successfully.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    public function options_get()
	{
        if( $this->get('driver_id') )
        {
            $options = $this->Driver_m->getOptions($this->get('driver_id'));
            if( !$options )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No options found');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $options, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}

    public function reports_get()
    {
        if( $this->get('driver_id') )
        {
            $reports = $this->Driver_m->getReports($this->get('driver_id'));
            if( !$reports )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No reports found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $reports, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function cancellation_reasons_get()
    {
        $reasons = $this->Driver_m->getCancellationReasons();
        if( !$reasons )
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'No reasons found');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => true, 'data' => $reasons, 'msg' => '');
        }

        $this->response( $output, $this->response_code );
    }

    public function rating_messages_get()
    {
        $messages = $this->Driver_m->getRatingMessages();
        if( !$messages )
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'No messages found');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => true, 'data' => $messages, 'msg' => '');
        }

        $this->response( $output, $this->response_code );
    }

    public function reservations_get()
    {
        if( $this->get('driver_id') )
        {
            $this->load->model('Ride_m');
            $reservations = $this->Ride_m->getDriverReservations($this->get('driver_id'));
            if( !$reservations )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No reservations found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $reservations, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }


    public function rate_passenger_post()
    {
        $posty = $_POST;
        if( $this->post('rating') && $this->post('ride_id') )
        {
            $rating_message_id = 0;
            if( array_key_exists('rating_message_id', $posty) ) {
                $rating_message_id = $_POST['rating_message_id'];
            }
            $notes = '';
            if( array_key_exists('notes', $posty) ) {
                $notes = $_POST['notes'];
            }
            $data = array(
                'type' => 1,    // Driver
                'ride_id' => $this->post('ride_id'),
                'rating' => $this->post('rating'),
                'rating_message_id' => $rating_message_id,
                'notes' => $notes,
                'date_add' => datenow()
            );
            $this->load->model('Ride_m');
            $status = $this->Ride_m->rateRide($data);
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not rate this passenger.');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => null, 'msg' => 'Rating has been saved.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function logout_get()
    {
        if( $this->get('driver_id') )
        {
            if( !$this->Driver_m->logout($this->get('driver_id')) )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not logout');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => null, 'msg' => 'Logout successfully');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }
    
    public function documents_get()
	{
        if( $this->get('driver_id') )
        {
            $documents = $this->Driver_m->getDocuments($this->get('driver_id'));
            if( !$documents )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No documents found');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $documents, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}

    public function history_get()
    {
        if( $this->get('driver_id') && $this->get('date') )
        {
            $history = $this->Driver_m->getHistory($this->get('driver_id'), $this->get('date'));
            if( !$history )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No history found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $history, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function rating_get()
    {
        if( $this->get('driver_id') )
        {
            $rating = $this->Driver_m->getRatings($this->get('driver_id'));
            if( !$rating )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No ratings found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $rating, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function vehicle_get()
    {
        if( $this->get('driver_id') )
        {
            $vehicle = $this->Driver_m->getVehicle($this->get('driver_id'));
            if( !$vehicle )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No vehicle found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $vehicle, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }
    
    public function forgot_post()
	{
        if( $this->post('email') )
        {
            $code = $this->Driver_m->forgotPassword($this->post('email'));
            if( $code )
            {
                $this->load->library('email');
                $config['mailtype'] = 'html';
                $this->email->initialize($config);
                
                $this->email->from(APP_EMAIL, APP_COMPANY);
                $this->email->to($this->post('email'));
                
                $this->email->subject('Password Reset Request');
                $this->email->message('<p>Please use the code below to reset your password: </p><p><strong>'. $code .'</strong></p>');
                
                $output = array('status' => true, 'data' => null, 'msg' => 'Please use the code sent to your email address to set new password.');
            }
            else 
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No email/phone found');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    public function reset_post()
	{
        if( $this->post('code') && $this->post('password') )
        {
            $status = $this->Driver_m->resetPassword($this->post('code'), $this->post('password'));
            if( $status )
            {   
                $output = array('status' => true, 'data' => null, 'msg' => 'Password has been reset successfully. Please login with your new password.');
            }
            else 
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid reset code');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-API-KEY, Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once APPPATH . 'libraries/REST_Controller.php';

class Vehicle_type extends REST_Controller {
    
    private $response_code = 200;
    
    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();  
        $this->load->model('Vehicle_type_m');
    }
    
    public function get_all_get()
	{
        $vehicle_types = $this->Vehicle_type_m->getAll();
        if( !$vehicle_types )
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'No ride available in your area');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => true, 'data' => $vehicle_types, 'msg' => '');
        }
	   
        $this->response( $output, $this->response_code );
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: X-API-KEY, Content-Type, Content-Range, Content-Disposition, Content-Description');

require_once APPPATH . 'libraries/REST_Controller.php';

class Passenger extends REST_Controller {
    
    private $response_code = 200;
    
    public function __construct()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
        parent::__construct();  
        $this->load->model('Passenger_m');
    }
    
    public function send_code_post()
	{
        if( $this->post('mobile') )
        {
            $mobile =  '+' . preg_replace( '/[^0-9]/', '', $this->post('mobile') );
            $code = $this->Passenger_m->verifyMobileAndGenerateCode($mobile);
            if( $code == 0 )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No account is associated with this mobile number.');
                $this->response_code = 404;
            }
            else if( $code == 1 )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'This account is already in use in other device.');
                $this->response_code = 403;
            }
            else
            {
                $this->load->library('Sms');
                $message = 'Your NVOII code is: ' . $code;
                $this->sms->send_message($mobile, $message);
                $output = array('status' => true, 'data' => null, 'msg' => 'Please enter the code received via SMS.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}


    public function device_token_post()
    {
        if( $this->post('token') && $this->post('platform') && $this->post('user_id') )
        {
            $status = $this->Passenger_m->saveToken($this->post('token'), $this->post('platform'), $this->post('user_id'));
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not save token');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $status, 'msg' => 'Token saved');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function cancel_ride_get()
    {
        if( $this->get('ride_id') )
        {
            $this->load->model('Ride_m');
            $data = array(
                'is_cancelled' => 1,
                'date_update' => datenow()
            );
            $where = array(
                'id' => $this->get('ride_id')
            );
            if( $this->Ride_m->edit($data, $where) )
            {
                $output = array('status' => true, 'data' => $data, 'msg' => 'Ride cancelled');
            }
            else
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid Ride ID');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function verify_code_post()
	{
        if( $this->post('mobile') && $this->post('code') )
        {
            $mobile =  '+' . preg_replace( '/[^0-9]/', '', $this->post('mobile') );
            $data = $this->Passenger_m->verifyCode($mobile, $this->post('code'));
            if( $data )
            {
                $output = array('status' => true, 'data' => $data, 'msg' => 'Code verified.');
            }
            else
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid code.');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
    public function update_photo_post()
    {
        if( $this->post('passenger_id') )
        {
            if( $_FILES['photo']['name'] != '' )
            {
                $config['upload_path']          = './' . PASSENGER_PHOTO_PATH;
                $config['allowed_types']        = 'jpeg|jpg|png';
                $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
                $this->load->library('upload', $config);
                        
                if ( !$this->upload->do_upload('photo'))
                {
                    $output = array('status' => false, 'data' => null, 'msg' => 'Invalid image type, please upload valid jpeg/png/jpg image');
                    $this->response_code = 404;
                }
                else
                {
                    $p_data = $this->upload->data();
                    $photo = $p_data['file_name'];
                    $data = array(
                        'photo' => $photo
                    );
                    $this->Passenger_m->edit($data, array('id' => $this->post('passenger_id')));
                    $passenger = $this->Passenger_m->getCompleteInfo($this->post('passenger_id'));
                    $output = array('status' => true, 'data' => $passenger, 'msg' => 'Photo has been updated');
                }
                
            }
            else 
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No image found');
                $this->response_code = 404;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
    }
    
   
    public function update_option_post()
	{
        $posty = $_POST;
        if( $this->post('passenger_id') && $this->post('option_key') && array_key_exists('option_value', $posty) )
        {
            $options = array(
                $this->post('option_key') => $posty['option_value']
            );
            $where = array(
                'passenger_id' => $this->post('passenger_id')
            );
            $options = $this->Passenger_m->editOptions($options, $where);
            if( !$options )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Option could not updated');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $options, 'msg' => 'Option has been updated');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}

    public function save_location_post()
    {
        if( $this->post('passenger_id') && $this->post('label') && $this->post('icon') && $this->post('latitude') && $this->post('longitude') && $this->post('address')  )
        {
            $data = array(
                'passenger_id' => $this->post('passenger_id'),
                'label' => $this->post('label'),
                'icon' => $this->post('icon'),
                'latitude' => $this->post('latitude'),
                'longitude' => $this->post('longitude'),
                'address' => $this->post('address')
            );
            $locations = $this->Passenger_m->saveLocation($data);
            if( !$locations )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Location could not be saved.');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $locations, 'msg' => 'Location saved successfully.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function save_card_post()
    {
        if( $this->post('passenger_id') && $this->post('card_number') && $this->post('expiry') && $this->post('cvv') && $this->post('zip')  )
        {
            $card_number = str_replace(" ", "", $this->post('card_number'));
            $expiry = explode('/', $this->post('expiry'));
            $cvv = $this->post('cvv');
            $this->load->library('creditcard');
            // Validate CC
            $res = $this->creditcard->validCreditCard($this->post('card_number'));
            if( $res['valid'] ) {
                // Success
                $type = $res['type'];
                if( $this->creditcard->validDate('20' . $expiry[1], $expiry[0]) ) {
                    // Success
                    if( $this->creditcard->validCvc($cvv, $type) ) {
                        // Success
                        $data = array(
                            'passenger_id' => $this->post('passenger_id'),
                            'card_number' => $card_number,
                            'expiry_month' => $expiry[0],
                            'expiry_year' => $expiry[1],
                            'card_type' => $type,
                            'cvv' => $cvv,
                            'zip' => $this->post('zip')
                        );
                        $cards = $this->Passenger_m->saveCard($data);
                        if ( $cards ) {
                            $output = array('status' => true, 'data' => $cards, 'msg' => 'Card saved successfully.');
                        } else {
                            $output = array('status' => false, 'data' => null, 'msg' => 'Card could not be saved.');
                            $this->response_code = 400;
                        }

                    } else {
                        $output = array('status' => false, 'data' => null, 'msg' => 'Invalid CVV.');
                        $this->response_code = 400;
                    }
                } else {
                    $output = array('status' => false, 'data' => null, 'msg' => 'Card is expired.');
                    $this->response_code = 400;
                }
            } else {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid Card Number.');
                $this->response_code = 400;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function update_card_post()
    {
        if( $this->post('card_id') && $this->post('passenger_id') && $this->post('card_number') && $this->post('expiry') && $this->post('cvv') && $this->post('zip')  )
        {
            $card_number = str_replace(" ", "", $this->post('card_number'));
            $expiry = explode('/', $this->post('expiry'));
            $cvv = $this->post('cvv');
            $this->load->library('creditcard');
            // Validate CC
            $res = $this->creditcard->validCreditCard($this->post('card_number'));
            if( $res['valid'] ) {
                // Success
                $type = $res['type'];
                if( $this->creditcard->validDate('20' . $expiry[1], $expiry[0]) ) {
                    // Success
                    if( $this->creditcard->validCvc($cvv, $type) ) {
                        // Success
                        $data = array(
                            'card_number' => $card_number,
                            'expiry_month' => $expiry[0],
                            'expiry_year' => $expiry[1],
                            'card_type' => $type,
                            'cvv' => $cvv,
                            'zip' => $this->post('zip')
                        );
                        $where = array(
                            'id' => $this->input->post('card_id')
                        );
                        $cards = $this->Passenger_m->updateCard($data, $where);
                        if ( $cards ) {
                            $output = array('status' => true, 'data' => $cards, 'msg' => 'Card updated successfully.');
                        } else {
                            $output = array('status' => false, 'data' => null, 'msg' => 'Card could not be updated.');
                            $this->response_code = 400;
                        }

                    } else {
                        $output = array('status' => false, 'data' => null, 'msg' => 'Invalid CVV.');
                        $this->response_code = 400;
                    }
                } else {
                    $output = array('status' => false, 'data' => null, 'msg' => 'Card is expired.');
                    $this->response_code = 400;
                }
            } else {
                $output = array('status' => false, 'data' => null, 'msg' => 'Invalid Card Number.');
                $this->response_code = 400;
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }
    
    public function options_get()
	{
        if( $this->get('passenger_id') )
        {
            $options = $this->Passenger_m->getOptions($this->get('passenger_id'));
            if( !$options )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No options found');
                $this->response_code = 404;
            }
            else 
            {
                $output = array('status' => true, 'data' => $options, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}

    public function cards_get()
    {
        if( $this->get('passenger_id') )
        {
            $cards = $this->Passenger_m->getCards($this->get('passenger_id'));
            if( !$cards )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No cards found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $cards, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function saved_locations_get()
    {
        if( $this->get('passenger_id') )
        {
            $locations = $this->Passenger_m->getSavedLocations($this->get('passenger_id'));
            if( !$locations )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No locations found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $locations, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function booking_history_get()
    {
        if( $this->get('passenger_id') )
        {
            $this->load->model('Ride_m');
            $rides = $this->Ride_m->getRidesByPassengerID($this->get('passenger_id'));
            if( !$rides )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No rides found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $rides, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function cancellation_reasons_get()
    {
        $reasons = $this->Passenger_m->getCancellationReasons();
        if( !$reasons )
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'No reasons found');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => true, 'data' => $reasons, 'msg' => '');
        }

        $this->response( $output, $this->response_code );
    }


    public function rating_messages_get()
    {
        $messages = $this->Passenger_m->getRatingMessages();
        if( !$messages )
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'No messages found');
            $this->response_code = 404;
        }
        else
        {
            $output = array('status' => true, 'data' => $messages, 'msg' => '');
        }

        $this->response( $output, $this->response_code );
    }

    public function logout_get()
    {
        if( $this->get('passenger_id') )
        {
            if( !$this->Passenger_m->logout($this->get('passenger_id')) )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not logout');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => null, 'msg' => 'Logout successfully');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function rating_get()
    {
        if( $this->get('passenger_id') )
        {
            $rating = $this->Passenger_m->getRatings($this->get('passenger_id'));
            if( !$rating )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'No ratings found');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => $rating, 'msg' => '');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }

    public function rate_driver_post()
    {
        $posty = $_POST;
        if( $this->post('rating') && $this->post('ride_id') )
        {
            $rating_message_id = 0;
            if( array_key_exists('rating_message_id', $posty) ) {
                $rating_message_id = $_POST['rating_message_id'];
            }
            $notes = '';
            if( array_key_exists('notes', $posty) ) {
                $notes = $_POST['notes'];
            }
            $data = array(
                'type' => 2,    // Passenger
                'ride_id' => $this->post('ride_id'),
                'rating' => $this->post('rating'),
                'rating_message_id' => $rating_message_id,
                'notes' => $notes,
                'date_add' => datenow()
            );
            $this->load->model('Ride_m');
            $status = $this->Ride_m->rateRide($data);
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Could not rate this driver.');
                $this->response_code = 404;
            }
            else
            {
                $output = array('status' => true, 'data' => null, 'msg' => 'Rating has been saved.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }

        $this->response( $output, $this->response_code );
    }
    
    public function update_profile_post()
	{
        if( $this->post('passenger_id') && $this->post('firstname') && $this->post('lastname') && $this->post('email') )
        {
            $data = array(
                'firstname' => $this->post('firstname'),
                'lastname' => $this->post('lastname'),
                'email' => $this->post('email')
            );
            $where = array(
                'id' => $this->post('passenger_id')
            );
            $status = $this->Passenger_m->edit($data, $where);
            if( !$status )
            {
                $output = array('status' => false, 'data' => null, 'msg' => 'Email already exists');
                $this->response_code = 404;
            }
            else 
            {
                $info = $this->Passenger_m->getCompleteInfo($this->post('passenger_id'));
                $output = array('status' => true, 'data' => $info, 'msg' => 'Profile has been updated.');
            }
        }
        else
        {
            $output = array('status' => false, 'data' => null, 'msg' => 'Missing required parameters');
            $this->response_code = 400;
        }
	   
        $this->response( $output, $this->response_code );
	}
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
    }
    
	public function index()
	{
        $data['pageMeta'] = array(
            'title' => 'Dashboard'
        );
        $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
        $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
        $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
		$this->load->view(THEME . '/dashboard', $data);
	}
}

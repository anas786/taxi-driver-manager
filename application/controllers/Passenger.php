<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passenger extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('Passenger_m');
    }
    
	public function index()
	{
        if( hasAccess($this->session->user_role, 'PASSENGER_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Passenger List'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1,
                            'date_update' => datenow()
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Passenger_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/passenger/list', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllPassengers()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->Passenger_m->getAllPassengers($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'PASSENGER_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New Passenger'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('firstname', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('lastname', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'valid_email', 'trim'));
                $this->form_validation->set_rules('mobile', 'Mobile Number', array('required', 'trim'));
                $this->form_validation->set_rules('password', 'Password', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $photo = PASSENGER_PHOTO_PATH . 'default.png';
                    $error = false;
                    if( $_FILES['photo']['name'] != '' ) 
                    {
                        $config['upload_path']          = './' . PASSENGER_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('photo'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $photo = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        $dat = array(
                            'firstname' => $this->input->post('firstname'),
                            'lastname' => $this->input->post('lastname'),
                            'email' => $this->input->post('email'),
                            'mobile' => $this->input->post('mobile'),
                            'password' => md5($this->input->post('password')),
                            'photo' => $photo,
                            'country_id' => $this->input->post('country_id'),
                            'is_active' => isset($_POST['is_active']) ? 1 : 0,
                            'date_add' => datenow(),
                            'date_update' => datenow(),
                            'date_registered' => datenow()
                        );
                        $id = $this->Passenger_m->add($dat);
                        if( !$id )
                        {
                            $this->session->set_flashdata('msg_error', 'Passenger with this email address already exists.');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg_success', 'Passenger has been added successfully.');
                            redirect( 'passenger/edit/' . $id );
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/passenger/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'PASSENGER_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit User'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('firstname', 'First Name', array('required', 'trim'));
                $this->form_validation->set_rules('lastname', 'Last Name', array('required', 'trim'));
                $this->form_validation->set_rules('email', 'Email Address', array('required', 'valid_email', 'trim'));
                $this->form_validation->set_rules('mobile', 'Mobile Number', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $error = false;
                    $photo = '';
                    if( $_FILES['photo']['name'] != '' ) 
                    {
                        $config['upload_path']          = './' . PASSENGER_PHOTO_PATH;
                        $config['allowed_types']        = 'jpeg|jpg|png';
                        $config['file_name']            = md5($_FILES['photo']['name']) . '_' . time();
        
                        $this->load->library('upload', $config);
                        
                        if ( !$this->upload->do_upload('photo'))
                        {
                            $error = true;
                            $this->session->set_flashdata('msg_error', $this->upload->display_errors());
                        }
                        else
                        {
                            $p_data = $this->upload->data();
                            $photo = $p_data['file_name'];
                        }
                    }
                    if( !$error )
                    {
                        if( $_POST['password'] != '' && $photo != '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'password' => md5($this->input->post('password')),
                                'photo' => $photo,
                                'country_id' => $this->input->post('country_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        else if( $_POST['password'] != '' && $photo == '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'password' => md5($this->input->post('password')),
                                'country_id' => $this->input->post('country_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_registered' => datenow()
                            );
                        }
                        else if( $_POST['password'] == '' && $photo != '' )
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'country_id' => $this->input->post('country_id'),
                                'photo' => $photo,
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        else
                        {
                            $dat = array(
                                'firstname' => $this->input->post('firstname'),
                                'lastname' => $this->input->post('lastname'),
                                'email' => $this->input->post('email'),
                                'mobile' => $this->input->post('mobile'),
                                'country_id' => $this->input->post('country_id'),
                                'is_active' => isset($_POST['is_active']) ? 1 : 0,
                                'date_update' => datenow(),
                                'date_registered' => datenow()
                            );
                        }
                        $where = array(
                            'id' => $this->input->post('id')
                        );
                        if( $this->Passenger_m->edit($dat, $where) )
                        {
                            $this->session->set_flashdata('msg_success', 'Passenger has been updated successfully.');
                        }
                        else
                        {
                            $this->session->set_flashdata('msg_error', 'Passenger with this email address already exists.');
                        }
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }
            
            $this->load->model('Localization_m');
            $data['countries'] = $this->Localization_m->getCountries();
            
            $data['passenger'] = $this->Passenger_m->getInfo($id);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/passenger/edit', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'PASSENGER_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1
                );
                $where = array(
                    'id' => $id
                );
                if( $this->Passenger_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'Passenger has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Passenger cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid Passenger ID.');
            }
            redirect( 'passenger' );
        }
        else
        {
            notallowed();
        }
    }
    
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vehicle_model extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if( !isLoggedIn() ) {
            $this->session->set_flashdata('msg_error', 'You need to login before accessing this page');
            redirect( 'auth' );
        }
        $this->load->model('Vehicle_model_m');
    }
    
	public function index()
	{
        if( hasAccess($this->session->user_role, 'VEHICLE_MODEL_LIST') )
        {
            $data['pageMeta'] = array(
                'title' => 'Vehicle Model List'
            );
            
            if( isset($_POST['bulk_delete']) )
            {
                if( isset($_POST['ids']) )
                {
                    foreach( $_POST['ids'] as $id )
                    {
                        $dat = array(
                            'is_delete' => 1
                        );
                        $where = array(
                            'id' => $id
                        );
                        $this->Vehicle_model_m->edit($dat, $where);
                    }
                    $this->session->set_flashdata('msg_success', 'Selected records have been removed successfully.');
                }
            }
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_model/list', $data);
        }
        else
        {
            notallowed();
        }
        
	}
    
    public function getAllVehicleModels()
    {
        if($this->input->is_ajax_request())
        {
			$get = $this->input->get();
			
			$draw = 1;
			$start = 0;
			$length = 10;
			$search = '';
			$order = array();
	
			if(is_array($get)){
				$draw	= array_key_exists('draw', $get)		? $get['draw']		: 1;
				$start	= array_key_exists('start', $get)		? $get['start']		: 0;
				$length = array_key_exists('length', $get)		? $get['length']	: 10;
				$search = array_key_exists('search', $get)		? $get['search']	: '';
				$search = array_key_exists('value', $search)	? $search['value']	: '';
				$order	= array_key_exists('order', $get)		? $get['order']	: '';
				$order	= !is_array($order) ? array() : $order;
			}
	
			$offset = $start * $length;		
			$data = $this->Vehicle_model_m->getAllVehicleModels($draw, $start, $length, $search, $order);
			
			
			echo json_encode($data);
            die;
		}
    }
    
    public function add() 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_MODEL_ADD') )
        {
            $data['pageMeta'] = array(
                'title' => 'Add New Vehicle Model'
            );
            
            if( isset($_POST['submit']) )
            {
                if( ($_POST['make'] != '' || $_POST['add_make'] != '') && ($_POST['model'] != '' || $_POST['add_model'] != '') && ($_POST['year'] != '' || $_POST['add_year'] != '') )
                {
                    $make = $this->input->post('add_make');
                    if( $_POST['make'] != '' ) {
                        $make = $this->input->post('make');
                    }
                    $model = $this->input->post('add_model');
                    if( $_POST['model'] != '' ) {
                        $model = $this->input->post('model');
                    }
                    $year = $this->input->post('add_year');
                    if( $_POST['year'] != '' ) {
                        $year = $this->input->post('year');
                    }

                    $dat = array(
                        'make' => $make,
                        'model' => $model,
                        'year' => $year
                    );
                    $id = $this->Vehicle_model_m->add($dat);
                    if( !$id )
                    {
                        $this->session->set_flashdata('msg_error', 'Vehicle Model with this name already exists.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_success', 'Vehicle Model has been added successfully.');
                        redirect( 'vehicle_model/edit/' . $id );
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }

            $data['models'] = $this->Vehicle_model_m->getModels();
            $data['makes'] = $this->Vehicle_model_m->getMakes();
            $data['years'] = $this->Vehicle_model_m->getYears();
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_model/add', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function edit($id=0) 
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_MODEL_EDIT') )
        {
            $data['pageMeta'] = array(
                'title' => 'Edit Vehicle Model'
            );
            
            if( isset($_POST['submit']) )
            {
                $this->form_validation->set_rules('make', 'Make', array('required', 'trim'));
                $this->form_validation->set_rules('model', 'Model', array('required', 'trim'));
                $this->form_validation->set_rules('year', 'Year', array('required', 'trim'));
                
                if( $this->form_validation->run() == TRUE )
                {
                    $dat = array(
                        'make' => $this->input->post('make'),
                        'model' => $this->input->post('model'),
                        'year' => $this->input->post('year')
                    );
                    $where = array(
                        'id' => $this->input->post('id')
                    );
                    if( $this->Vehicle_model_m->edit($dat, $where) )
                    {
                        $id = $this->input->post('id');
                        $this->session->set_flashdata('msg_success', 'Vehicle Model has been updated successfully.');
                    }
                    else
                    {
                        $this->session->set_flashdata('msg_error', 'Vehicle Model with this name already exists.');
                    }
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Please fill out all the required (*) fields.');
                }
            }

            $data['models'] = $this->Vehicle_model_m->getModels();
            $data['makes'] = $this->Vehicle_model_m->getMakes();
            $data['years'] = $this->Vehicle_model_m->getYears();
            
            $data['vehicle_model'] = $this->Vehicle_model_m->getInfo($id);
            
            $data['header'] = $this->load->view(THEME . '/layout/header', $data, true);
            $data['sidebar'] = $this->load->view(THEME . '/layout/sidebar', $data, true);
            $data['footer'] = $this->load->view(THEME . '/layout/footer', $data, true);
    		$this->load->view(THEME . '/vehicle_model/edit', $data);
        }
        else
        {
            notallowed();
        }
    }
    
    public function delete($id=null)
    {
        if( hasAccess($this->session->user_role, 'VEHICLE_MODEL_DELETE') )
        {
            if( $id != null )
            {
                $dat = array(
                    'is_delete' => 1
                );
                $where = array(
                    'id' => $id
                );
                if( $this->Vehicle_model_m->edit($dat, $where) )
                {
                    $this->session->set_flashdata('msg_success', 'Vehicle Model has been deleted successfully.');
                }
                else
                {
                    $this->session->set_flashdata('msg_error', 'Vehicle Model cannot be deleted this time.');
                }
            }
            else 
            {
                $this->session->set_flashdata('msg_error', 'Invalid Vehicle Model ID.');
            }
            redirect( 'vehicle_model' );
        }
        else
        {
            notallowed();
        }
    }

}

    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">local_taxi</i>
            <h3>Edit <span class="semi-bold">Vehicle</span></h3>
            <a href="<?php echo base_url('vehicle'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
          </div>
          <div class="row-fluid">
            <div class="span12">
            <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
            <?php echo form_open_multipart('vehicle/edit/' . $vehicle['id'], $attr); ?>
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <h4>Edit Vehicle</h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <?php if( isset($_SESSION['msg_error']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_error'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset($_SESSION['msg_success']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_success'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="form-group">
                                <label class="form-label" for="name">Driver *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="driver_id" id="driver_id">
                                        <option value="">-- Select Driver --</option>
                                        <?php foreach( $drivers as $driver ): ?>
                                            <option value="<?php echo $driver['id']; ?>" <?php if( $driver['id'] == $vehicle['driver_id'] ){ echo 'selected'; } ?>><?php echo $driver['lastname'] . ' ' . $driver['firstname']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="vehicle_make_model_id">Vehicle *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="vehicle_make_model_id" id="vehicle_make_model_id">
                                        <option value="">-- Select Vehicle --</option>
                                        <?php foreach( $vehicle_make_models as $make ): ?>
                                            <option value="<?php echo $make['id']; ?>" <?php if( $make['id'] == $vehicle['vehicle_make_model_id'] ){ echo 'selected'; } ?>><?php echo $make['make'] . ' ' . $make['model'] . ' ' . $make['year']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="color_id">Color *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="color_id" id="color_id">
                                        <option value="">-- Select Color --</option>
                                        <?php foreach( $colors as $color ): ?>
                                            <option value="<?php echo $color['id']; ?>" <?php if( $color['id'] == $vehicle['color_id'] ){ echo 'selected'; } ?>><?php echo $color['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="vehicle_type_id">Vehicle Type</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="vehicle_type_id" id="vehicle_type_id">
                                        <option value="">-- Select Vehicle Type --</option>
                                        <?php foreach( $vehicle_types as $vehicle_type ): ?>
                                            <option value="<?php echo $vehicle_type['id']; ?>" <?php if( $vehicle_type['id'] == $vehicle['vehicle_type_id'] ){ echo 'selected'; } ?>><?php echo $vehicle_type['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="plate_number">Plate Number *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="plate_number" id="plate_number" value="<?php echo $vehicle['plate_number']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="is_active">Active?</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <div class="slide-primary">
                                        <input type="checkbox" name="is_active" class="js-switch" <?php if( $vehicle['is_active'] ){ echo 'checked'; } ?> />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-actions">
                                <div class="pull-right">
                                    <input type="hidden" name="submit" value="">
                                    <input type="hidden" name="id" value="<?php echo $vehicle['id']; ?>">
                                    <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                    <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">local_car_wash</i>
            <h3>Add New <span class="semi-bold">Vehicle Model</span></h3>
            <a href="<?php echo base_url('vehicle_model'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
          </div>
          <div class="row-fluid">
            <div class="span12">
            <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
            <?php echo form_open_multipart('vehicle_model/add', $attr); ?>
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <h4>Add New Vehicle Model</h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <?php if( isset($_SESSION['msg_error']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_error'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset($_SESSION['msg_success']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_success'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <div class="form-group">
                                <label class="form-label" for="make">Make *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select name="make" id="make" class="select2">
                                        <option value="">- Select Make -</option>
                                        <?php foreach( $makes as $make ): ?>
                                        <option value="<?php echo $make['make']; ?>"><?php echo $make['make']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="model">Model *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select name="model" id="model" class="select2">
                                        <option value="">- Select Model -</option>
                                        <?php foreach( $models as $model ): ?>
                                            <option value="<?php echo $model['model']; ?>"><?php echo $model['model']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="year">Year *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select name="year" id="year" class="select2">
                                        <option value="">- Select Year -</option>
                                        <?php foreach( $years as $year ): ?>
                                            <option value="<?php echo $year['year']; ?>"><?php echo $year['year']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                            <div class="form-group">
                                <label class="form-label">&nbsp;</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <h3>OR</h3>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">&nbsp;</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <h3>OR</h3>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label">&nbsp;</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <h3>OR</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5">

                            <div class="form-group">
                                <label class="form-label" for="add_make">Add Make</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="add_make" id="add_make" class="form-control" value="<?php echo set_value('add_make') ?>" placeholder="Add New Make">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="add_model">Add Model</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="add_model" id="add_model" class="form-control" value="<?php echo set_value('add_model') ?>" placeholder="Add New Model">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="add_year">Add Year</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="add_year" id="add_year" class="form-control" value="<?php echo set_value('add_year') ?>" placeholder="Add New Year">
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12">
                            <div class="form-actions">
                                <div class="pull-right">
                                    <input type="hidden" name="submit" value="">
                                    <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                    <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
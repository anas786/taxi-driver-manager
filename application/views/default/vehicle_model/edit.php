    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">local_car_wash</i>
            <h3>Edit <span class="semi-bold">Vehicle Model</span></h3>
            <a href="<?php echo base_url('vehicle_model'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
          </div>
          <div class="row-fluid">
            <div class="span12">
            <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
            <?php echo form_open_multipart('vehicle_model/edit/' . $vehicle_model['id'], $attr); ?>
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <h4>Edit Vehicle Model</h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <?php if( isset($_SESSION['msg_error']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_error'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset($_SESSION['msg_success']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_success'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="form-group">
                                <label class="form-label" for="make">Make *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="make" id="make" value="<?php echo $vehicle_model['make']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="model">Model *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="model" id="model" value="<?php echo $vehicle_model['model']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="year">Year *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="number" name="year" id="year" value="<?php echo $vehicle_model['year']; ?>" class="form-control" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-actions">
                                <div class="pull-right">
                                    <input type="hidden" name="submit" value="">
                                    <input type="hidden" name="id" value="<?php echo $vehicle_model['id']; ?>">
                                    <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                    <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="row-fluid">
            <div class="span12">
                <div style="margin: 150px auto; text-align: center;">
                    <div><i class="fa fa-exclamation-triangle fa-4x"></i></div>
                    <h2>You are not allowed to access this module!</h2>
                    <a href="<?php echo base_url(); ?>" class="btn btn-cons btn-success">Go to Dashboard</a>
                </div>
            </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
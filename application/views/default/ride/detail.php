    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">assignment</i>
            <h3>Ride Detail <span class="semi-bold">(Booking ID: <?php echo $ride['id']; ?>)</span></h3>
            <?php if( hasAccess($this->session->user_role, 'RIDE_EDIT') ): ?>
            <a href="<?php echo base_url('ride/edit/' . $ride['id']); ?>" class="btn btn-cons btn-primary"><span class="fa fa-pencil"></span> Edit</a>
            <?php endif; ?>
          </div>
          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="active">
                                    <a href="#tabRideDetail" role="tab" data-toggle="tab">Overview</a>
                                </li>
                                <li>
                                    <a href="#tabRideHistory" role="tab" data-toggle="tab">History</a>
                                </li>
                                <li>
                                    <a href="#tabRideRating" role="tab" data-toggle="tab">Ratings</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tabRideDetail">
                                    <table class="table table-condensed">
                                        <tr>
                                            <th>Booking ID</th>
                                            <td><?php echo $ride['id']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ride Requested On</th>
                                            <td><?php echo easyDateTime($ride['date_add']); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Vehicle Type</th>
                                            <td><?php echo $ride['vehicle_type']['name']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ride Type</th>
                                            <td><?php echo $ride['ride_type_text']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>No. of Passengers</th>
                                            <td><?php echo $ride['passengers']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Pick-up Location</th>
                                            <td><?php echo $ride['pickup_location']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Drop-off Location</th>
                                            <td><?php echo $ride['dropoff_location']; ?></td>
                                        </tr>
                                        <tr>
                                            <th>Payment Method</th>
                                            <td><?php echo $ride['payment_method_info']['payment_method']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tabRideHistory">
                                    <table class="table table-condensed">
                                        <?php foreach( $ride['history'] as $history ): ?>
                                        <tr>
                                            <th><?php echo easyDateTime($history['datetime']); ?></th>
                                            <td>
                                                <?php
                                                    if( $history['status'] == 1 ) {
                                                        echo 'Ride Requested';
                                                    } else if( $history['status'] == 2 ) {
                                                        echo 'Ride Accepted by Driver';
                                                    } else if( $history['status'] == 3 ) {
                                                        echo 'Driver Arrived at Pick-up Location';
                                                    } else if( $history['status'] == 4 ) {
                                                        echo 'Ride Started';
                                                    } else if( $history['status'] == 5 ) {
                                                        echo 'Ride Completed';
                                                    } else if( $history['status'] == 6 ) {
                                                        echo 'Ride Cancelled';
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                                <div class="tab-pane" id="tabRideRating">
                                    <h4>Passenger's rating to Driver</h4>
                                    <table class="table table-condensed">
                                        <?php if( isset($ride['passenger_rating']) ): ?>
                                        <tr>
                                            <th>Rating</th>
                                            <td>
                                                <?php for($i = 1; $i <= 5; $i++): ?>
                                                <?php if( $i <= $ride['passenger_rating']['rating'] ): ?>
                                                <i class="material-icons">star</i>
                                                <?php else: ?>
                                                <i class="material-icons">star_border</i>
                                                <?php endif; ?>
                                                <?php endfor; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>What could be improved?</th>
                                            <td><?php echo $ride['passenger_rating']['message']; ?></td>
                                        </tr>
                                            <tr>
                                                <th>Notes</th>
                                                <td><?php echo $ride['passenger_rating']['notes']; ?></td>
                                            </tr>
                                        <?php else: ?>
                                        <tr>
                                            <th>Rating not given by Passenger</th>
                                        </tr>
                                        <?php endif; ?>
                                    </table>

                                    <h4>Driver's rating to Passenger</h4>
                                    <table class="table table-condensed">
                                        <?php if( isset($ride['driver_rating']) ): ?>
                                        <tr>
                                            <th>Rating</th>
                                            <td>
                                                <?php for($i = 1; $i <= 5; $i++): ?>
                                                    <?php if( $i <= $ride['driver_rating']['rating'] ): ?>
                                                        <i class="material-icons">star</i>
                                                    <?php else: ?>
                                                        <i class="material-icons">star_border</i>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>What went wrong?</th>
                                            <td><?php echo $ride['driver_rating']['message']; ?></td>
                                        </tr>
                                            <tr>
                                                <th>Notes</th>
                                                <td><?php echo $ride['driver_rating']['notes']; ?></td>
                                            </tr>
                                        <?php else: ?>
                                        <tr>
                                            <th>Rating not given by Driver</th>
                                        </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <img src="https://maps.googleapis.com/maps/api/staticmap?size=600x400&path=enc%3A<?php echo $ride_routes->routes[0]->overview_polyline->points; ?>&markers=color:0xB4E402|label:P|<?php echo $ride['pickup_coordinates']; ?>&markers=color:0x383d41|label:D|<?php echo $ride['dropoff_coordinates']; ?>&key=<?php echo GOOGLE_MAP_API_KEY; ?>" class="img-responsive">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>Passenger Details</h3>
                            <table class="table table-condensed">
                                <tr>
                                    <th>Name</th>
                                    <td><a href="<?php echo base_url('passenger/edit/' . $ride['passenger']['id']); ?>"><?php echo $ride['passenger']['lastname'] . ' ' . $ride['passenger']['firstname']; ?></a></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?php echo $ride['passenger']['email']; ?></td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td><?php echo $ride['passenger']['mobile']; ?></td>
                                </tr>
                                <tr>
                                    <th>Photo</th>
                                    <td><img src="<?php echo $ride['passenger']['photo']; ?>" width="80"></td>
                                </tr>
                                <tr>
                                    <th>Date Registered</th>
                                    <td><?php echo easyDateTime($ride['passenger']['date_registered']); ?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <h3>Driver Details</h3>
                            <table class="table table-condensed">
                                <tr>
                                    <th>Name</th>
                                    <td><a href="<?php echo base_url('driver/edit/' . $ride['driver']['id']); ?>"><?php echo $ride['driver']['lastname'] . ' ' . $ride['driver']['firstname']; ?></a></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><?php echo $ride['driver']['email']; ?></td>
                                </tr>
                                <tr>
                                    <th>Phone</th>
                                    <td><?php echo $ride['driver']['mobile']; ?></td>
                                </tr>
                                <tr>
                                    <th>Photo</th>
                                    <td><img src="<?php echo $ride['driver']['photo']; ?>" width="80"></td>
                                </tr>
                                <tr>
                                    <th>Date Registered</th>
                                    <td><?php echo easyDateTime($ride['driver']['date_registered']); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
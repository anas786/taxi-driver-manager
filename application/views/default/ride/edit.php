    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">supervisor_account</i>
            <h3>Add New <span class="semi-bold">Driver</span></h3>
            <a href="<?php echo base_url('driver'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
          </div>
          <div class="row-fluid">
            <div class="span12">
            <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
            <?php echo form_open_multipart('driver/edit/' . $driver['id'], $attr); ?>
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <h4>Edit Driver</h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <?php if( isset($_SESSION['msg_error']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_error'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset($_SESSION['msg_success']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_success'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="form-group">
                                <label class="form-label" for="firstname">First Name *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="firstname" id="firstname" value="<?php echo $driver['firstname']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="lastname">Last Name *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="lastname" id="lastname" value="<?php echo $driver['lastname']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="email">Email Address *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="email" name="email" id="email" class="form-control" value="<?php echo $driver['email']; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="password">Password</label>
                                <span class="help">(Leave blank if you don't want to change password)</span>
                                <div class="controls">
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="country_id">Country *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="country_id" id="country_id" onchange="fetchStates()">
                                        <option value="">--Select Country--</option>
                                        <?php foreach( $countries as $country ): ?>
                                        <option value="<?php echo $country['id']; ?>" <?php if( $country['id'] == $driver['country_id'] ){ echo 'selected'; } ?>><?php echo $country['name'] . ' (+' . $country['phonecode'] . ')'; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="state_id">State *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="state_id" id="state_id" onchange="fetchCities()">
                                        <?php foreach( $states as $state ): ?>
                                        <option value="<?php echo $state['id']; ?>" <?php if( $state['id'] == $driver['state_id'] ){ echo 'selected'; } ?>><?php echo $state['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="city_id">City *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="city_id" id="city_id">
                                        <?php foreach( $cities as $city ): ?>
                                        <option value="<?php echo $city['id']; ?>" <?php if( $city['id'] == $driver['city_id'] ){ echo 'selected'; } ?>><?php echo $city['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="mobile">Mobile *</label>
                                <span class="help">Without Country Code</span>
                                <div class="controls">
                                    <input type="text" name="mobile" id="mobile" value="<?php echo $driver['mobile']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="photo">Photo</label>
                                <span class="help"><img src="<?php echo base_url(DRIVER_PHOTO_PATH . $driver['photo']); ?>" width="40"></span>
                                <div class="controls">
                                    <input type="file" name="photo" id="photo" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="is_active">Active?</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <div class="slide-primary">
                                        <input type="checkbox" name="is_active" class="js-switch" <?php if( $driver['is_active'] ){ echo 'checked'; } ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="form-group">
                                <label class="form-label" for="driving_license">Driving License</label>
                                <span class="help"><a href="<?php echo base_url(DRIVER_DOC_PATH . $driver['driving_license']); ?>" target="_blank"><i class="fa fa-2x fa-file-text"></i></a></span>
                                <div class="controls">
                                    <input type="file" name="driving_license" id="driving_license" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="insurance_certificate">Insurance Certificate</label>
                                <span class="help"><a href="<?php echo base_url(DRIVER_DOC_PATH . $driver['insurance_certificate']); ?>" target="_blank"><i class="fa fa-2x fa-file-text"></i></a></span>
                                <div class="controls">
                                    <input type="file" name="insurance_certificate" id="insurance_certificate" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="inspection_certificate">Inspection Certificate</label>
                                <span class="help"><a href="<?php echo base_url(DRIVER_DOC_PATH . $driver['inspection_certificate']); ?>" target="_blank"><i class="fa fa-2x fa-file-text"></i></a></span>
                                <div class="controls">
                                    <input type="file" name="inspection_certificate" id="inspection_certificate" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="taxi_association_number">Taxi Association Number</label>
                                <span class="help"><a href="<?php echo base_url(DRIVER_DOC_PATH . $driver['taxi_association_number']); ?>" target="_blank"><i class="fa fa-2x fa-file-text"></i></a></span>
                                <div class="controls">
                                    <input type="file" name="taxi_association_number" id="taxi_association_number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="private_hire_vehicle_license">Private Hire Vehicle License</label>
                                <span class="help"><a href="<?php echo base_url(DRIVER_DOC_PATH . $driver['private_hire_vehicle_license']); ?>" target="_blank"><i class="fa fa-2x fa-file-text"></i></a></span>
                                <div class="controls">
                                    <input type="file" name="private_hire_vehicle_license" id="private_hire_vehicle_license" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-actions">
                                <div class="pull-right">
                                    <input type="hidden" name="submit" value="">
                                    <input type="hidden" name="id" value="<?php echo $driver['id']; ?>">
                                    <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                    <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">person_pin</i>
            <h3>List <span class="semi-bold">Driver</span></h3>
            <a href="<?php echo base_url('driver/add'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-plus"></span> Add New Driver</a>
          </div>
          <?php if( isset($_SESSION['msg_error']) ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_error'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if( isset($_SESSION['msg_success']) ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_success'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php $attr = array("onsubmit" => "return confirm('Are you sure that you want to remove selected drivers?')"); ?>
            <?php echo form_open('driver', $attr); ?>
          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple ">
                <div class="grid-title">
                  <h4>List Drivers</h4>
                  <div class="tools">
                    Bulk Operation: <input type="submit" class="btn btn-cons btn-danger" disabled id="bulk_delete" name="bulk_delete" value="Delete">
                  </div>
                </div>
                <div class="grid-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-condensed" id="example" data-url="<?php echo base_url('driver/getAllDrivers'); ?>">
                        <thead>
                          <tr>
                            <th style="width:1%">
                              <div class="checkbox check-default" style="margin-right:auto;margin-left:auto;">
                                <input type="checkbox" value="1" id="checkboxAll">
                                <label for="checkboxAll"></label>
                              </div>
                            </th>
                            <th style="width:8%">Photo</th>
                            <th style="width:15%">Full Name</th>
                            <th style="width:20%">Email</th>
                            <th style="width:12%">Mobile</th>
                            <th style="width:10%">Country</th>
                            <th style="width:10%" data-hide="phone,tablet">Status</th>
                            <th style="width:12%" data-hide="phone,tablet">Registered</th>
                            <th style="width:15%">Actions</th>
                          </tr>
                        </thead>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">airport_shuttle</i>
            <h3>List <span class="semi-bold">Vehicle Type</span></h3>
            <a href="<?php echo base_url('vehicle_type/add'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-plus"></span> Add New Vehicle Type</a>
          </div>
          <?php if( isset($_SESSION['msg_error']) ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_error'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if( isset($_SESSION['msg_success']) ): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_success'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php $attr = array("onsubmit" => "return confirm('Are you sure that you want to remove selected vehicle types?')"); ?>
            <?php echo form_open('vehicle_type', $attr); ?>
          <div class="row-fluid">
            <div class="span12">
              <div class="grid simple ">
                <div class="grid-title">
                  <h4>List Vehicle Types</h4>
                  <div class="tools">
                    Bulk Operation: <input type="submit" class="btn btn-cons btn-danger" disabled id="bulk_delete" name="bulk_delete" value="Delete">
                  </div>
                </div>
                <div class="grid-body">
                    <div class="table-responsive">
                      <table class="table table-hover table-condensed" id="example" data-url="<?php echo base_url('vehicle_type/getAllVehicleTypes'); ?>">
                        <thead>
                          <tr>
                            <th style="width:1%">
                              <div class="checkbox check-default" style="margin-right:auto;margin-left:auto;">
                                <input type="checkbox" value="1" id="checkboxAll">
                                <label for="checkboxAll"></label>
                              </div>
                            </th>
                            <th style="width:8%">ID</th>
                            <th style="width:15%">Image</th>
                            <th style="width:15%">Type</th>
                            <th style="width:20%">Country</th>
                            <th style="width:10%" data-hide="phone,tablet">Status</th>
                            <th style="width:15%" data-hide="phone,tablet">Added On</th>
                            <th style="width:15%">Actions</th>
                          </tr>
                        </thead>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>

<?php echo $header; ?>

<!-- BEGIN CONTAINER -->
<div class="page-container row-fluid">

    <?php echo $sidebar; ?>

    <!-- BEGIN PAGE CONTAINER-->
    <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
            <div class="page-title"> <i class="material-icons">airport_shuttle</i>
                <h3>Add New <span class="semi-bold">Vehicle Type</span></h3>
                <a href="<?php echo base_url('vehicle_type'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
                    <?php echo form_open_multipart('vehicle_type/add', $attr); ?>
                    <div class="grid simple form-grid">
                        <div class="grid-title no-border">
                            <h4>Add New Vehicle Type</h4>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"></a>
                            </div>
                        </div>
                        <div class="grid-body no-border">
                            <?php if( isset($_SESSION['msg_error']) ): ?>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="alert alert-danger">
                                            <button class="close" data-dismiss="alert"></button>
                                            <?php echo $this->session->flashdata('msg_error'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if( isset($_SESSION['msg_success']) ): ?>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="alert alert-success">
                                            <button class="close" data-dismiss="alert"></button>
                                            <?php echo $this->session->flashdata('msg_success'); ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                    <div class="form-group">
                                        <label class="form-label" for="name">Vehicle Type *</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <input type="text" name="name" id="name" value="<?php echo set_value('name'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="tagline">Tagline *</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <input type="text" name="tagline" id="tagline" value="<?php echo set_value('tagline'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="description">Description</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <textarea name="description" id="description" class="form-control"rows="4"><?php echo set_value('description'); ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="country_id">Country *</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <select class="select2" name="country_id" id="country_id">
                                                <option value="">--Select Country--</option>
                                                <?php foreach( $countries as $country ): ?>
                                                    <option value="<?php echo $country['id']; ?>"><?php echo $country['name'] . ' (+' . $country['phonecode'] . ')'; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="max_capacity">Max Capacity *</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <input type="number" name="max_capacity" id="max_capacity" value="<?php echo set_value('max_capacity'); ?>" class="form-control" required min="1">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="base_fare">Base Fare *</label>
                                        <span class="help">"in USD e.g. 5.00"</span>
                                        <div class="controls">
                                            <input type="number" name="base_fare" id="base_fare" value="<?php echo set_value('base_fare'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="minimum_fare">Minimum Fare *</label>
                                        <span class="help">"in USD e.g. 5.00"</span>
                                        <div class="controls">
                                            <input type="number" name="minimum_fare" id="minimum_fare" value="<?php echo set_value('max_capacity'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="per_mile_fare">Per Mile Fare *</label>
                                        <span class="help">"in USD e.g. 5.00"</span>
                                        <div class="controls">
                                            <input type="number" name="per_mile_fare" id="per_mile_fare" value="<?php echo set_value('per_mile_fare'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="per_minute_fare">Per Minute Fare *</label>
                                        <span class="help">"in USD e.g. 1.00"</span>
                                        <div class="controls">
                                            <input type="number" name="per_minute_fare" id="per_minute_fare" value="<?php echo set_value('per_minute_fare'); ?>" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="image">Image</label>
                                        <span class="help">(.png, .jpg, .jpeg)</span>
                                        <div class="controls">
                                            <input type="file" name="image" id="image" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="is_active">Active?</label>
                                        <span class="help"></span>
                                        <div class="controls">
                                            <div class="slide-primary">
                                                <input type="checkbox" name="is_active" class="js-switch" checked="checked" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-12">
                                    <div class="form-actions">
                                        <div class="pull-right">
                                            <input type="hidden" name="submit" value="">
                                            <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                            <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->

<?php echo $footer; ?>
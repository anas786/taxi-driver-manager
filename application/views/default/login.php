<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?php echo $pageMeta['title']; ?> - <?php echo APP_NAME; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- BEGIN PLUGIN CSS -->
    <link href="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/pace/pace-theme-flash.css') ?>" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrapv3/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrapv3/css/bootstrap-theme.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/animate.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-scrollbar/jquery.scrollbar.css') ?>" rel="stylesheet" type="text/css" />
    <!-- END PLUGIN CSS -->
    <!-- BEGIN CORE CSS FRAMEWORK -->
    <link href="<?php echo base_url('assets/theme/' . THEME . '/webarch/css/webarch.css') ?>" rel="stylesheet" type="text/css" />
    <!-- END CORE CSS FRAMEWORK -->
  </head>
  <!-- END HEAD -->
  <!-- BEGIN BODY -->
  <body class="error-body no-top">
    <div class="container">
      <div class="row login-container column-seperation">
        <div class="col-md-5 col-md-offset-1">
          <h2>
        Sign in to <?php echo APP_NAME; ?>
      </h2>
          <br>
          <img src="<?php echo base_url(APP_LOGO); ?>" alt="<?php echo APP_NAME; ?>" class="img-responsive">
        </div>
        <div class="col-md-5">
          <br>
          <?php $attr = array('class' => 'login-form validate', 'id' => 'login-form', 'name' => 'login-form'); ?>
          <?php echo form_open('auth/login', $attr); ?>
            <?php if( isset($_SESSION['msg_error']) ): ?>
            <div class="row">
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_error'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if( isset($_SESSION['msg_success']) ): ?>
            <div class="row">
                <div class="col-md-10">
                    <div class="alert alert-success">
                        <button class="close" data-dismiss="alert"></button>
                        <?php echo $this->session->flashdata('msg_success'); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">Email</label>
                <input class="form-control" id="txtusername" name="txtusername" type="email" required>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-10">
                <label class="form-label">Password</label> <span class="help"></span>
                <input class="form-control" id="txtpassword" name="txtpassword" type="password" required>
              </div>
            </div>
            <div class="row">
              <div class="col-md-10">
                <button class="btn btn-primary btn-cons pull-right" type="submit">Login</button>
              </div>
            </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
    <!-- END CONTAINER -->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/pace/pace.min.js') ?>" type="text/javascript"></script>
    <!-- BEGIN JS DEPENDECENCIES-->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery/jquery-1.11.3.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrapv3/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-block-ui/jqueryblockui.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-unveil/jquery.unveil.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-validation/js/jquery.validate.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrap-select2/select2.min.js') ?>" type="text/javascript"></script>
    <!-- END CORE JS DEPENDECENCIES-->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/webarch/js/webarch.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/js/chat.jsassets/js/chat.js') ?>" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS -->
  </body>
</html>
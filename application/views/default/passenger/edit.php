    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <div class="clearfix"></div>
        <div class="content">
          <div class="page-title"> <i class="material-icons">supervisor_account</i>
            <h3>Edit <span class="semi-bold">Passenger</span></h3>
            <a href="<?php echo base_url('passenger'); ?>" class="btn btn-cons btn-primary"><span class="fa fa-times"></span> Cancel</a>
          </div>
          <div class="row-fluid">
            <div class="span12">
            <?php $attr = array('role' => 'form', 'class' => 'validate', 'novalidate' => 'novalidate'); ?>
            <?php echo form_open_multipart('passenger/edit/' . $passenger['id'], $attr); ?>
              <div class="grid simple form-grid">
                <div class="grid-title no-border">
                  <h4>Edit Passenger</h4>
                  <div class="tools">
                    <a href="javascript:;" class="collapse"></a>
                  </div>
                </div>
                <div class="grid-body no-border">
                    <?php if( isset($_SESSION['msg_error']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-danger">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_error'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php if( isset($_SESSION['msg_success']) ): ?>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="alert alert-success">
                                <button class="close" data-dismiss="alert"></button>
                                <?php echo $this->session->flashdata('msg_success'); ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="form-group">
                                <label class="form-label" for="firstname">First Name *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="firstname" id="firstname" value="<?php echo $passenger['firstname']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="lastname">Last Name *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="lastname" id="lastname" value="<?php echo $passenger['lastname']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="email">Email Address *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="email" name="email" id="email" class="form-control" value="<?php echo $passenger['email']; ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="password">Password</label>
                                <span class="help">(Leave empty if you don't want to change password)</span>
                                <div class="controls">
                                    <input type="password" name="password" id="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="mobile">Mobile *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <input type="text" name="mobile" id="mobile" value="<?php echo $passenger['mobile']; ?>" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="photo">Photo</label>
                                <span class="help"><img src="<?php echo base_url(PASSENGER_PHOTO_PATH . $passenger['photo']); ?>" width="40"></span>
                                <div class="controls">
                                    <input type="file" name="photo" id="photo" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="country_id">Country *</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <select class="select2" name="country_id" id="country_id">
                                        <?php foreach( $countries as $country ): ?>
                                        <option value="<?php echo $country['id']; ?>" <?php if( $country['id'] == $passenger['country_id'] ){ echo 'selected'; } ?>><?php echo $country['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="is_active">Active?</label>
                                <span class="help"></span>
                                <div class="controls">
                                    <div class="slide-primary">
                                        <input type="checkbox" name="is_active" class="js-switch" <?php if( $passenger['is_active'] ){ echo 'checked'; } ?>>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-actions">
                                <div class="pull-right">
                                    <input type="hidden" name="submit" value="">
                                    <input type="hidden" name="id" value="<?php echo $passenger['id']; ?>">
                                    <button type="submit" class="btn btn-cons btn-success">Submit</button>
                                    <button type="reset" class="btn btn-cons btn-default">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
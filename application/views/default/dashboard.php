    <?php echo $header; ?>
    
    <!-- BEGIN CONTAINER -->
    <div class="page-container row-fluid">
      
      <?php echo $sidebar; ?>
      
      <!-- BEGIN PAGE CONTAINER-->
      <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div id="portlet-config" class="modal hide">
          <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Widget Settings</h3>
          </div>
          <div class="modal-body"> Widget settings form goes here </div>
        </div>
        <div class="clearfix"></div>
        <div class="content sm-gutter">
          <div class="page-title">
          </div>
          <!-- BEGIN DASHBOARD TILES -->
          <div class="row">
            <div class="col-md-4 col-vlg-3 col-sm-6">
              <div class="tiles green m-b-10">
                <div class="tiles-body">
                  <div class="controller">
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div>
                  <div class="tiles-title text-black">OVERALL SALES </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Overall Visits</span> <span class="item-count animate-number semi-bold" data-value="2415" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="751" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats ">
                    <div class="wrapper last">
                      <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1547" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                  </div>
                  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-vlg-3 col-sm-6">
              <div class="tiles blue m-b-10">
                <div class="tiles-body">
                  <div class="controller">
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div>
                  <div class="tiles-title text-black">OVERALL VISITS </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Overall Visits</span> <span class="item-count animate-number semi-bold" data-value="15489" data- animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="551" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats ">
                    <div class="wrapper last">
                      <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1450" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="54%"></div>
                  </div>
                  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-vlg-3 col-sm-6">
              <div class="tiles purple m-b-10">
                <div class="tiles-body">
                  <div class="controller">
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div>
                  <div class="tiles-title text-black">SERVER LOAD </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Overall Load</span> <span class="item-count animate-number semi-bold" data-value="5695" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="568" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats ">
                    <div class="wrapper last">
                      <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="12459" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="90%"></div>
                  </div>
                  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-vlg-3 visible-xlg visible-sm col-sm-6">
              <div class="tiles red m-b-10">
                <div class="tiles-body">
                  <div class="controller">
                    <a href="javascript:;" class="reload"></a>
                    <a href="javascript:;" class="remove"></a>
                  </div>
                  <div class="tiles-title text-black">OVERALL SALES </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Overall Sales</span> <span class="item-count animate-number semi-bold" data-value="5669" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats">
                    <div class="wrapper transparent">
                      <span class="item-title">Today's</span> <span class="item-count animate-number semi-bold" data-value="751" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="widget-stats ">
                    <div class="wrapper last">
                      <span class="item-title">Monthly</span> <span class="item-count animate-number semi-bold" data-value="1547" data-animation-duration="700">0</span>
                    </div>
                  </div>
                  <div class="progress transparent progress-small no-radius m-t-20" style="width:90%">
                    <div class="progress-bar progress-bar-white animate-progress-bar" data-percentage="64.8%"></div>
                  </div>
                  <div class="description"> <span class="text-white mini-description ">4% higher <span class="blend">than last month</span></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- END DASHBOARD TILES -->
          <div class="row">
            <div class="col-md-6 col-vlg-4">
              <div class="row">
                <div class="col-md-12 m-b-10">
                  <!-- BEGIN SALES WIDGET WITH FLOT CHART -->
                  <div class="tiles white add-margin">
                    <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                      <div class="row b-grey b-b xs-p-b-20">
                        <div class="col-md-4 col-sm-4">
                          <h4 class="text-black semi-bold">Total Income</h4>
                          <h3 class="text-success semi-bold">$15,354</h3>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <div class="m-t-20">
                            <h5 class="text-black semi-bold">Total due</h5>
                            <h4 class="text-success semi-bold">$4,653</h4>
                          </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                          <div class="m-t-20">
                            <input type="text" class="dark form-control" id="txtinput3" placeholder="Search">
                          </div>
                        </div>
                      </div>
                      <div class="row b-grey">
                        <div class="col-md-3 col-sm-3">
                          <div class="m-t-10">
                            <p class="text-success">Open</p>
                            <p class="text-black">16:203.26</p>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                          <div class="m-t-10">
                            <p class="text-success">Day Range</p>
                            <p class="text-black">01.12.13 - 01.01.14</p>
                          </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                          <div class="m-t-10">
                            <div class="pull-left">
                              Cash
                            </div>
                            <div class="pull-right">
                              <span class="text-success">$10,525</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="pull-left">
                              Visa Classic
                            </div>
                            <div class="pull-right">
                              <span class="text-success">$5,989</span>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tiles grey" id="chart_1" style="height: 260px;width:100%">
                    </div>
                  </div>
                  <!-- END SALES WIDGET WITH FLOT CHART -->
                </div>
              </div>
            </div>
            <div class="col-md-6 col-vlg-8">
              <div class="row hidden-xlg">
                <div class="col-md-12 m-b-10">
                  <!-- BEGIN SALES WIDGET WITH FLOT CHART 2-->
                  <div class="tiles white add-margin">
                    <div class="p-t-20 p-l-20 p-r-20 p-b-20">
                      <div class="row b-grey b-b xs-p-b-20">
                        <div class="col-md-4 col-sm-4">
                          <h4 class="text-black semi-bold">Total Visits</h4>
                          <h3 class="text-error semi-bold">25,850</h3>
                        </div>
                        <div class="col-md-3 col-sm-3">
                          <div class="m-t-20">
                            <h5 class="text-black semi-bold">Today</h5>
                            <h4 class="text-error semi-bold">1,900</h4>
                          </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                          <div class="m-t-20">
                            <input type="text" class="dark form-control" id="txtinput4" placeholder="Search">
                          </div>
                        </div>
                      </div>
                      <div class="row b-grey">
                        <div class="col-md-3 col-sm-3">
                          <div class="m-t-10">
                            <p class="text-error">Open</p>
                            <p class="text-black">15:25:56</p>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                          <div class="m-t-10">
                            <p class="text-error">Day Range</p>
                            <p class="text-black">01.02.2014 - 01.1.2015</p>
                          </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                          <div class="m-t-10">
                            <div class="pull-left">
                              Cash
                            </div>
                            <div class="pull-right">
                              <span class="text-error">$10,525</span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="pull-left">
                              Visa Classic
                            </div>
                            <div class="pull-right">
                              <span class="text-error">$5,989</span>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="tiles grey" id="sales_chart_alt" style="height: 260px;position: relative;width:100%"> </div>
                  </div>
                  <!-- END SALES WIDGET WITH FLOT CHART -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CHAT -->
    </div>
    <!-- END CONTAINER -->
    
    <?php echo $footer; ?>
        <!-- BEGIN SIDEBAR -->
      <div class="page-sidebar " id="main-menu">
        <!-- BEGIN MINI-PROFILE -->
        <div class="page-sidebar-wrapper scrollbar-dynamic" id="main-menu-wrapper">
          <div class="user-info-wrapper sm">
            <div class="profile-wrapper sm">
              <img src="<?php echo base_url('assets/img/avatar.jpg'); ?>" alt="" data-src="<?php echo base_url('assets/img/avatar.jpg'); ?>" data-src-retina="<?php echo base_url('assets/img/avatar2x.jpg'); ?>" width="69" height="69" />
              <div class="availability-bubble online"></div>
            </div>
            <div class="user-info sm">
              <div class="username"><?php echo $this->session->user_name; ?></div>
              <div class="status"><?php echo $this->session->user_email; ?></div>
            </div>
          </div>
          <!-- END MINI-PROFILE -->
          <!-- BEGIN SIDEBAR MENU -->
          <p class="menu-title sm">BROWSE <span class="pull-right"><a href="javascript:;"><i class="material-icons">refresh</i></a></span></p>
          <ul>
            <li <?php if( $this->router->fetch_class() == 'dashboard' ): ?>class="active"<?php endif; ?>>
                <a href="<?php echo base_url('dashboard'); ?>"><i class="material-icons">home</i> <span class="title">Dashboard</span> </a>
            </li>
            <li <?php if( $this->router->fetch_class() == 'live_tracking' ): ?>class="active"<?php endif; ?>> 
                <a href="<?php echo base_url('live_tracking'); ?>"><i class="material-icons">location_on</i> <span class="title">Live Tracking</span> </a>
            </li>

            <li <?php if( $this->router->fetch_class() == 'user' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">supervisor_account</i> <span class="title">Users</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('user/add'); ?>">Add New User </a> </li>
                <li> <a href="<?php echo base_url('user'); ?>">List All Users</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'passenger' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">airline_seat_recline_normal</i> <span class="title">Passengers</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('passenger/add'); ?>">Add New Passenger </a> </li>
                <li> <a href="<?php echo base_url('passenger'); ?>">List All Passengers</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'driver' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">person_pin</i> <span class="title">Drivers</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('driver/add'); ?>">Add New Driver </a> </li>
                <li> <a href="<?php echo base_url('driver'); ?>">List All Drivers</a> </li>
              </ul>
            </li>
              <li <?php if( $this->router->fetch_class() == 'ride' ): ?>class="open active"<?php endif; ?>>
                  <a href="javascript:;"> <i class="material-icons">assignment</i> <span class="title">Rides</span> <span class=" arrow"></span> </a>
                  <ul class="sub-menu">
                      <li> <a href="<?php echo base_url('ride/completed'); ?>">Completed Rides</a> </li>
                      <li> <a href="<?php echo base_url('ride/ongoing'); ?>">On Going Rides</a> </li>
                      <li> <a href="<?php echo base_url('ride/cancelled'); ?>">Cancelled Rides</a> </li>
                  </ul>
              </li>
            <li <?php if( $this->router->fetch_class() == 'vehicle' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">local_taxi</i> <span class="title">Vehicles</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('vehicle/add'); ?>">Add New Vehicle </a> </li>
                <li> <a href="<?php echo base_url('vehicle'); ?>">List All Vehicles</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'vehicle_type' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">airport_shuttle</i> <span class="title">Vehicle Types</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('vehicle_type/add'); ?>">Add New Vehicle Type </a> </li>
                <li> <a href="<?php echo base_url('vehicle_type'); ?>">List All Vehicle Types</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'vehicle_model' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">local_car_wash</i> <span class="title">Vehicle Models</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('vehicle_model/add'); ?>">Add New Vehicle Model </a> </li>
                <li> <a href="<?php echo base_url('vehicle_model'); ?>">List All Vehicle Models</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'localization' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">public</i> <span class="title">Localizations</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('localization/country'); ?>">Countries </a> </li>
                <li> <a href="<?php echo base_url('localization/state'); ?>">States</a> </li>
              </ul>
            </li>
            <li <?php if( $this->router->fetch_class() == 'report' ): ?>class="open active"<?php endif; ?>>
              <a href="javascript:;"> <i class="material-icons">timeline</i> <span class="title">Reports</span> <span class=" arrow"></span> </a>
              <ul class="sub-menu">
                <li> <a href="<?php echo base_url('report/passenger'); ?>">Passenger Report</a> </li>
                <li> <a href="<?php echo base_url('report/driver'); ?>">Driver Report</a> </li>
                <li> <a href="<?php echo base_url('report/vehicle'); ?>">Vehicle Report</a> </li>
              </ul>
            </li>
          <div class="clearfix"></div>
          <!-- END SIDEBAR MENU -->
        </div>
      </div>
      <a href="#" class="scrollup">Scroll</a>
      <div class="footer-widget">
        <!--<div class="progress transparent progress-small no-radius no-margin">
          <div class="progress-bar progress-bar-success animate-progress-bar" data-percentage="79%" style="width: 79%;"></div>
        </div>-->
        <div class="pull-right">
          <!--<div class="details-status"> <span class="animate-number" data-value="86" data-animation-duration="560">86</span>% </div>-->
          <a href="<?php echo base_url('auth/logout'); ?>"><i class="material-icons">power_settings_new</i></a></div>
      </div>
      <!-- END SIDEBAR -->
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
        var API_URL = '<?php echo API_URL; ?>';
    </script>
    <!-- BEGIN CORE JS FRAMEWORK-->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/pace/pace.min.js') ?>" type="text/javascript"></script>
    <!-- BEGIN JS DEPENDECENCIES-->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery/jquery-1.11.3.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrapv3/js/bootstrap.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-block-ui/jqueryblockui.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-unveil/jquery.unveil.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-numberAnimate/jquery.animateNumbers.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-validation/js/jquery.validate.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/bootstrap-select2/select2.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/switchery/dist/switchery.min.js') ?>" type="text/javascript"></script>
    <!-- END CORE JS DEPENDECENCIES-->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url('assets/theme/' . THEME . '/webarch/js/webarch.js') ?>" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS -->
    <script src="<?php echo base_url('assets/lib/bootstrap-datatable/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/lib/bootstrap-datatable/dataTables.bootstrap.min.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="<?php echo base_url('assets/theme/' . THEME . '/assets/plugins/jquery-gmap/gmaps.js') ?>" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <!--<script src="<?php echo base_url('assets/theme/' . THEME . '/assets/js/dashboard_v2.js') ?>" type="text/javascript"></script>-->
    
    <script src="<?php echo base_url('assets/js/custom.js') ?>" type="text/javascript"></script>
  </body>
</html>
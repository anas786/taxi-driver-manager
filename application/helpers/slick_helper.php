<?php
/* SRMS Helper Class */
defined('BASEPATH') OR exit('No direct script access allowed');

/* Function to check whether a role has access to the specific module */
function hasAccess($role_id, $module)
{
    // If super admin
    if( $role_id == 1 )
    {
        // Allow everything
        return true;
    }
    else
    {
        $CI =& get_instance();
    
        $CI->load->database();
        
        $sql = "SELECT * FROM `tbl_module_access` JOIN `tbl_module` ON tbl_module_access.module_id = tbl_module.id WHERE tbl_module.module = '".$module."' AND tbl_module_access.role_id = '".$role_id."'";
    
        $result = $CI->db->query($sql);
    
        if( $result->num_rows() > 0 )
        {
            return true;    // Has Access
        }
        else
        {
            return false;   // Doesn't have Access
        }
    }
    
}

/* Function to check if user role is a super admin */
function isAdmin()
{
    $CI =& get_instance();
    
    if($CI->session->has_userdata('user_id'))
    {
        if($CI->session->userdata('user_role') == 1)
        {
            // Admin
            return true;
        }
        else
        {
            // Not Admin
            return false;
        }
    }
    else
    {
        return false;
    }
    
}

/* Function to check if user is logged in */
function isLoggedIn()
{
    $CI =& get_instance();
    
    if($CI->session->has_userdata('user_id'))
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

/* Date Format */
function easyDate($date, $format='d-m-Y')
{
    $CI =& get_instance();
     
    $d = new DateTime($date);

    if( APP_TIMEZONE > 0 ) {
        $d->add(new DateInterval('PT' . APP_TIMEZONE . 'H'));
    } else {
        $d->sub(new DateInterval('PT' . abs(APP_TIMEZONE) . 'H'));
    }
    
    return $d->format($format);
}

/* Date Format */
function easyDateTime($datetime, $format='d-m-Y h:i A')
{
    $CI =& get_instance();
    
    $d = new DateTime($datetime);

    if( APP_TIMEZONE > 0 ) {
        $d->add(new DateInterval('PT' . APP_TIMEZONE . 'H'));
    } else {
        $d->sub(new DateInterval('PT' . abs(APP_TIMEZONE) . 'H'));
    }
    
    return $d->format( $format );
}
/* Time Format */
function easyTime($time, $format='h:i A')
{
    $CI =& get_instance();
    
    $d = new DateTime($time);

    if( APP_TIMEZONE > 0 ) {
        $d->add(new DateInterval('PT' . APP_TIMEZONE . 'H'));
    } else {
        $d->sub(new DateInterval('PT' . abs(APP_TIMEZONE) . 'H'));
    }
    
    return $d->format( $format );
}

/* Return current date */
function datenow()
{
    return gmdate('Y-m-d H:i:s');
}

/* Return numbers as money format */
function moneyFormat($val,$symbol='',$r=2)
{
    $n = $val; 
    $c = is_float($n) ? 1 : number_format($n,$r);
    $d = '.';
    $t = ',';
    $sign = ($n < 0) ? '-' : '';
    $i = $n=number_format(abs($n),$r); 
    $j = (($j = $i) > 3) ? $j % 3 : 0; 

   return  $symbol.$sign .($j ? substr($i,0, $j) + $t : '').preg_replace('/(\d{3})(?=\d)/',"$1" + $t,substr($i,$j)) ;

}

/* Calculates age from date of birth */
function calculateAge($dob, $to='') 
{
    if( $to == '' )
    {
        $from = new DateTime($dob);
        $to   = new DateTime('today');
    }
    else 
    {
        $from = new DateTime($dob);
        $to   = new DateTime($to);
    }
    return $from->diff($to)->y;
}

/* Get Countries from DB */
function getCountries() 
{
    $CI =& get_instance();
    
    $CI->load->database();
    
    $result = $CI->db->get('country');
    
    return $result->result_array();
}

/* Send Push Notifications */
function sendPush($tokens, $title, $msg, $custom_data=null) 
{
    //FCM api URL
    $url = 'https://fcm.googleapis.com/fcm/send';
    //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
    $server_key = 'AAAA5YOG9rQ:APA91bHs3B-reUHsN8_Bvr_lrNasHHqQL3W2UKhXxNE5-N1QLfnTTfq5kh4dor95Dxcd8d0iccw8HQz_ELq3Wf3K2kABhVWn5xEQYHFMK9jgZhluO44z7t2oT4sowUv1_RjOfrHyyhPw';
                
    $msg = array
      (
        'body'  => $msg,
        'title' => $title,
        'sound' => 'default',
        'icon' => 'fcm_push_icon',
        'color' => '#b71c1c',
        'click_action' => 'FCM_PLUGIN_ACTIVITY'
      );
      
    $target = $tokens;
    if(count($target) == 1) {
        $fields['to'] = $target[0];
    } else {
        $fields['registration_ids'] = $target;
    }
    
    $fields['notification'] = $msg;
    if( $custom_data != null ) {
        $fields['data'] = $custom_data;
    }
    
    //header with content_type api key
    $headers = array(
        'Content-Type:application/json',
        'Authorization:key='.$server_key
    );
                
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('FCM Send Error: ' . curl_error($ch));
    }
    curl_close($ch);
}

function notallowed()
{
    $CI =& get_instance();
    $data['pageMeta'] = array(
        'title' => 'User List'
    );
    $data['header'] = $CI->load->view(THEME . '/layout/header', $data, true);
    $data['sidebar'] = $CI->load->view(THEME . '/layout/sidebar', $data, true);
    $data['footer'] = $CI->load->view(THEME . '/layout/footer', $data, true);
	$CI->load->view(THEME . '/notallowed', $data);
}

